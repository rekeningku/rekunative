import Home from './home'
import Login from './login'
import TodoList from './todo-list'
import TodoAdd from './todo-add'

import SignIn1Basic from './examples/signin1/basic/signin1'
import SignIn1Custom from './examples/signin1/custom/signin1'
import SignIn2Basic from './examples/signin2/basic/signin2'
import SignUp1Basic from './examples/signup1/basic/signup1'
import SignUp1Custom from './examples/signup1/custom/signup1'
import SignUp2 from './examples/signup2/signup2'
import Profile1Basic from './examples/profile1/basic/profile1' //#TODO
import Profile2 from './examples/profile2/profile2'
import Profile3 from './examples/profile3/profile3' //#TODO
import ProfileSettingBasic from './examples/profilesetting/basic/profilesetting' //#TODO
import ProfileSettingCustom from './examples/profilesetting/custom/profilesetting' //#TODO
import ForgotPasswordBasic from './examples/forgotpassword/basic/forgotpassword'
import ForgotPasswordCustom from './examples/forgotpassword/custom/forgotpassword'
import NotificationBasic from './examples/notification/basic/notification' //#TODO
import NotificationCustom from './examples/notification/custom/notification' //#TODO
import ContactBasic from './examples/contact/basic/contact' //#TODO
import ContactCustom from './examples/contact/custom/contact' //#TODO
import Article1Basic from './examples/article1/basic/article1' //#TODO
import Article2 from './examples/article2/article2' //#TODO
import Article3 from './examples/article3/article3' //#TODO
import ArticleView1Basic from './examples/articleview1/basic/articleview1' //#TODO
import ArticleView1Custom from './examples/articleview1/custom/articleview1' //#TODO
import ArticleView2 from './examples/articleview2/articleview2' //#TODO
import Chat from './examples/chat/chat' //#TODO
import ChatList from './examples/chatlist/chatlist' //#TODO
import CommentBasic from './examples/comment/basic/comment' //#TODO
import Dashboard1Basic from './examples/dashboard1/basic/dashboard1' //#TODO
import Dashboard1Custom from './examples/dashboard1/custom/dashboard1' //#TODO
import Dashboard2 from './examples/dashboard2/dashboard2' //#TODO
import OnBoarding1Basic from './examples/onboarding1/basic/onboarding1' //#TODO
import OnBoarding1Custom from './examples/onboarding1/custom/onboarding1' //#TODO
import OnBoarding2 from './examples/onboarding2/onboarding2' //#TODO
import CreditCardBasic from './examples/creditcard/basic/creditcard' //#TODO
import CreditCardCustom from './examples/creditcard/custom/creditcard' //#TODO
import GridMenu1Basic from './examples/gridmenu1/basic/gridmenu1' //#TODO
import GridMenu2Basic from './examples/gridmenu2/basic/gridmenu2' //#TODO
import ListMenuBasic from './examples/listmenu/basic/listmenu' //#TODO
import SideMenuBasic from './examples/sidemenu/basic/sidemenu' //#TODO
import SideMenuCustom from './examples/sidemenu/custom/sidemenu' //#TODO
import SettingBasic from './examples/setting/basic/setting' //#TODO
import SettingCustom from './examples/setting/custom/setting' //#TODO
import Product1Basic from './examples/product1/basic/product1' //#TODO
import Product1Custom from './examples/product1/custom/product1' //#TODO
import Product2 from './examples/product2/product2' //#TODO
import Transaction1Basic from './examples/transaction1/basic/transaction1' //#TODO
import TransactionDetail from './examples/transactiondetail/basic/transactiondetail' //#TODO
import Transaction2 from './examples/transaction2/transaction2' //#TODO
import IllustrationBasic from './examples/illustration/basic/illustration' //#TODO
import Carousel1Basic from './examples/carousel1/basic/carousel1' //#TODO
import Carousel1Custom from './examples/carousel1/custom/carousel1' //#TODO
import Carousel2Basic from './examples/carousel2/basic/carousel2' //#TODO
import Carousel2Custom from './examples/carousel2/custom/carousel2' //#TODO
import MapsPickerBasic from './examples/mapspicker/basic/mapspicker' //#TODO
import Video1Basic from './examples/video1/basic/video1'
import VideoDetailBasic from './examples/videodetail/basic/videodetail'

export {
	Home,
	Login,
	TodoList,
	TodoAdd,
	//examples
	SignIn1Basic,
	SignIn1Custom,
	SignIn2Basic,
	SignUp1Basic,
	SignUp1Custom,
	SignUp2,
	Profile1Basic,
	Profile2,
	Profile3,
	ProfileSettingBasic,
	ProfileSettingCustom,
	NotificationBasic,
	NotificationCustom,
	ContactBasic,
	ContactCustom,
	Article1Basic,
	Article2,
	Article3,
	ArticleView1Basic,
	ArticleView1Custom,
	ArticleView2,
	Chat,
	ChatList,
	CommentBasic,
	Dashboard1Basic,
	Dashboard1Custom,
	Dashboard2,
	OnBoarding1Basic,
	OnBoarding1Custom,
	OnBoarding2,
	CreditCardBasic,
	CreditCardCustom,
	GridMenu1Basic,
	GridMenu2Basic,
	ListMenuBasic,
	SideMenuBasic,
	SideMenuCustom,
	SettingBasic,
	SettingCustom,
	Product1Basic,
	Product1Custom,
	Product2,
	TransactionDetail,
	Transaction1Basic,
	Transaction2,
	IllustrationBasic,
	ForgotPasswordBasic,
	ForgotPasswordCustom,
	Carousel1Basic,
	Carousel1Custom,
	Carousel2Basic,
	Carousel2Custom,
	MapsPickerBasic,
	Video1Basic,
	VideoDetailBasic
}
