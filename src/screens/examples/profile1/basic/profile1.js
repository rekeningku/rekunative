import React, {Component} from 'react'

import Profile1 from '../../../../../rekunative/templates/components/profile1'

class Profile1Example extends Component {

    static navigationOptions = {
        headerTitle: 'USER PROFILE',
    }

    render() {
        const options = {
            data: {
                name: 'Helen Glibert',
                text1: {
                    label:'Posts',
                    value:86,
                    onPress:() => {
                        alert('Posts')
                    }
                },
                text2: {
                    label:'Followers',
                    value:'22.1k',
                    onPress:() => {
                        alert('Followers')
                    }
                },
                text3: {
                    label:'Following',
                    value:536,
                     onPress:() => {
                        alert('Following')
                    }
                },
                imageUrl: 'https://i.pinimg.com/564x/01/7d/d3/017dd36c244908f6eb97c51e2e4934c6.jpg'
            },
            imagePosts: [
                {
                    url:'https://i.pinimg.com/564x/4b/d3/06/4bd306da86060fadd8c2560778daf6e7.jpg',
                    like:19,
                    comment:26,
                    user:6,
                    onPressImage:() => {
                        alert('image')
                    },
                    onPressLike:()=>{
                        alert('Like')
                    },
                    onPressComment:()=>{
                        alert('Comment')
                    },
                    onPressUser:() => {
                        alert('User')
                    }
                },
                {
                    url:'https://i.pinimg.com/564x/7c/de/87/7cde87c529f9e267029551838f8b68b4.jpg',
                    like:19,
                    comment:26,
                    user:6,
                    onPressImage:() => {
                        alert('image')
                    },
                    onPressLike:()=>{
                        alert('Like')
                    },
                    onPressComment:()=>{
                        alert('Comment')
                    },
                    onPressUser:() => {
                        alert('User')
                    }
                },
                {
                    url:'https://i.pinimg.com/564x/55/3d/2e/553d2e79b9c64cec0cb42b09c25660b5.jpg',
                    like:19,
                    comment:26,
                    user:6,
                    onPressImage:() => {
                        alert('image')
                    },
                    onPressLike:()=>{
                        alert('Like')
                    },
                    onPressComment:()=>{
                        alert('Comment')
                    },
                    onPressUser:() => {
                        alert('User')
                    }
                },
                {
                    url:'https://i.pinimg.com/564x/11/75/f6/1175f642098122307a1b14ac286e62e5.jpg',
                    like:19,
                    comment:26,
                    user:6,
                    onPressImage:() => {
                        alert('image')
                    },
                    onPressLike:()=>{
                        alert('Like')
                    },
                    onPressComment:()=>{
                        alert('Comment')
                    },
                    onPressUser:() => {
                        alert('User')
                    }
                },
                {
                    url:'https://i.pinimg.com/564x/78/34/11/78341103ac7dc6e2e5ca4257a699baed.jpg',
                    like:19,
                    comment:26,
                    user:6,
                    onPressImage:() => {
                        alert('image')
                    },
                    onPressLike:()=>{
                        alert('Like')
                    },
                    onPressComment:()=>{
                        alert('Comment')
                    },
                    onPressUser:() => {
                        alert('User')
                    }
                },
                {
                    url:'https://i.pinimg.com/564x/88/66/2d/88662d3302b43eec3ad89da81f118343.jpg',
                    like:19,
                    comment:26,
                    user:6,
                    onPressImage:() => {
                        alert('image')
                    },
                    onPressLike:()=>{
                        alert('Like')
                    },
                    onPressComment:()=>{
                        alert('Comment')
                    },
                    onPressUser:() => {
                        alert('User')
                    }
                },
                {
                    url:'https://i.pinimg.com/564x/f7/27/49/f72749d9739732c0f5269fa1316920d3.jpg',
                    like:19,
                    comment:26,
                    user:6,
                    onPressImage:() => {
                        alert('image')
                    },
                    onPressLike:()=>{
                        alert('Like')
                    },
                    onPressComment:()=>{
                        alert('Comment')
                    },
                    onPressUser:() => {
                        alert('User')
                    }
                },
                {
                    url:'https://i.pinimg.com/564x/ee/0a/c6/ee0ac61a81c8dbabda92b61f10828b6f.jpg',
                    like:19,
                    comment:26,
                    user:6,
                    onPressImage:() => {
                        alert('image')
                    },
                    onPressLike:()=>{
                        alert('Like')
                    },
                    onPressComment:()=>{
                        alert('Comment')
                    },
                    onPressUser:() => {
                        alert('User')
                    }
                },
                {
                    url:'https://i.pinimg.com/564x/61/80/ff/6180ff88a77a779ec6c62130d77139c1.jpg',
                    like:19,
                    comment:26,
                    user:6,
                    onPressImage:() => {
                        alert('image')
                    },
                    onPressLike:()=>{
                        alert('Like')
                    },
                    onPressComment:()=>{
                        alert('Comment')
                    },
                    onPressUser:() => {
                        alert('User')
                    }
                },
            ],
            btnSubHeader: {
                left: {
                    name: 'FOLLOW',
                    onPress: () => {
                        alert('FOLLOW')
                    }
                },
                right: {
                    name: 'MESSAGE',
                    onPress: () => {
                        alert('MESSAGE')
                    }
                }
            },
            btnOptionModal:{
                onPress:() =>{
                    alert('Options')
                }
            }
        }

        return (<Profile1 {...options}/>)
    }
}
export default Profile1Example
