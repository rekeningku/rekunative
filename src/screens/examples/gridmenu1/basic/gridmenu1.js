import React, { Component } from 'react'

import GridMenu1 from '../../../../../rekunative/templates/components/gridmenu1'

export default class GridMenu1Example extends Component {

	static navigationOptions = {
		headerTitle: 'GRID MENU'
	}

	render() {
		const options = {
			data:[{
				label: 'Auth',
				icon: 'unlock-alt',
				to: 'ListMenu',
				onPress: () => {
					alert('pressed')
				},
				submenu: {
					label: 'Auth',
					menus: [
						{
							iconName: 'lock-outline',
							iconType: 'MaterialIcons',
							label: 'Auth',
							onPress: () => {
								alert('Auth')
							}
						},
						{
							iconName: 'person-outline',
							iconType: 'MaterialIcons',
							label: 'Social',
							onPress: () => {
								alert('Social')
							}
						},
						{
							iconName: 'envelope-o',
							iconType: 'FontAwesome',
							label: 'Message',
							onPress: () => {
								alert('Message')
							}
						}
					]
				}
			},
			{
				label: 'Social',
				icon: 'user-circle',
				to: 'Profile1',
				onPress: () => {
					alert('pressed')
				}
			},
			{
				label: 'Articles',
				icon: 'user-circle',
				to: 'Article1',
				onPress: () => {
					alert('pressed')
				}
			},
			{
				label: 'Messaging',
				icon: 'envelope',
				onPress: () => {
					alert('pressed')
				}
			},
			{
				label: 'Dashboards',
				icon: 'columns',
				to: 'Dashboard1',
				onPress: () => {
					alert('pressed')
				}
			},
			{
				label: 'Walkthrough',
				icon: 'mobile-alt',
				onPress: () => {
					alert('pressed')
				}
			},
			{
				label: 'Ecommerce',
				icon: 'credit-card',
				onPress: () => {
					alert('pressed')
				}
			},
			{
				label: 'Navigation',
				icon: 'compass',
				onPress: () => {
					alert('pressed')
				}
			},
			{
				label: 'Other',
				icon: 'bars',
				onPress: () => {
					alert('pressed')
				}
			},
			{
				label: 'Themes',
				icon: 'palette',
				onPress: () => {
					alert('pressed')
				}
			}]
		}
		
		return <GridMenu1 {...options} />
	}
}
