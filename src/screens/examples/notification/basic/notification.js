import React, {Component} from 'react'

import Notification from '../../../../../rekunative/templates/components/notification'

class NotificationExample extends Component {
    static navigationOptions = {
        headerTitle: 'NOTIFICATION',
    }

    render() {
        const options = {
            data: [
                {
                    name: 'Helen Gilbert',
                    desc: 'liked profile page for Dribble App Design Concept',
                    imgProfileUrl: 'https://i.pinimg.com/564x/01/7d/d3/017dd36c244908f6eb97c51e2e4934c6.jpg',
                    imageUrl:'',
                    time:'a few seconds ago',
                    isRead:false
                },
                {
                    name: 'Emilie McDiarmid',
                    desc: 'liked a photo on your album',
                    imgProfileUrl: 'https://i.pinimg.com/564x/a5/8b/1f/a58b1f3bb4dc10ba447a674398ed9183.jpg',
                    imageUrl:'https://i.pinimg.com/564x/44/cb/69/44cb69dddb721876901b05ba1de14f4a.jpg',
                    time:'5 minutes ago',
                    isRead:false
                },
                {
                    name: 'Sandra Paver',
                    desc: 'liked a photo on your album',
                    imgProfileUrl: 'https://i.pinimg.com/564x/79/b0/d3/79b0d36f3a11a9d0add785d58481d9de.jpg',
                    imageUrl:'https://i.pinimg.com/564x/44/cb/69/44cb69dddb721876901b05ba1de14f4a.jpg',
                    time:'9 minutes ago',
                    isRead:true
                },
                {
                    name: "Clayton O'Mullaney",
                    desc: 'liked profile page for Dribbble App Design Concept',
                    imgProfileUrl: 'https://i.pinimg.com/564x/30/36/71/303671a20140d5f8adce77bd11c2fefa.jpg',
                    imageUrl:'',
                    time:'18 minutes ago',
                    isRead:true
                },
                {
                    name: "Carlee Aubry",
                    desc: 'liked a photo on your album',
                    imgProfileUrl: 'https://i.pinimg.com/564x/1a/29/21/1a2921012c7c36bf9aee008b93b207f4.jpg',
                    imageUrl:'',
                    time:'23 minutes ago',
                    isRead:true
                },
                {
                    name: "Patrick Holden",
                    desc: 'like a photo your album',
                    imgProfileUrl: 'https://i.pinimg.com/564x/57/fa/a3/57faa396ce7ccc679a0c5e7b5600ff67.jpg',
                    imageUrl:'https://i.pinimg.com/564x/44/cb/69/44cb69dddb721876901b05ba1de14f4a.jpg',
                    time:'27 minutes ago',
                    isRead:true
                },
                {
                    name: "Edwar Storton",
                    desc: 'followed you IOS Developer and Graphic Designer',
                    imgProfileUrl: 'https://i.pinimg.com/564x/bc/fe/5a/bcfe5a139930f81fa2593ef8feb839bf.jpg',
                    time:'32 minutes ago',
                    isRead:true
                },
            ]
        }
        return (<Notification {...options}/>)
    }
}

export default NotificationExample
