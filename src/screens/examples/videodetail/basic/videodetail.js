import React, { Component } from 'react'

import Video from '../../../../../rekunative/templates/components/videodetail'

export default class VideoDetailExample extends Component {
	static navigationOptions = {
		headerTitle: 'Video Detail'
	}

    render(){
        const options = {
            videoUrl : 'https://www.youtube.com/watch?v=FaBzudGS0Hw',
            height: '50%',
            width: '100%'
        }
		return <Video {...options} />
	}
}
