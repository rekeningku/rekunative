import React, { Component } from 'react'

import OnBoarding1 from '../../../../../rekunative/templates/components/onboarding1'

export default class OnBoarding1Example extends Component {
	static navigationOptions = {
		header: null
	}

	render() {
		const options = {
			data:[
				{
					key: 'somethun',
					title: 'Quick setup, good defaults',
					text: 'React-native-app-intro-slider is easy to setup with a small footprint and no dependencies. And it comes with good default layouts!',
					icon: 'images',
				},
				{
					key: 'somethun1',
					title: 'Super customizable',
					text: 'The component is also super customizable, so you can adapt it to cover your needs and wants.',
					icon: 'sliders-h',
				},
				{
					key: 'somethun2',
					title: 'No need to buy me beer',
					text: 'Usage is all free',
					icon: 'beer',
				}
			]
		}
		return <OnBoarding1 {...options} />
	}
}
