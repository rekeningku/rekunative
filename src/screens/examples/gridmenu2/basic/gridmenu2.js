import React, {Component} from 'react'

import GridMenu2 from '../../../../../rekunative/templates/components/gridmenu2'

export default class GridMenu2Example extends Component {

    static navigationOptions = {
        headerTitle: 'GridMenu 2'
    }

    render() {
        const options = {
            data: [
                {
                    label: 'Auth',
                    icon: 'unlock-alt',
                    to: 'ListMenu',
                    onPress: () => {
                        alert('pressed')
                    }
                }, {
                    label: 'Social',
                    icon: 'user-circle',
                    to: 'Profile1',
                    onPress: () => {
                        alert('pressed')
                    }
                }, {
                    label: 'Articles',
                    icon: 'user-circle',
                    to: 'Article1',
                    onPress: () => {
                        alert('pressed')
                    }
                }, {
                    label: 'Messaging',
                    icon: 'envelope',
                    onPress: () => {
                        alert('pressed')
                    }
                }, {
                    label: 'Dashboards',
                    icon: 'columns',
                    to: 'Dashboard1',
                    onPress: () => {
                        alert('pressed')
                    }
                }, {
                    label: 'Walkthrough',
                    icon: 'mobile-alt',
                    onPress: () => {
                        alert('pressed')
                    }
                }, {
                    label: 'Ecommerce',
                    icon: 'credit-card',
                    onPress: () => {
                        alert('pressed')
                    }
                }, {
                    label: 'Navigation',
                    icon: 'compass',
                    onPress: () => {
                        alert('pressed')
                    }
                }, {
                    label: 'Other',
                    icon: 'bars',
                    onPress: () => {
                        alert('pressed')
                    }
                }
            ]
        }

        return (<GridMenu2 {...options}/>)
    }
}