import React, { Component } from 'react'

import ProfileSetting from '../../../../../rekunative/templates/components/profilesetting'

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const initialValues = {
	firstName: 'HELLEN',
	lastName: 'GILBERT',
	email: 'h.gilbert@gmail.com',
	country: 'zimbabwe',
	phone: '081212121212'
}

const validationSchema = {
	title: 'profile Setting',
	description: 'profile setting form',
	type: 'object',
	properties: {
		firstName: {
			description: 'first name of person',
			type: 'string',
			required: true
		},
		lastName: {
			description: 'last name of person',
			type: 'string',
			required: true
		},
		email: {
			description: 'Email of person',
			type: 'string',
			required: true,
			format: 'email'
		},
		country: {
			description: 'Country of person',
			type: 'string',
			required: true
		},
		phone: {
			description: 'Phone of person',
			type: 'string',
			required: true,
			matches: phoneRegExp
		},
		oldPassword: {
			description: 'password of preson',
			type: 'string',
			minLength: 8
		},
		newPassword: {
			description: 'password of preson',
			name: 'newPassword',
			type: 'string',
			minLength: 8
		},
		newPasswordConfirmation: {
			description: 'password of preson',
			type: 'string',
			minLength: 8
		}
	}
}

const validationConfig = {
	// for error message
	errMessages: {
		firstName: {
			required: 'Required!'
		},
		lastName: {
			required: 'Required!'
		},
		email: {
			required: 'Required!',
			format: 'Your email not valid!'
		},
		country: {
			required: 'Required!'
		},
		phone: {
			required: 'Required!',
			matches: 'Your phone number not valid!'
		}
	}
}

export default class ProfileSettingExample extends Component {
	static navigationOptions = {
		headerTitle: 'PROFILE SETTINGS'
	}

	state = {
		newPassword: ''
	}

	render() {
		const options = {
			// define some form
			form:[
				{
					group: 'Info',
					fields: [
						{
							label: 'First Name',
							name: 'firstName',
							onChangeText(value) {
								console.warn(value)
							}
						},
						{
							label: 'Last Name',
							name: 'lastName',
							onChangeText(value) {
								console.warn(value)
							}
						},
						{
							label: 'Email',
							name: 'email',
							onChangeText(value) {
								console.warn(value)
							}
						},
						{
							label: 'Country',
							name: 'country',
							onChangeText(value) {
								console.warn(value)
							}
						},
						{
							label: 'Phone',
							name: 'phone',
							onChangeText(value) {
								console.warn(value)
							}
						}
					]
				},
				{
					group: 'change password',
					fields: [
						{
							label: 'Old Password',
							name: 'oldPassword',
							type: 'password',
							onChangeText: (value) => {
								console.warn(value)
							}
						},
						{
							label: 'New Password',
							name: 'newPassword',
							type: 'password',
							onChangeText: (value) => {
								this.setState({
									newPassword: value
								})
							}
						},
						{
							label: 'New Password Confirmation',
							name: 'newPasswordConfirmation',
							type: 'password',
							onChangeText: (value) => {
								let error = null
								if (value !== this.state.newPassword) {
									error = {
										newPasswordConfirmation: 'Password Confirmation does not match'
									}
								}
								return error
							}
						}
					]
				}
			],
			validationConfig:validationConfig,
			// make a schema
			validationSchema:validationSchema,
			// make default values
			initialValues:initialValues,
			// image profile
			image:{
				uri: 'https://i.pinimg.com/564x/01/7d/d3/017dd36c244908f6eb97c51e2e4934c6.jpg',
			},
			// socmed connect
			socmed1:{
				label: 'Twitter',
				icon: 'twitter',
				onConnect(value) {
					console.warn(value)
				}
			},
			socmed2:{
				label: 'Google',
				icon: 'google',
				onConnect(value) {
					console.warn(value)
				}
			},
			socmed3:{
				label: 'Facebook',
				icon: 'facebook',
				onConnect(value) {
					console.warn(value)
				}
			},
			// submit
			btnSubmit:{
				label: 'SAVE',
				onPress(value) {
					console.warn(value)
				}
			},
			setting:{
				subHeader:{
					color:'#22619F'
				},
				fieldLabel:{
					fontSize:14,
					color:'#D4D4D4'
				},
				inputValue:{
					color:'#000',
					fontSize: 14,
					textAlign: 'right',
					fontWeight: '500'
				},	
				switch:{
					socmed1:{
						backgroundActive:'#22619F',
						backgroundInactive:'#CACACA',
						circleActiveColor:'#FFFFFF',
						circleInActiveColor:'#FFFFFF',
						innerCircleStyle:{borderColor: '#CACACA'}
					},
					socmed2:{
						backgroundActive:'#22619F',
						backgroundInactive:'#CACACA',
						circleActiveColor:'#FFFFFF',
						circleInActiveColor:'#FFFFFF',
						innerCircleStyle:{borderColor: '#CACACA'}
					},
					socmed3:{
						backgroundActive:'#22619F',
						backgroundInactive:'#CACACA',
						circleActiveColor:'#FFFFFF',
						circleInActiveColor:'#FFFFFF',
						innerCircleStyle:{borderColor: '#CACACA'}
					},
				},
				btnSubmit:{
					colors : [ '#46b6fb', '#2B79C9' ]
				}
			}
		}
		return (
			<ProfileSetting {...options}/>
		)
	}
}
