import React, { Component } from 'react'

import Product1 from '../../../../../rekunative/templates/components/product1'

export default class Product1Example extends Component {
	static navigationOptions = {
		headerTitle: 'PRODUCT'
	}
	render() {
		const options = {
			data:[
				{
					productName: "Apple - Best of Himalaya's",
					price: 169000,
					image: { uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSmtkB-m8zXV7L-be5yaXnBFBfLv-TfN9qMDzlXrUAr9LWu_7p0TA' },
					button: {
						onIncrease: (value) => {
							console.warn(value)
						},
						onDecrease: (value) => {
							console.warn(value)
						}
					},
					weight: '500g',
					description: 'Standard Delivery'
				},
				{
					productName: 'Chicken - Yummy Leg Piece',
					price: 200000,
					image: { uri: 'https://www.africanbites.com/wp-content/uploads/2013/11/IMG_8471.jpg' },
					button: {
						onIncrease: (value) => {
							console.warn(value)
						},
						onDecrease: (value) => {
							console.warn(value)
						}
					},
					weight: '500g',
					description: 'Standard Delivery'
				},
				{
					productName: "Apple - Best of Himalaya's",
					price: 169,
					image: { uri: 'https://www.africanbites.com/wp-content/uploads/2013/11/IMG_8471.jpg' },
					button: {
						onIncrease: (value) => {
							console.warn(value)
						},
						onDecrease: (value) => {
							console.warn(value)
						}
					},
					weight: '500g',
					description: 'Standard Delivery'
				},
				{
					productName: "Apple - Best of Himalaya's",
					price: 169,
					image: { uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSmtkB-m8zXV7L-be5yaXnBFBfLv-TfN9qMDzlXrUAr9LWu_7p0TA' },
					button: {
						onIncrease: (value) => {
							console.warn(value)
						},
						onDecrease: (value) => {
							console.warn(value)
						}
					},
					weight: '500g',
					description: 'Standard Delivery'
				}
			],
			searchInput:{
				label: 'search',
				icon: 'search',
				onChangeText: (value) => {
					console.warn(value)
				}
			},
			setting:{
				image:{
					width: '100%',
					height: 120	
				},
				overlayImage:{
					position: 'absolute',
					top: 0,
					left: 0,
					right: 0,
					height: 120,
					backgroundColor: 'rgba(0,0,0,0.5)',
					zIndex: 10
				},
				viewNumberFormat:{
					position: 'absolute',
					bottom: 10,
					right: 10,
					zIndex: 12,
					flexDirection: 'row',
					alignItems: 'center'
				},
				numberFormat:{
					displayType:'text',
					thousandSeparator:true,
					prefix:'Rp. ',
					color:'white',
				},
				textSlash:{
					fontSize: 10, 
					color: 'white'
				},
				textWeight:{
					fontSize: 10, 
					textAlign: 'left', 
					opacity: 0.8, 
					color: 'white'
				},
				inputNumberQty:{
					textAlign: 'center',
					fontSize: 15,
					color: '#1E1E1E',
					borderWidth: 1,
					borderColor: '#2B93E8',
					height: 30,
					padding: 0
				},
				textDescription:{
					fontSize: 12, 
					textAlign: 'center'
				}
			}
		}
		return (
			<Product1 {...options}/>
		)
	}
}
