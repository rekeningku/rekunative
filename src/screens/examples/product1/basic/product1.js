import React, { Component } from 'react'

import Product1 from '../../../../../rekunative/templates/components/product1'

export default class Product1Example extends Component {
	static navigationOptions = {
		headerTitle: 'PRODUCT'
	}
	
	render() {
		const options = {
			data:[
				{
					productName: "Apple - Best of Himalaya's",
					price: 169000,
					image: { uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSmtkB-m8zXV7L-be5yaXnBFBfLv-TfN9qMDzlXrUAr9LWu_7p0TA' },
					button: {
						onIncrease: (value) => {
							console.log(value)
						},
						onDecrease: (value) => {
							console.log(value)
						}
					},
					weight: '500g',
					description: 'Standard Delivery'
				},
				{
					productName: 'Chicken - Yummy Leg Piece',
					price: 200000,
					image: { uri: 'https://www.africanbites.com/wp-content/uploads/2013/11/IMG_8471.jpg' },
					button: {
						onIncrease: (value) => {
							console.log(value)
						},
						onDecrease: (value) => {
							console.log(value)
						}
					},
					weight: '500g',
					description: 'Standard Delivery'
				},
				{
					productName: "Apple - Best of Himalaya's",
					price: 169,
					image: { uri: 'https://www.africanbites.com/wp-content/uploads/2013/11/IMG_8471.jpg' },
					button: {
						onIncrease: (value) => {
							console.log(value)
						},
						onDecrease: (value) => {
							console.log(value)
						}
					},
					weight: '500g',
					description: 'Standard Delivery'
				},
				{
					productName: "Apple - Best of Himalaya's",
					price: 169,
					image: { uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSmtkB-m8zXV7L-be5yaXnBFBfLv-TfN9qMDzlXrUAr9LWu_7p0TA' },
					button: {
						onIncrease: (value) => {
							console.log(value)
						},
						onDecrease: (value) => {
							console.log(value)
						}
					},
					weight: '500g',
					description: 'Standard Delivery'
				}
			],
			searchInput:{
				label: 'search',
				onChangeText: (value) => {
					console.log(value)
				}
			}
		}
		return (
			<Product1 {...options} />
		)
	}
}
