import React, {Component} from 'react'

import CreditCard from '../../../../../rekunative/templates/components/creditcard'

export default class CreditCardExample extends Component {
    static navigationOptions = {
        headerTitle: 'CREDIT CARD',
    }

    render(){
        const options = {
            data:[
                {
                    name: 'CitiBank',
                    logoUrl: 'https://cdn.freebiesupply.com/logos/large/2x/visa-5-logo-black-and-white.png',
                    accountNumber: '3538 2133 2133 8699',
                    currency: 'USD',
                    branch: 'CINDEE SETON',
                    balance: '$440',
                    date: '07/19',
                },
                {
                    name: 'CitiBank',
                    logoUrl: 'https://cdn.freebiesupply.com/logos/large/2x/visa-5-logo-black-and-white.png',
                    accountNumber: '3538 2133 2133 8699',
                    currency: 'USD',
                    branch: 'CINDEE SETON',
                    balance: '$440',
                    date: '07/19',
                },        
                {
                    name: 'CitiBank',
                    logoUrl: 'https://cdn.freebiesupply.com/logos/large/2x/visa-5-logo-black-and-white.png',
                    accountNumber: '3538 2133 2133 8699',
                    currency: 'USD',
                    branch: 'CINDEE SETON',
                    balance: '$440',
                    date: '07/19',
                }    
            ],
        }

        return(
            <CreditCard {...options}/>
        )
    }
}