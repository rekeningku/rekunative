import React, {Component} from 'react'

import TransactionDetail from '../../../../../rekunative/templates/components/transactiondetail'

export default class TransactionDetailExample extends Component {
    
    static navigationOptions = {
        headerTitle: 'Transaction Detail',
    }

    render(){
        const options = {
            header:{
                title: "Menunggu Pembayaran", //optional
                subtitle: "Berakhir dalam 11.32 AM", //optional
                text: "05 Feb 2019" //optional
            },
            transaction:{
                account:{
                    fullname:'John J Chambers',
                    phone:'870-663-2606',
                    address:'4642  Sunset Drive Pine Bluff Arkansas 71601',
                },
                orders:[
                    {
                        store:'Mairu Indonesia',
                        delivery:'standar',
                        orderPending:true,
                        estimatedTime:'Sel 05 Feb - Jum 08 Feb',
                        orderCode:"243873116900912",
                        name:'Travel Laptop Backpack,Business Anti Theft Slim Durable Laptops Backpack with USB Charging Port,Water Resistant College School Computer Bag for Women & Men Fits 15.6 Inch Laptop and Notebook - Black',
                        imageUrl:'https://www.jakartanotebook.com/images/products/19/252/26067/48/xiaomi-tas-laptop-ransel-minimalis-dark-gray-3.jpg',
                        price:'16.500',
                        qty:1,
                        orderTime:'04 Feb 2019 11:31:52',
                        warranty:false,
                        waitingForPayment:true,
                        expiredDate:'05 Feb 2019',
                        expiredTime:'11:32 AM',
                    },
                ],
                paymentMethod:'Bank transfer',
                subTotal:'160.500',
                shippingCosts:'5.600',
                piece:'5.600',
                total:'160.500',
               
            },
            btnChatSeller:{
                label:'Chat Seller',
                onPress: () => {
                    alert('Chat With Seller')
                }
            },
            btnDetailOrder:{
                onPress: () => {
                    alert('Detail Order')
                }
            },
            btnCancelOrder:{
                label:'BATAL',
                onPress: () =>{
                    alert('Cancel Order')
                }
            }
        }
        return (
            <TransactionDetail {...options} />
        )
    }
}