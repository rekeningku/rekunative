import React, {Component} from 'react'
import {TouchableOpacity} from 'react-native'
import {Icon} from 'native-base'

import Article1 from '../../../../../rekunative/templates/components/article1'

export default class Article1Example extends Component {

    static navigationOptions = {
        headerTitle: 'ARTICLE1',
        headerTitleStyle: {
            alignSelf: 'center',
            textAlign: 'center',
            flex: 1,
            marginRight: '20%'
        },
        headerLeft: (
            <TouchableOpacity style={{
                marginLeft: 10
            }}>
                <Icon name="ios-arrow-back"/>
            </TouchableOpacity>
        )

    }

    render() {
        const options = {
            data: [
                {
                    title: 'Plants Of Our Nature',
                    time: '5 minutes ago',
                    like: 18,
                    comment: 26,
                    user: 5,
                    imageUrl:'https://i.pinimg.com/564x/51/cd/d6/51cdd66f1768b8e380fd1eab073d532f.jpg',
                    onPress: () => {
                        alert('Article1')
                    }
                },
                {
                    title: 'Plants Of Our Nature',
                    time: '23 minutes ago',
                    like: 18,
                    comment: 26,
                    user: 5,
                    imageUrl:'https://i.pinimg.com/564x/ea/00/d3/ea00d3d857fcae5619c714b4dfd2d9ac.jpg',
                    onPress: () => {
                        alert('Article2')
                    }
                },
                {
                    title: 'Sea World',
                    time: '41 minutes ago',
                    like: 18,
                    comment: 26,
                    user: 5,
                    imageUrl:'https://i.pinimg.com/564x/09/88/29/09882930e2a73c3eb036e62e88e9c58b.jpg',
                    onPress: () => {
                        alert('Article3')
                    }
                },
                {
                    title: 'Flowers',
                    time: 'an hour ago',
                    like: 18,
                    comment: 26,
                    user: 5,
                    imageUrl:'https://i.pinimg.com/564x/fc/0a/a4/fc0aa4315545588a7fc6d17f70c24ed5.jpg',
                    onPress: () => {
                        alert('Article4')
                    }
                },
                {
                    title: 'Birds Of Our Planet',
                    time: 'an hour ago',
                    like: 18,
                    comment: 26,
                    user: 5,
                    imageUrl:'https://i.pinimg.com/564x/c5/7d/56/c57d56fc82acbefdfc34d11697ebe231.jpg',
                    onPress: () => {
                        alert('Article5')
                    }
                },
            ],
            
        }

        return (<Article1 {...options}/>)
    }
}
