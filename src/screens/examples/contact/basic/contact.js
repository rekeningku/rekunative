import React, {Component} from 'react'

import Contact from '../../../../../rekunative/templates/components/contact'

export default class ContactExample extends Component {
    static navigationOptions = {
        header: null,
    }

    render(){
        const options = {
            data:[
                {
                    id: 1,
                    name: 'New group',
                    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRRsvQdqRJ9GMBC5fP6X-HmNXmLxoDO-jMa4hu4KvPz8f7oc0_0oA',
                    status: null,
                    onPress: () => {
                        alert('New Group')
                    }
                },
                {
                    id: 2,
                    name: 'New contact',
                    image: 'https://cdn1.iconfinder.com/data/icons/social-messaging-ui-color-round-1/254000/18-512.png',
                    status: null,
                    onPress: () => {
                        alert('New Contact')
                    }
                },
                {
                    id: 3,
                    name: 'Andi',
                    image: 'https://data.junkee.com/wp-content/uploads/2017/04/static1.squarespace.jpg',
                    status:'Im Eating',
                    onPress: () => {
                        alert('Contact')
                    }
                },
                {
                    id: 4,
                    name: 'Budi',
                    image: 'https://data.junkee.com/wp-content/uploads/2017/04/static1.squarespace.jpg',
                    status:'Im Eating',
                    onPress: () => {
                        alert('Contact')
                    }
                },        
                {
                    id: 5,
                    name: 'Charly',
                    image: 'https://data.junkee.com/wp-content/uploads/2017/04/static1.squarespace.jpg',
                    status:'Im Eating',
                    onPress: () => {
                        alert('Contact')
                    }
                }    
            ],
            popUpMenu:[
                { id: 'invite', label: 'Invite a friend' },
                { id: 'contact', label: 'Contacts' },
                { id: 'refresh', label: 'Refresh' },
                { id: 'help', label: 'Help  ' }
            ],
            onPressMenu: (item: PopupMenuItem) => {
                alert('Pressed: ' + item.label)
            },

            filterKey: 'name'
        }

        return(
            <Contact {...options}/>
        )
    }
}