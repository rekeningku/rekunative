import React, {Component} from 'react'

import SignIn1 from '../../../../../rekunative/templates/components/signin1'

class SignIn1Example extends Component {

    static navigationOptions = {
        header: null
    }

    state = {
        email: '',
        password: ''
    }

    render() {
        const options = {
            image: {
                url: 'https://i.ibb.co/TBzjW9h/circle-2x.png',
                height: 190,
                width: 190
            },
            socmed1: {
                name: 'twitter',
                onPress: () => {
                    alert('twitter')
                }
            },
            socmed2: {
                name: 'google',
                onPress: () => {
                    alert('google')
                }
            },
            socmed3: {
                name: 'facebook',
                onPress: () => {
                    alert('facebook')
                }
            },
            field1: {
                label: 'Email',
                validation: {
                    msgRequired: 'Required !',
                    message: 'Email Not Valid!'
                }
            },
            field2: {
                label: 'Password',
                validation: {
                    msgRequired: 'Required !',
                    minChr: 6,
                    message: 'Password must min 6 charcter'
                }
            },
            btnSubmit: {
                label: 'LOGIN',
                onPress: (value) => {
                    this.setState({email: value.email, password: value.password})
                }
            },

            bottom:{
                text:"Don't have an account ?",
                textBold:"Sign up now"
            }
        }

        return (<SignIn1 {...options}/>)
    }
}

export default SignIn1Example