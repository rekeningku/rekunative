import React, {Component} from 'react'
import {View, TouchableOpacity} from 'react-native'
import {Text} from 'native-base'

import SignIn1 from '../../../../../rekunative/templates/components/signin1'

class SignIn1Example extends Component {

    static navigationOptions = {
        header: null
    }

    state = {
        email: '',
        password: ''
    }

    render() {
        const option = {
            image: {
                url: 'https://i.ibb.co/TBzjW9h/circle-2x.png',
                height: 190,
                width: 190
            },
            socmed1: {
                name: 'twitter',
                borderColor:'#3741A8',
                color: '#3741A8',
                onPress: () => {
                    alert('twitter')
                }
            },
            socmed2: {
                name: 'google',
                borderColor:'#3741A8',
                color: '#3741A8',
                onPress: () => {
                    alert('google')
                }
            },
            socmed3: {
                name: 'facebook',
                borderColor:'#3741A8',
                color: '#3741A8',
                onPress: () => {
                    alert('facebook')
                }
            },
            field1: {
                label: 'Email',
                validation: {
                    msgRequired: 'Required !',
                    message: 'Email Not Valid!'
                }
            },
            field2: {
                label: 'Password',
                validation: {
                    msgRequired: 'Required !',
                    minChr: 6,
                    message: 'Password must min 6 charcter'
                }
            },
            btnSubmit: {
                label: 'LOGIN',
                onPress: (value) => {
                    this.setState({email: value.email, password: value.password})
                }
            },
            bottom: 
                <View
                    style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <Text>Dont't have an account ?</Text>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Signup2')}>
                        <Text
                            style={{
                            fontWeight: 'bold',
                            color: 'black',
                            fontWeight: 'bold'
                        }}>
                            Sign up now</Text>
                    </TouchableOpacity>
                </View>
            
        }

        return (<SignIn1 {...option}/>)
    }
}

export default SignIn1Example