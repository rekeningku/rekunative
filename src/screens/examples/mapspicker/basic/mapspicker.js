import React, {Component} from 'react'

import Mapspicker from '../../../../../rekunative/templates/components/mapspicker'

export default class MapsPickerExample extends Component {
    static navigationOptions = {
		header: null
	}

    state = {
        latitude:0,
        longitude:0,
        latitudeDelta:0,
        longitudeDelta:0
    }

    render() {
        const options = {
            data:{
                title:'Mau kemana hari ini?',
                apiKey:'AIzaSyCND4B_7qBkBvwloFSy2gROwrrTCLYE5Jk', //required
                placeholder:'Enter Location',
                iconLeft:{
                    onPress:() => {
                        alert('back')
                    }
                }
            },

            // set api key in android/app/src/main/AndroidManifest.xml
            // <meta-data
            // android:name="com.google.android.geo.API_KEY"
            // android:value="API KEY"/>

            setMaps:(lat,lon,latDelta,lonDelta)=>{
                this.setState({
                    latitude:lat,
                    longitude:lon,
                    latitudeDelta:latDelta,
                    longitudeDelta:lonDelta
                })
                console.log(lat)
            }
        }
        return (<Mapspicker {...options}/>)
    }
}
