import React, {Component} from 'react'
import { Thumbnail,Text, View } from "native-base"
import { StyleSheet } from "react-native"

import Comment from '../../../../../rekunative/templates/components/comment'

export default class CommentExample extends Component {
    static navigationOptions = {
        headerTitle: 'COMMENTS',
    }

    state = { isLike: true, showReply: true, replyId: null, likeId: null, replyFocus: true, isCustom: true,
        textReplyValue: '', textCommentValue: '', 
        //datas comment
        comments: [{
            id: 0,
            name: 'Andi',
            comment: 'Doing what you like will always keep you happy . .',
            image: 'https://data.junkee.com/wp-content/uploads/2017/04/static1.squarespace.jpg',
            time: '3 hrs',
            like: 0,
            isLike: false,
        },
        {
            id: 1,
            name: 'Budi',
            comment: 'Doing what you like will always keep you happy . .',
            image: 'https://data.junkee.com/wp-content/uploads/2017/04/static1.squarespace.jpg',
            time: '3 hrs',
            like: 1,
            isLike: false,
        },        
        {
            id: 2,
            name: 'Charly',
            comment: 'Doing what you like will always keep you happy . .',
            image: 'https://data.junkee.com/wp-content/uploads/2017/04/static1.squarespace.jpg',
            time: '3 hrs',
            like: 1,
            isLike: false,
        }],
        //Datas reply
        replies: [{
            id: 0,
            commentId:2,
            name: 'Andi',
            comment: 'Doing what you like will always keep you happy . .',
            image: 'https://data.junkee.com/wp-content/uploads/2017/04/static1.squarespace.jpg',
            time: '3 hrs',
            like: 0,
            isLike: false,
        },
        {
            id: 1,
            commentId:1,
            name: 'Budi',
            comment: 'Doing what you like will always keep you happy . .',
            image: 'https://data.junkee.com/wp-content/uploads/2017/04/static1.squarespace.jpg',
            time: '3 hrs',
            like: 1,
            isLike: false,
        },        
        {
            id: 2,
            commentId:1,
            name: 'Charly',
            comment: 'Doing what you like will always keep you happy . .',
            image: 'https://data.junkee.com/wp-content/uploads/2017/04/static1.squarespace.jpg',
            time: '3 hrs',
            like: 1,
            isLike: false,
        }]
    }

    render(){
        const options = {
            handleChangeReply: (typedText) => {
                this.setState({ textReplyValue: typedText })
            },
            handleChangeComment: (typedText) => {
                this.setState({ textCommentValue: typedText })
            },

            buttonReplyListener: (id) => {
                this.state.replies.push({
                    id: this.state.replies.length, 
                    name: this.state.comments[0].name,
                    commentId: id,
                    comment: this.state.textReplyValue,
                    image: this.state.comments[0].image,
                    time: '3 hrs',
                    like: 0,
                    isLike: false
                })
                this.setState({})
            },

            buttonCommentListener: () => {
                this.state.comments.push({
                    id: this.state.comments.length, 
                    name: this.state.comments[0].name,
                    comment: this.state.textCommentValue,
                    image: this.state.comments[0].image,
                    time: '3 hrs',
                    like: 0,
                    isLike: false
                })
                this.setState({})
            },
        
            buttonReply : (id) => {
                if (this.state.showReply && this.state.replyId == id) {
                    this.setState({
                        showReply: false,
                        replyId: null,
                        replyFocus: true
                    })
                }
                else {
                    this.setState({
                        showReply: true,
                        replyId: id,
                        replyFocus: true
                    })
                }
            },
            buttonLike: (id) => {
                if (this.state.comments[id].isLike == true) {
                    this.state.comments[id].like -= 1
                    this.state.comments[id].isLike = false
                    this.setState({})
                }
                else {
                    this.state.comments[id].like += 1
                    this.state.comments[id].isLike = true
                    this.setState({})
                }
            },

            showReplies: (id) => {
                return this.state.replies.map(function (item, i) {
                    if (id == item.commentId) {
                        return (
                            <View style={{ flexDirection: 'row', width: '95%', marginBottom :25 }} key={i}>
                                <Thumbnail source={{ uri: item.image }} style={{ height: 30, width: 30, marginRight: 10 }} />
                                <View style={{backgroundColor: '#e0e0eb', borderRadius: 15, width: '90%', height: 65, paddingLeft: 10}}>
                                    <Text style={{fontWeight: 'bold', fontSize: 14}}>{item.name}</Text>
                                    <Text style={{fontSize: 14}}>{item.comment}</Text>
                                    <Text note style={{ marginTop: 5}} >{item.time}</Text>
                                </View>
                                
                            </View>
                        )
                    }
                })
            }
        }

        const styles = StyleSheet.create({
            thumbnail: {
              height: 40,
              width: 40
            },
            view: {
              backgroundColor: "#e0e0eb",
              borderRadius: 15,
              paddingLeft: 10,
              paddingTop: 5,
              paddingBottom: 5,
              height: "auto",
              width: "97%"
            },
            iconLike: {
              fontSize: 15,
              color: "blue"
            },
            thumbnailReply: {
              height: 30,
              width: 30
            },
            buttonReply: {
              elevation: 0,
              height: 40,
              width: 65,
              marginLeft: 5,
              backgroundColor: "#fff",
              borderWidth: 1,
              borderColor: "#d1d1e0"
            },
            textButtonReply: {
              fontSize: 10,
              color: "#b3b3cc"
            },
            thumbnailComment: {
              height: 40,
              width: 40,
              marginLeft: 5
            },
            buttonComment: {
              elevation: 0,
              height: 40,
              width: 60,
              marginLeft: 5,
              marginRight: 5,
              backgroundColor: "#fff",
              borderWidth: 1,
              borderColor: "#d1d1e0"
            },
            footer: {
              backgroundColor: "#fff"
            }
          })

        return(
            <Comment {...options} {...this.state} customStyles={styles}/>
        )
    }
}