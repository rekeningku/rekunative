import React, {Component} from 'react'

import ArticleView1 from '../../../../../rekunative/templates/components/articleview1'

export default class ArticleView1Example extends Component {
    static navigationOptions = {
        headerTitle: 'ARTICLE VIEW',
    }

    render(){
        const options = {
            data: {
                imageUrl: 'https://images.pexels.com/photos/371633/pexels-photo-371633.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                profilePictureUrl: 'https://data.junkee.com/wp-content/uploads/2017/04/static1.squarespace.jpg',
                title: 'Plants Of Our Nature',
                time: '5 Minutes Ago',
                like:19,
                comment:26,
                person:5,
                content: 'How rare it is to come across a new novel as beautifully conceived and finished as this. When much contemporary writing is stuffed to the covers full of incident, narrative trickery and the variously believable wonders of fictive plot, here is a debut that quietens and slows our reading right down to the tick of its own heart.'
            },
            like: {
                icon: 'heart',
                type:'FontAwesome',
                onPress:() =>{
                    alert('love')
                }
            },
            comment: {
                icon: 'comment-o',
                type:'FontAwesome',
                onPress:() =>{
                    alert('comment')
                }
            },
            person: {
                icon: 'user-o',
                type:'FontAwesome',
                onPress:() =>{
                    alert('person')
                }
            },
            setting:{
                title:{
                    color:'#000'
                },
                time:{
                    color:'#000'
                },
                content:{
                    color:'#000'
                },
                imageProfile:{
                    height:40,
                    width:40
                },
                imageArticle:{
                    height:180
                }
            }
        }

        return(
            <ArticleView1 {...options}/>
        )
    }
}