import React, { Component } from 'react'

import Listmenu from '../../../../../rekunative/templates/components/listmenu'

export default class ListMenuExample extends Component {
	static navigationOptions = {
		headerTitle: 'LIST MENU'
	}

	render() {
		const options = {
			data: [
				{
					icon: 'lock-outline',
					type: 'MaterialIcons',
					label: 'auth',
					onPress: () => {
						alert('auth')
					}
				},
				{
					icon: 'person-outline',
					type: 'MaterialIcons',
					label: 'social',
					onPress: () => {
						alert('social')
					}
				},
				{
					icon: 'envelope-o',
					type: 'FontAwesome',
					label: 'message',
					onPress: () => {
						alert('message')
					}
				}
			]
		}

		return <Listmenu {...options} />
	}
}
