import React, {Component} from 'react'
import { View } from 'react-native'

import Carousel2 from '../../../../../rekunative/templates/components/carousel2'

export default class Carousel2Example extends Component {

    static navigationOptions = {
        headerTitle: 'Carousel 2'
    }

    render() {
        const options = {
            carousel:{
                header:{
                    title:'Carousel 2 Example',
                    onPress : () => alert('Header Carousel')
                },
            },
            data: [
                {
                    url: 'https://i.pinimg.com/564x/74/de/e6/74dee6b3072740864027898f3599670e.jpg',
                    title: 'Image 1',
                    color:'white',
                    fontSize:20,
                    onPress:()=>alert('image 1')
                }, 
                {
                    url: 'https://i.pinimg.com/564x/68/4a/2b/684a2b02c209db4d47679ad51effd23a.jpg',
                    title: 'Image 2',
                    color:'white',
                    fontSize:20,
                    onPress:()=>alert('image 2')
                }, 
                {
                    url: 'https://i.pinimg.com/564x/1b/63/d9/1b63d9f34ddf5d034214784ce49c78af.jpg',
                    title: 'Image 3',
                    color:'white',
                    fontSize:20,
                    onPress:()=>alert('image 3')
                },
                {
                    url: 'https://i.pinimg.com/564x/5d/af/d7/5dafd762fafa1d649867d1189c66205d.jpg',
                    title: 'Image 4',
                    color:'white',
                    fontSize:20,
                    onPress:()=>alert('image 4')
                },
                {
                    url: 'https://i.pinimg.com/564x/68/3e/5b/683e5b771c0e1d5e7595142957406d41.jpg',
                    title: 'Image 5',
                    color:'white',
                    fontSize:20,
                    onPress:()=>alert('image 5')
                },
            ]
        }

        return (
            <View style={{flex:1,backgroundColor:'#00A986',paddingVertical:20,paddingHorizontal:10}}>
                <Carousel2 {...options} />
            </View>
        )
    }
}
