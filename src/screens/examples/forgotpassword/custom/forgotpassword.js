import React, { Component } from 'react'
import { Text } from 'native-base'

import ForgotPassword from '../../../../../rekunative/templates/components/forgotpassword'

const validationSchema = {
	title: 'Forgot password',
	description: 'Forgot Password form',
	type: 'object',
	properties: {
		email: {
			description: 'Email of person',
			type: 'string',
			required: true
		}
	}
}

const validationConfig = {
	// for error message
	errMessages: {
		email: {
			pattern: 'Not a valid email address',
			required: 'Required!'
		}
	}
}

export default class ForgotPasswordExample extends Component {
	static navigationOptions = {
		header: null
	}

	render() {
		const options = {
			image:{
				url: { uri: 'https://images.rekeningku.com/icons/safe.png' },
				height: 100,
				width: 100
			},
			title:'Password Recovery',
			btnSubmit:{
				label: 'SEND',
				onPress(value) {
					console.log(value)
				}
			},
			field1:{
				label: 'Email',
				onChangeText(value) {
					console.log(value)
				},
				validationSchema,
				validationConfig
			},
			instructionLabel:(
				<Text
					style={{
						textAlign: 'center',
						color: '#C6C6C6'
					}}
				>
					Enter your email below to receive your password reset and instructions
				</Text>
			)
		}
		return (
			<ForgotPassword {...options}/>
		)
	}
}