import React, { Component } from 'react'

import ForgotPassword from '../../../../../rekunative/templates/components/forgotpassword'

export default class ForgotPasswordExample extends Component {
	static navigationOptions = {
		header: null
	}

	render() {
		const options = {
			image:{
				url: { uri: 'https://images.rekeningku.com/icons/safe.png' },
				height: 100,
				width: 100
			},
			title:'Password Recovery',
			btnSubmit:{
				label: 'SEND',
				onPress(value) {
					console.log(value)
				}
			},
			field1:{
				label: 'Email',
				onChangeText(value) {
					console.log(value)
				},
			},
		}
		return (
			<ForgotPassword {...options}/>
		)
	}
}