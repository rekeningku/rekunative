import React, { Component } from 'react'
import { View , TouchableOpacity } from 'react-native'
import {Text} from 'native-base'

import Signup2 from '../../../../rekunative/templates/components/signup2'

class Signup2Example extends Component {
    state = {
        name: '',
        email: '',
        password:'',
        confirmPassword:''
    }

    static navigationOptions = {
        header: null
    }

    render() {
        const options = {
            image: {
                url: 'https://i.ibb.co/TBzjW9h/circle-2x.png',
                height: 150,
                width: 150
            },
            backgroundColor:'#0A1042',
            inputBorderColor:'#060E3F',
            header:{
                name:'Registration',
                color:'#FAFEFE'
            },
            field1: {
                label: 'Name',
                onChangeText: (val) => {
                    this.setState({
                        name:val
                    })
                }
            },
            field2: {
                label: 'Email',
                onChangeText: (val) => {
                    this.setState({
                        email:val
                    })
                }
            },
            field3: {
                label: 'Password',
                onChangeText: (val) => {
                    this.setState({
                        password:val
                    })
                }
            },
            field4: {
                label: 'Confirm Password',
                onChangeText: (val) => {
                    this.setState({
                        confirmPassword:val
                    })
                }
            },
            btnSubmit: {
                label: 'SIGN UP',
                onPress:()=>{
                    alert('SIGN UP')
                }
            },
            custom: (
                <View style={{
                    flexDirection: 'row',
                    alignItems:'center',
                    justifyContent:'center',
                }}>
                    <Text style={{color:'#FAFEFE'}}>Already have an account ?</Text>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate('Signin1')}>
                        <Text
                            style={{
                            fontWeight: 'bold',
                            fontWeight:'bold',
                            color:'#FAFEFE'
                        }}> Sign In Now</Text>
                    </TouchableOpacity>
                </View>
        
            )
        }

        return (<Signup2 {...options} />)
    }
}

export default Signup2Example