import React, {Component} from 'react'

import Transaction1 from '../../../../../rekunative/templates/components/transaction1'

export default class Transaction1Example extends Component {

    static navigationOptions = {
        headerTitle: 'Transaction1',
    }

    render(){
        const options = {
            transactions:[
                {
                    code:'INV/20181213/XVIII/XII/246622854',
                    date:'13 September 2018',
                    imageStore:'https://i.pinimg.com/564x/f6/a4/cf/f6a4cf1c5cb6060223d6807c38f47390.jpg',  
                    store:'Thinkpad Second',
                    transactionStatus:true,
                    noResi:'GK-133309848'
                },
                {
                    code:'INV/20181210/XVIII/XII/245549448',
                    date:'10 Desember 2018',
                    imageStore:'https://i.pinimg.com/564x/85/0a/07/850a07a205b7e1828e9b5fd11677a5a0.jpg',
                    store:'Jakarta Home Store',
                    transactionStatus:true,
                    noResi:'SD-2-0HY7BBUNJSK7CD3K6YC8'
                },
                {
                    code:'INV/20181207/XVIII/XII/243331403',
                    date:'07 Desember 2018',
                    imageStore:'https://i.pinimg.com/564x/62/60/3c/62603cbeece88d2806b3f7fc853026b5.jpg',
                    store:'Trinity Comp',
                    transactionStatus:false,
                    noResi:''
                },
                {
                    code:'INV/20181119/XVIII/XI/234072811',
                    date:'19 November 2018',
                    imageStore:'https://i.pinimg.com/236x/a5/97/f1/a597f1ac4388272829388f139d2169de.jpg',
                    store:'IvanZ Supplement',
                    transactionStatus:true,
                    noResi:'GK-125592395'
                },
                {
                    code:'INV/20181119/XVIII/XI/230872983',
                    date:'13 November 2018',
                    imageStore:'https://i.pinimg.com/564x/97/54/5d/97545dfe80b74f2cc9560026702f63c8.jpg',
                    store:'iBBi Tekno Jaya',
                    transactionStatus:true,
                    noResi:'GK-125592556'
                },
            ]
        }

        return (
            <Transaction1 {...options}/>
        )
    }
}