import React, {Component} from 'react'

import Setting from '../../../../../rekunative/templates/components/setting'

export default class SettingExample extends Component {
    state = {
        notification:false,
        refresh:false
    }

    static navigationOptions = {
		headerTitle: 'SETTING'
    }

    render(){
        const options = {
            data1:{
                title:{
                    label:'PROFILE SETTINGS',
                },
                field1:{
                    label:'Edit Profile',
                },
                field2:{
                    label:'Change Password',
                },
                field3:{
                    label:'Send Push Notification',
                    onChange: (value) => {
                        this.setState({
                            notification:value    
                        })
                    }
                },
                field4:{
                    label:'Refresh Automatically',
                    onChange: (value) =>{
                        this.setState({
                            refresh:value
                        })
                    }
                },
            },
            data2:{
                title:{
                    name:'FIND FRIENDS',
                },
                field1:{
                    iconLeft:'twitter',
                    label:'Find Firends With Twitter',
                    iconRight:'chevron-right'
                },
                field2:{
                    iconLeft:'google',
                    label:'Find Firends With Google',
                    iconRight:'chevron-right'
                },
                field3:{
                    iconLeft:'facebook',
                    label:'Find Firends With Facebook',
                    iconRight:'chevron-right'
                },
            },
            data3:{
                title:{
                    name:'SUPPORT',
                },
                field1:{
                    label:'Help'
                },
                field2:{
                    label:'Privacy Policy'
                },
                field3:{
                    label:'Terms *& Conditions'
                },
                field4:{
                    label:'Logout'
                }
            },
            setting:{
                title1:{
                    color:'#22619F'
                },
                title2:{
                    color:'#22619F'
                },
                title3:{
                    color:'#22619F'
                },
                socmed1:{
                    activeColor:'#47ABDC'
                },
                socmed2:{
                    activeColor:'#E44339'
                },
                socmed3:{
                    activeColor:'#34578A'
                },
                switch1:{
                    backgroundActive:'#22619F',
                    backgroundInactive:'#CACACA',
                    circleActiveColor:'#FFFFFF',
                    circleInActiveColor:'#FFFFFF',
                    innerCircleStyle:{ borderColor: '#CACACA' }
                },
                switch2:{
                    backgroundActive:'#22619F',
                    backgroundInactive:'#CACACA',
                    circleActiveColor:'#FFFFFF',
                    circleInActiveColor:'#FFFFFF',
                    innerCircleStyle:{ borderColor: '#CACACA' }
                },
            }
        }
        
        return (
            <Setting {...options}/>
        )
    }
}