import React, {Component} from 'react'

import Setting from '../../../../../rekunative/templates/components/setting'

export default class SettingExample extends Component {
    state = {
        notification:false,
        refresh:false
    }

    static navigationOptions = {
		headerTitle: 'SETTING'
    }

    render(){
        const options = {
            data1:{
                title:{
                    label:'PROFILE SETTINGS',
                },
                field1:{
                    label:'Edit Profile',
                },
                field2:{
                    label:'Change Password',
                },
                field3:{
                    label:'Send Push Notification',
                    onChange: (value) => {
                        this.setState({
                            notification:value    
                        })
                    }
                },
                field4:{
                    label:'Refresh Automatically',
                    onChange: (value) =>{
                        this.setState({
                            refresh:value
                        })
                    }
                },
            },
            data2:{
                title:{
                    name:'FIND FRIENDS',
                },
                field1:{
                    iconLeft:'twitter',
                    label:'Find Firends With Twitter',
                    iconRight:'chevron-right'
                },
                field2:{
                    iconLeft:'google',
                    label:'Find Firends With Google',
                    iconRight:'chevron-right'
                },
                field3:{
                    iconLeft:'facebook',
                    label:'Find Firends With Facebook',
                    iconRight:'chevron-right'
                },
            },
            data3:{
                title:{
                    name:'SUPPORT',
                },
                field1:{
                    label:'Help'
                },
                field2:{
                    label:'Privacy Policy'
                },
                field3:{
                    label:'Terms *& Conditions'
                },
                field4:{
                    label:'Logout'
                }
            },
        }
        
        return (
            <Setting {...options}/>
        )
    }
}