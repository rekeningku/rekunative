import React, {Component} from 'react'

import Illustration from '../../../../../rekunative/templates/components/Illustration'

export default class IllustrationExample extends Component {

    static navigationOptions = {
        headerTitle: 'Illustration',
    }

    render(){
        const options = {
            data:{
                text:'404 Not Found',
                image:<Text>Test</Text>, //custom component
                color:'#2B79C9', 
                fontSize:19     
            },
        }

        return (
            <Illustration {...options} />
        )
    }
}