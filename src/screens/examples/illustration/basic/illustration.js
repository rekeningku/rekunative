import React, {Component} from 'react'

import Illustration from '../../../../../rekunative/templates/components/Illustration'

export default class IllustrationExample extends Component {

    static navigationOptions = {
        headerTitle: 'Illustration',
    }

    render(){
        const options = {
            data:{
                text:'404 Not Found',
                image:'https://imgdb.net/images/4888.png',
            },
        }

        return (
            <Illustration {...options} />
        )
    }
}