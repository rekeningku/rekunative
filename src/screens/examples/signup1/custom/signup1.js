import React, {Component} from 'react'
import {TouchableOpacity, View, StyleSheet} from 'react-native';
import {Text} from 'native-base'

import SignUp1 from '../../../../../rekunative/templates/components/signup1'

export default class SignUp1Example extends Component{
    static navigationOptions = {
        header: null,
    }
    
    render(){
        const options = {
            fields: [{
                label: 'Name',
                onChangeText(value) {
                    console.log(value)
                }
            },
            {
                label: 'Email',
                onChangeText(value) {
                    console.log(value)
                }
            },
            {
                label: 'Password',
                onChangeText(value) {
                    console.log(value)
                }
            },
            {
                label: 'Confirm Password',
                onChangeText(value) {
                    console.log(value)
                }
            }],
            
            image:{
                url:'https://i.ibb.co/TBzjW9h/circle-2x.png'
            },
            btnSignUp:{
                label:'Sign Up',
                color1:'#46b6fb',
                color2:'#2B79C9',
                onPress:() =>{
                    alert('Register')
                }
            },
            txtTitle:{
                label:'Registration'
            },

            field1:{
                label: 'Name',
                onChangeText(value) {
                    console.log(value)
                }
            },
            field2:{
                label: 'Email',
                onChangeText(value) {
                    console.log(value)
                }
            },
            field3:{
                label: 'Password',
                onChangeText(value) {
                    console.log(value)
                }
            },
            field4:{
                label: 'Confirm Password',
                onChangeText(value) {
                    console.log(value)
                }
            },
            customInfo:(
                <View style={styles.customView}>
                    <Text>Already have an account ?</Text>
                    <TouchableOpacity  onPress={()=>alert('Login')}>
                        <Text style={styles.customText}> Sign in now</Text>
                    </TouchableOpacity>
                </View>
            )
        } 
        

        const validationSchema = {
            title: 'Signup',
            description: 'Signup setting form',
            type: 'object',
            properties: {
                name: {
                    description: 'Name of person',
                    type: 'string',
                    required: true
                },
                email: {
                    description: 'Email of person',
                    type: 'string',
                    required: true,
                    format: 'email'
                },
                password: {
                    description: 'Password of person',
                    type: 'string',
                    required: true,
                    minLength: 6
                },
                passwordConfirmation: {
                    description: 'Password confirmation of person',
                    type: 'string',
                    required: true
                }
            }
        }

        const validationConfig = {
            // for error message
            errMessages: {
                name: {
                    required: 'Required!'
                },
                email: {
                    required: 'Required!',
                    format: 'Your email not valid!'
                },
                password: {
                    required: 'Required!',
                    min: 'Min 6 character for password'
                },
                passwordConfirmation: {
                    required: 'Required!',
                    matches: 'Your password and confirmation password do not match!'
                }
            }
        }

        return(
            <SignUp1 {...options} config={validationConfig} schema={validationSchema} handleChange={this.handleChange}/>
        )
    }
}

const styles = StyleSheet.create({
    customView: {
        flexDirection: 'row',
        alignItems:'center',
        justifyContent:'center',
        marginTop:15
    },
    customText: {
        fontWeight: 'bold',
        color: 'black',
        fontWeight:'bold'
    }
})
