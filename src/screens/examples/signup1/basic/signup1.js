import React, {Component} from 'react'

import SignUp1 from '../../../../../rekunative/templates/components/signup1'

export default class SignUp1Example extends Component{
    static navigationOptions = {
        header: null,
    }
    
    render(){
        const options = {
            fields: [{
                label: 'Name',
                onChangeText(value) {
                    console.log(value)
                }
            },
            {
                label: 'Email',
                onChangeText(value) {
                    console.log(value)
                }
            },
            {
                label: 'Password',
                onChangeText(value) {
                    console.log(value)
                }
            },
            {
                label: 'Confirm Password',
                onChangeText(value) {
                    console.log(value)
                }
            }],
            
            image:{
                url:'https://i.ibb.co/TBzjW9h/circle-2x.png'
            },
            btnSignUp:{
                label:'Sign Up',
                onPress:() =>{
                    alert('Register')
                }
            },
            txtTitle:{
                label:'Registration'
            },

            field1:{
                label: 'Name',
                onChangeText(value) {
                    console.log(value)
                }
            },
            field2:{
                label: 'Email',
                onChangeText(value) {
                    console.log(value)
                }
            },
            field3:{
                label: 'Password',
                onChangeText(value) {
                    console.log(value)
                }
            },
            field4:{
                label: 'Confirm Password',
                onChangeText(value) {
                    console.log(value)
                }
            },
            customInfo:{
                text:'Already have an account ?',
                textBold:'Sign in now'
            }
        } 

        return(
            <SignUp1 {...options} handleChange={this.handleChange}/>
        )
    }
}