import React, {Component} from 'react'
import {Container, Content, Text} from 'native-base'

export default class ContactExample extends Component {

    render(){
        return (
            <Container>
                <Content>
                    <Text>ContactExample</Text>
                </Content>
            </Container>
        )
    }
}