import React, { Component } from 'react'
import { processColor } from 'react-native'
import { View, Text, H3 } from 'native-base'

import Dashboard from '../../../../../rekunative/templates/components/dashboard1'

const petrel = 'rgb(59, 145, 153)'

export default class ContactExample extends Component {
	static navigationOptions = {
		headerTitle: 'DASHBOARD'
	}

	render() {
		const options = {
			socmed1:{
				label: 'Stars',
				number: 4512,
				icon: 'github',
				onPress() {
					console.log('socmed1')
				}
			},
			socmed2:{
				label: 'Tweets',
				number: 2256,
				icon: 'twitter',
				onPress() {
					console.log('socmed1')
				}
			},
			socmed3:{
				label: 'Likes',
				number: 1124,
				icon: 'facebook',
				onPress() {
					console.log('socmed1')
				}
			},
			pieChart:{
				index: 0,
				label: 'AUDIENCE OVERVIEW',
				data: {
					dataSets: [
						{
							values: [
								{ value: 45, label: 'Likes' },
								{ value: 21, label: 'Comments' },
								{ value: 15, label: 'Share' },
								{ value: 9, label: 'People' }
							],
							label: '',
						},
					]
				},
				description: {
					text: 'This is Pie chart description',
				}
			},
			lineChart:{
				index: 1,
				label: 'REAL TIME VISITORS',
				data: {
					dataSets: [
						{
							values: [ 10, 20, 38, 26, 89, 10 ],
						}
					]
				}
			},
			progressChart:{
				index: 2,
				label: {
					title:'REACH',
					number:'1 500 356',
					small:'+6 per day in average'
				},
				data: {
					dataSets: [
						{
							values: [ { value: 45, label: 'Visitor' }, { value: 20, label: '' } ],
						}
					]
				},
				description: {
					text: '',
				}
			}

		}
		return (
			<Dashboard {...options}/>
		)
	}
}
