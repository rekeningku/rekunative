import React, { Component } from 'react'
import { processColor } from 'react-native'
import { View, Text, H3 } from 'native-base'

import Dashboard from '../../../../../rekunative/templates/components/dashboard1'

const petrel = 'rgb(59, 145, 153)'

export default class ContactExample extends Component {
	static navigationOptions = {
		headerTitle: 'DASHBOARD'
	}

	render() {
		const options = {
			socmed1:{
				label: 'Stars',
				number: 4512,
				icon: 'github',
				backgroundColor: '#2AB5FA',
				color: 'white',
				onPress() {
					console.log('socmed1')
				}
			},
			socmed2:{
				label: 'Tweets',
				number: 2256,
				icon: 'twitter',
				backgroundColor: '#FFC61C',
				color: 'white',
				onPress() {
					console.log('socmed1')
				}
			},
			socmed3:{
				label: 'Likes',
				number: 1124,
				icon: 'facebook',
				backgroundColor: '#5468FF',
				color: 'white',
				onPress() {
					console.log('socmed1')
				}
			},
			pieChart:{
				index: 0,
				label: 'AUDIENCE OVERVIEW',
				legend: {
					enabled: true,
					textSize: 10,
					form: 'CIRCLE',
					horizontalAlignment: 'LEFT',
					verticalAlignment: 'BOTTOM',
					orientation: 'HORIZONTAL',
					wordWrapEnabled: true
				},
				data: {
					dataSets: [
						{
							values: [
								{ value: 45, label: 'Likes' },
								{ value: 21, label: 'Comments' },
								{ value: 15, label: 'Share' },
								{ value: 9, label: 'People' }
							],
							label: '',
							config: {
								colors: [
									processColor('#C0FF8C'),
									processColor('#FFF78C'),
									processColor('#FFD08C'),
									processColor('#8CEAFF'),
									processColor('#FF8C9D')
								],
								valueTextSize: 20,
								valueTextColor: processColor('white'),
								sliceSpace: 5,
								valueFormatter: "#.#'%'",
								valueLineColor: processColor('green')
							}
						}
					]
				},
			},
			lineChart:{
				index: 1,
				label: 'REAL TIME VISITORS',
				data: {
					dataSets: [
						{
							values: [ 10, 20, 38, 26, 89, 10 ],
							label: 'Line',
							config: {
								// mode: 'CUBIC_BEZIER',
								drawValues: false,
								lineWidth: 2,
								drawCircles: true,
								circleColor: processColor(petrel),
								drawCircleHole: false,
								circleRadius: 5,
								highlightColor: processColor('transparent'),
								color: processColor(petrel),
								drawFilled: true,
								fillGradient: {
									colors: [ processColor('#EBF6FF'), processColor('#EBF6FF') ],
									positions: [ 0, 0.5 ],
									angle: 90,
									orientation: 'TOP_BOTTOM'
								},
								fillAlpha: 1000,
								valueTextSize: 15
							}
						}
					]
				}
			},
			progressChart:{
				index: 2,
				label: (
					<View>
						<H3 style={{ fontWeight: 'bold' }}>REACH</H3>
						<Text>1 500 356</Text>
						<Text style={{ fontSize: 10 }}>+6 per day in average</Text>
					</View>
				),
				legend: {
					enabled: true,
					textSize: 15,
					form: 'CIRCLE',

					horizontalAlignment: 'RIGHT',
					verticalAlignment: 'TOP',
					orientation: 'VERTICAL',
					wordWrapEnabled: true
				},
				data: {
					dataSets: [
						{
							values: [ { value: 45, label: 'Visitor' }, { value: 20, label: '' } ],
							label: '',
							config: {
								colors: [
									processColor('#C0FF8C'),
									processColor('#FFFFFF'),
									processColor('#FFD08C'),
									processColor('#8CEAFF'),
									processColor('#FF8C9D')
								],
								valueTextSize: 20,
								valueTextColor: processColor('white'),
								// xValuePosition: "OUTSIDE_SLICE",
								// yValuePosition: 'OUTSIDE_SLICE',
								valueFormatter: "#.#'%'",
								valueLineColor: processColor('green')
							}
						}
					]
				},
				highlights: [ { x: 2 } ],
				description: {
					text: '',
					textSize: 15,
					textColor: processColor('darkgray')
				}
			}

		}
		return (
			<Dashboard {...options}/>
		)
	}
}
