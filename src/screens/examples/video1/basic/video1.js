import React, {Component} from 'react'
import Video1 from '../../../../../rekunative/templates/components/video1'

export default class Video1Example extends Component {
    
    static navigationOptions = {
		headerTitle: 'Video1'
    }
    
    render() {
        const options = {
            data:[
                {
                    name:'Rekeningku DotCom',
                    viewers:'242x',
                    date:'2 Apr 2018',
                    title:'Mining Digital Asset terbesar di Indonesia',
                    videoUrl:'https://www.youtube.com/watch?v=FaBzudGS0Hw',
                    imageProfile:'https://yt3.ggpht.com/a-/AAuE7mA8dTkctgJK3RmOO2zkKV48xHWEPlL0-hEGdg=s288-mo-c-c0xffffffff-rj-k-no',
                },
                {
                    name:'Rekeningku DotCom',
                    viewers:'15x',
                    date:' 17 Jan 2019',
                    title:'Rekeningku.com - Cryptocurrency Marketplace Indonesia',
                    videoUrl:'https://www.youtube.com/watch?v=oVi-DyZAWz8',
                    imageProfile:'https://yt3.ggpht.com/a-/AAuE7mA8dTkctgJK3RmOO2zkKV48xHWEPlL0-hEGdg=s288-mo-c-c0xffffffff-rj-k-no',
                },
            ],
            popUpMenu:[
                {id:'tidakTertarik',label:'Tidak Tertarik'},
                {id:'simpanTontonNanti',label:'Simpan ke Tonton Nanti'},
                {id:'simpanPlaylist',label:'Simpan ke playlist'},
                {id:'download',label:'Download'},
                {id:'bagikan',label:'Bagikan'},
                {id:'laporkan',label:'Laporkan'},
            ],
            onPressMenu:(item) => {
                alert(item.label)
            }
        }
        return (
            <Video1 {...options}/>
        )
    }
}
