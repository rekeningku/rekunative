import React, {Component} from 'react'
import {View} from 'react-native'

import Carousel1 from '../../../../../rekunative/templates/components/carousel1'

export default class Carousel1Example extends Component {

    static navigationOptions = {
        headerTitle: 'Carousel 1'
    }

    render() {
        const options = {
           carousel:{
                header: {
                    title: 'Carousel 1 Example',
                    color: '#fff',
                    fontSize:20,
                    fontWeight:'bold',
                    iconRight: 'chevron-right',
                    iconSize:20,
                    iconType:'Entypo',  //https://oblador.github.io/react-native-vector-icons/
                    onPress: () => {
                        alert('Test')
                    }
                },
                setting:{
                    width:640,                      
                    height:200,                     
                    spinnerColor:'#2B79C9',         
                    showSpinner:true,               
                    resizeMode:'cover',             
                    activeAnimationType:'spring',
                    inactiveSlideScale:8,
                    enableMomentum:true,
                    activeSlideAlignment:'start',
                    inactiveSlideOpacity:1,
                    loop:true,
                    activeAnimationOptions:{
                        friction: 4,
                        tension: 40
                    }
                },
           },
           data: [
                {
                    url: 'https://i.pinimg.com/564x/74/de/e6/74dee6b3072740864027898f3599670e.jpg',
                    onPress: () => alert('Image 1')
                }, {
                    url: 'https://i.pinimg.com/564x/68/4a/2b/684a2b02c209db4d47679ad51effd23a.jpg',
                    onPress: () => alert('Image 2')

                }, {
                    url: 'https://i.pinimg.com/564x/1b/63/d9/1b63d9f34ddf5d034214784ce49c78af.jpg',
                    onPress: () => alert('Image 3')

                }, {
                    url: 'https://i.pinimg.com/564x/5d/af/d7/5dafd762fafa1d649867d1189c66205d.jpg',
                    onPress: () => alert('Image 4')

                }, {
                    url: 'https://i.pinimg.com/564x/68/3e/5b/683e5b771c0e1d5e7595142957406d41.jpg',
                    onPress: () => alert('Image 5')

                }
            ]
        }

        return (
            <View
                style={{
                flex: 1,
                paddingTop: 30,
                paddingHorizontal: 10,
                backgroundColor: '#17B695'
            }}>
                <Carousel1 {...options}/>
            </View>
        )
    }
}
