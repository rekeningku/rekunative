import React, {Component} from 'react'

import Sidemenu from '../../../../../rekunative/templates/components/sidemenu'

export default class SideMenuExample extends Component {

    static navigationOptions = {
		headerTitle: 'SIDE MENU'
    }

    render(){
        const options = {
            data:[
                {
                    name:'Auth',
                    iconLeft:'lock',
                    onPress:() => {
                        alert('Auth')
                    }
                },
                {
                    name:'Social',
                    iconLeft:'user-circle',
                    onPress:() => {
                        alert('social')
                    }
                },
                {
                    name:'Articles',
                    iconLeft:'edit',
                    onPress:() => {
                        alert('articles')
                    }
                },
                {
                    name:'Forgot Password',
                    iconLeft:'key',
                    onPress:() => {
                        alert('articles')
                    }
                },
                {
                    name:'Dashboard',
                    iconLeft:'home',
                    onPress:() => {
                        alert('articles')
                    }
                },
                {
                    name:'Notification',
                    iconLeft:'bookmark',
                    onPress:() => {
                        alert('articles')
                    }
                },
                {
                    name:'Comments',
                    iconLeft:'comments',
                    onPress:() => {
                        alert('articles')
                    }
                },
                {
                    name:'Profile',
                    iconLeft:'github-alt',
                    onPress:() => {
                        alert('articles')
                    }
                },
                {
                    name:'Setting',
                    iconLeft:'gears',
                    onPress:() => {
                        alert('articles')
                    }
                },
            ]
        }

        return (
            <Sidemenu {...options}/>
        )
    }
}