import React, {Component} from 'react'

import Sidemenu from '../../../../../rekunative/templates/components/sidemenu'

export default class SideMenuExample extends Component {

    static navigationOptions = {
		headerTitle: 'SIDE MENU'
    }

    render(){
        const options = {
            data:[
                {
                    name:'Auth',
                    iconLeft:'lock',
                    iconRight:'chevron-right',
                    color:'#22619F',
                    onPress:() => {
                        alert('Auth')
                    }
                },
                {
                    name:'Social',
                    iconLeft:'user-circle',
                    iconRight:'chevron-right',
                    color:'#22619F',
                    onPress:() => {
                        alert('social')
                    }
                },
                {
                    name:'Articles',
                    iconLeft:'edit',
                    iconRight:'chevron-right',
                    color:'#22619F',
                    onPress:() => {
                        alert('articles')
                    }
                },
                {
                    name:'Forgot Password',
                    iconLeft:'key',
                    iconRight:'chevron-right',
                    color:'#22619F',
                    onPress:() => {
                        alert('articles')
                    }
                },
                {
                    name:'Dashboard',
                    iconLeft:'home',
                    iconRight:'chevron-right',
                    color:'#22619F',
                    onPress:() => {
                        alert('articles')
                    }
                },
                {
                    name:'Notification',
                    iconLeft:'bookmark',
                    iconRight:'chevron-right',
                    color:'#22619F',
                    onPress:() => {
                        alert('articles')
                    }
                },
                {
                    name:'Comments',
                    iconLeft:'comments',
                    iconRight:'chevron-right',
                    color:'#22619F',
                    onPress:() => {
                        alert('articles')
                    }
                },
                {
                    name:'Profile',
                    iconLeft:'github-alt',
                    iconRight:'chevron-right',
                    color:'#22619F',
                    onPress:() => {
                        alert('articles')
                    }
                },
                {
                    name:'Setting',
                    iconLeft:'gears',
                    iconRight:'chevron-right',
                    color:'#22619F',
                    onPress:() => {
                        alert('articles')
                    }
                },
            ]
        }

        return (
            <Sidemenu {...options}/>
        )
    }
}