import {
    Article1Basic,
    ArticleView1Basic,
    ArticleView1Custom,
    Carousel1Basic,
    Carousel1Custom,
    Carousel2Basic,
    Carousel2Custom,
    CommentBasic,
    ContactBasic,
    ContactCustom,
    CreditCardBasic,
    CreditCardCustom,
    Dashboard1Basic,
    Dashboard1Custom,
    ForgotPasswordBasic,
    ForgotPasswordCustom,
    GridMenu1Basic,
    GridMenu2Basic,
    Home,
    IllustrationBasic,
    ListMenuBasic,
    Login,
    MapsPickerBasic,
    NotificationBasic,
    NotificationCustom,
    OnBoarding1Basic,
    OnBoarding1Custom,
    Product1Basic,
    Product1Custom,
    Profile1Basic,
    Profile2,
    ProfileSettingBasic,
    ProfileSettingCustom,
    SettingBasic,
    SettingCustom,
    SideMenuBasic,
    SideMenuCustom,
    SignIn1Basic,
    SignIn1Custom,
    SignIn2Basic,
    SignUp1Basic,
    SignUp1Custom,
    SignUp2,
    TodoAdd,
    TodoList,
    Transaction1Basic,
    TransactionDetail,
    Video1Basic,
    VideoDetailBasic
} from '../screens'
import {createStackNavigator} from 'react-navigation'

const initialRouteName = 'Video1Basic'

const AppNavigator = createStackNavigator({
	Article1Basic: {
		screen: Article1Basic
	},
	ArticleView1Basic: {
		screen: ArticleView1Basic
	},
	ArticleView1Custom: {
		screen: ArticleView1Custom
	},
	Carousel1Basic: {
		screen: Carousel1Basic
	},
	Carousel1Custom: {
		screen: Carousel1Custom
	},
	Carousel2Basic: {
		screen: Carousel2Basic
	},
	Carousel2Custom: {
		screen: Carousel2Custom
	},
	CommentBasic: {
		screen: CommentBasic
	},
	ContactBasic: {
		screen: ContactBasic
	},
	ContactCustom: {
		screen: ContactCustom
	},
	CreditCardBasic: {
		screen: CreditCardBasic
	},
	CreditCardCustom: {
		screen: CreditCardCustom
	},
	Dashboard1Basic: {
		screen: Dashboard1Basic
	},
	Dashboard1Custom: {
		screen: Dashboard1Custom
	},
	ForgotPasswordBasic: {
		screen: ForgotPasswordBasic
	},
	ForgotPasswordCustom: {
		screen: ForgotPasswordCustom
	},
	GridMenu1Basic: {
		screen: GridMenu1Basic
	},
	GridMenu2Basic: {
		screen: GridMenu2Basic
	},
	Home: {
		screen: Home
	},
	IllustrationBasic: {
		screen: IllustrationBasic
	},
	ListMenuBasic: {
		screen: ListMenuBasic
	},
	Login: {
		screen: Login
	},
	MapsPickerBasic: {
		screen: MapsPickerBasic
	},
	NotificationBasic: {
		screen: NotificationBasic
	},
	NotificationCustom: {
		screen: NotificationCustom
	},
	OnBoarding1Basic: {
		screen: OnBoarding1Basic
	},
	OnBoarding1Custom: {
		screen: OnBoarding1Custom
	},
	Product1Basic: {
		screen: Product1Basic
	},
	Product1Custom: {
		screen: Product1Custom
	},
	Profile1Basic: {
		screen: Profile1Basic
	},
	Profile2: {
		screen: Profile2
	},
	ProfileSettingBasic: {
		screen: ProfileSettingBasic
	},
	ProfileSettingCustom: {
		screen: ProfileSettingCustom
	},
	SettingBasic: {
		screen: SettingBasic
	},
	SettingCustom: {
		screen: SettingCustom
	},
	SideMenuBasic: {
		screen: SideMenuBasic
	},
	SideMenuCustom: {
		screen: SideMenuCustom
	},
	SignIn1Basic: {
		screen: SignIn1Basic
	},
	SignIn1Custom: {
		screen: SignIn1Custom
	},
	SignIn2Basic: {
		screen: SignIn2Basic
	},
	SignUp1Basic: {
		screen: SignUp1Basic
	},
	SignUp1Custom: {
		screen: SignUp1Custom
	},
	SignUp2: {
		screen: SignUp2
	},
	TodoAdd: {
		screen: TodoAdd
	},
	TodoList: {
		screen: TodoList
	},
	Transaction1Basic: {
		screen: Transaction1Basic
	},
	TransactionDetail: {
		screen: TransactionDetail
	},
	Video1Basic: {
		screen: Video1Basic
	},
	VideoDetailBasic: {
		screen: VideoDetailBasic
	}
}, {
	initialRouteName
})

export default AppNavigator
