import React, { Component } from 'react'
import { Text, Container, Content, Grid, Row, View, Left, Body, Card, CardItem, Icon, Button, Col, Item, Input, Title } from 'native-base'
import { StyleSheet, FlatList, Dimensions, Image } from 'react-native'
import NumberFormat from 'react-number-format'
import _ from 'lodash'

const widthCard = (Dimensions.get('window').width - 30) / 2

class ProductItem extends Component {
	state = {
		value: 0
	}

	increase = () => {
		this.setState({
			value: this.state.value + 1
		})
		this.props.item.button.onIncrease(this.state.value + 1)
	}

	decrease = () => {
		value = this.state.value > 0 ? this.state.value - 1 : 0
		this.setState({
			value
		})
		this.props.item.button.onDecrease(value)
	}

	render() {
		const { item,setting } = this.props
		return (
			<Card style={styles.card}>
				<CardItem cardBody style={{ padding: 10, paddingBottom: 5, justifyContent: 'center', alignItems: 'center' }}>
					<Grid>
						<Row style={{ width: '100%' }}>
							<Text note style={{ fontWeight: '600', fontSize: 14, height: 40, textAlign: 'center' }}>
								{item.productName}
							</Text>
						</Row>
					</Grid>
				</CardItem>
				<CardItem cardBody style={[ styles.cardItem, { position: 'relative' } ]}>
					<View
						style={!setting ? {
							position: 'absolute',
							top: 0,
							left: 0,
							right: 0,
							height: 120,
							backgroundColor: 'rgba(0,0,0,0.5)',
							zIndex: 10
						}:setting.overlayImage}
					/>
					<Image
						source={item.image}
						style={!setting ? {
							width: '100%',
							height: 120
						}:setting.image}
					/>
					<View
						style={!setting ? {
							position: 'absolute',
							bottom: 10,
							right: 10,
							zIndex: 12,
							flexDirection: 'row',
							alignItems: 'center'
						} : setting.viewNumberFormat}
					>
						<NumberFormat
							value={item.price}
							displayType={setting ? setting.numberFormat.displayType : 'text'}
							thousandSeparator={setting ? setting.numberFormat.thousandSeparator : true}
							prefix={setting ? setting.numberFormat.prefix : 'Rp. '}
							renderText={(value) => (
								<Text note style={{ color: setting ? setting.numberFormat.color : 'white' }}>
									{value}
								</Text>
							)}
						/>
						<Text note style={setting ? setting.textSlash : { fontSize: 10, color: 'white' }}>
							{' '}
							/
						</Text>
						<Text note style={setting ? setting.textWeight : { fontSize: 10, textAlign: 'left', opacity: 0.8, color: 'white' }}>
							{item.weight}
						</Text>
					</View>
				</CardItem>
				<CardItem cardBody style={[ styles.cardItem, { paddingBottom: 0, paddingHorizontal: 10 } ]}>
					<Grid>
						<Row style={{ marginTop: 5 }}>
							<Button
								style={[
									styles.button,
									{
										borderTopRightRadius: 0,
										borderBottomRightRadius: 0
									}
								]}
								small
								onPress={this.decrease}
							>
								<Icon name='minus' type='FontAwesome5' style={{ marginLeft: 0, marginRight: 0, fontSize: 10 }} />
							</Button>
							<Input
								style={!setting ? {
									textAlign: 'center',
									fontSize: 15,
									color: '#1E1E1E',
									borderWidth: 1,
									borderColor: '#2B93E8',
									height: 30,
									padding: 0
								}:setting.inputNumberQty}
								value={this.state.value.toString()}
							/>
							<Button
								style={[
									styles.button,
									{
										borderTopLeftRadius: 0,
										borderBottomLeftRadius: 0
									}
								]}
								small
								onPress={this.increase}
							>
								<Icon name='plus' type='FontAwesome5' style={{ marginLeft: 0, marginRight: 0, fontSize: 10 }} />
							</Button>
						</Row>
						<Row style={{ width: '100%', justifyContent: 'center' }}>
							<Text note style={!setting ? { fontSize: 12, textAlign: 'center' } : setting.description}>
								{item.description}
							</Text>
						</Row>
					</Grid>
				</CardItem>
				<CardItem cardBody style={[ styles.cardItem, { padding: 10, paddingTop: 0, justifyContent: 'flex-end' } ]} />
			</Card>
		)
	}
}

class Product1 extends Component {
	renderItem = (props) => <ProductItem item={props.item} setting={this.props.setting} />

	onSearch = _.debounce((value) => {
		this.props.searchInput.onChangeText(value)
	}, 800)

	render() {
		return (
			<Container style={styles.root}>
				<Content>
					<Grid>
						<Row style={styles.productSubHeader}>
							<Left>
								<Title small style={{ color: 'white', fontSize: 15 }}>
									{this.props.data.length} Products Found
								</Title>
							</Left>
							<Body>
								<Item rounded style={{ height: 30 }}>
									<Icon type='FontAwesome5' name={this.props.searchInput.icon ? this.props.searchInput.icon : 'search'} style={{ color: 'white', fontSize: 16 }} />
									<Input
										style={{ padding: 0, fontSize: 12, color: 'white', paddingLeft: 5 }}
										placeholderTextColor='#fefefe'
										placeholder={this.props.searchInput.label ? this.props.searchInput.label : 'Search'}
										onChangeText={this.onSearch}
									/>
								</Item>
							</Body>
						</Row>
						<Row style={{ padding: 10, paddingBottom: 0 }}>
							<Grid>
								<FlatList
									numColumns={2}
									data={this.props.data}
									renderItem={this.renderItem}
									keyExtractor={(item, key) => item.productName + key}
									columnWrapperStyle={{
										justifyContent: 'space-between',
										alignItems: 'center'
									}}
								/>
							</Grid>
						</Row>
					</Grid>
				</Content>
			</Container>
		)
	}
}

const styles = StyleSheet.create({
	root: {
		backgroundColor: '#EEEEEE'
	},
	productSubHeader: {
		backgroundColor: '#2B93E8',
		padding: 10
	},
	// card

	card: {
		marginTop: 0,
		marginLeft: 0,
		marginBottom: 10,
		marginRight: 0,
		width: widthCard
	},
	cardItem: {
		paddingTop: 0,
		paddingBottom: 5,
		justifyContent: 'center'
	},
	button: {
		alignSelf: 'center',
		justifyContent: 'center',
		width: 40,
		borderRadius: 15,
		backgroundColor: '#2B93E8'
	}
})

export default Product1
