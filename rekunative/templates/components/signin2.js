import React, { Component } from 'react'
import { Image, StyleSheet, Dimensions, TouchableOpacity, StatusBar } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Text, Form, Item, Input, Icon, Button, Grid, Col, Row, View } from 'native-base'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

let ScreenHeight = Dimensions.get('window').height - StatusBar.currentHeight

const DefaultCustom = () => (
	<View style={styles.bottomTextWrapper}>
		<Text>Don't have an account?</Text>
		<TouchableOpacity style={styles.buttonText}>
			<Text style={styles.signupText}> Sign up now</Text>
		</TouchableOpacity>
	</View>
)

class Signin extends Component {
	static navigationOptions = {
		header: null
	}

	render() {
		return (
			<KeyboardAwareScrollView>
				<Grid style={styles.root}>
					<Row style={styles.logoContainer}>
						<Col>
							<Image
								source={
									this.props.image.url || {
										uri: 'https://i.ibb.co/TBzjW9h/circle-2x.png'
									}
								}
								style={[
									styles.logo,
									{
										height: this.props.image.height || 190,
										width: this.props.image.width || 190
									}
								]}
							/>
						</Col>
					</Row>
					<Row style={styles.formContainer}>
						<Col>
							<Form>
								<Item regular last style={styles.input} bordered>
									<Input placeholder={this.props.field1.label || 'Email'} onChangeText={this.props.field1.onChangeText} />
								</Item>
								<Item regular last style={styles.input} bordered>
									<Input
										placeholder={this.props.field2.label || 'Password'}
										onChangeText={this.props.field2.onChangeText}
										secureTextEntry
									/>
								</Item>
								<LinearGradient colors={[ '#2B93E8', '#1959A1' ]} start={{ x: 0, y: 0 }} end={{ x: 1, y: 0 }} style={styles.gradient}>
									<Button
										full
										style={{
											elevation: 0,
											backgroundColor: 'transparent'
										}}
										onPress={this.props.btnSubmit.onSubmit}
									>
										<Text>{this.props.btnSubmit.label || 'Login'}</Text>
									</Button>
								</LinearGradient>
							</Form>
							<Row style={styles.socialContainer}>
								<View style={styles.socialWrapper}>
									<Button rounded transparent style={styles.social} onPress={this.props.socmed1.onPress}>
										<Icon name={this.props.socmed1.name || 'twitter'} type='FontAwesome' />
									</Button>
									<Button rounded transparent style={styles.social} onPress={this.props.socmed2.onPress}>
										<Icon name={this.props.socmed2.name || 'google'} type='FontAwesome' />
									</Button>
									<Button rounded transparent style={styles.social} onPress={this.props.socmed3.onPress}>
										<Icon name={this.props.socmed3.name || 'facebook'} type='FontAwesome' />
									</Button>
								</View>
								{this.props.custom || <DefaultCustom />}
							</Row>
						</Col>
					</Row>
				</Grid>
			</KeyboardAwareScrollView>
		)
	}
}

const styles = StyleSheet.create({
	root: {
		flex: 1,
		height: ScreenHeight
	},
	logoContainer: {
		justifyContent: 'center',
		alignItems: 'center',
		paddingVertical: 10,
		height: ScreenHeight * 0.4
	},
	logo: {
		width: '70%',
		alignSelf: 'center',
		height: 60
	},
	ana: {
		width: 80,
		height: 80,
		alignSelf: 'center'
	},
	formContainer: {
		paddingHorizontal: 20,
		paddingVertical: 10,
		height: ScreenHeight * 0.6
	},
	input: {
		borderRadius: 40,
		marginBottom: 20,
		height: 50,
		shadowOffset: {
			width: 0,
			height: 1
		},
		shadowOpacity: 0.2,
		shadowRadius: 1.41,
		elevation: 2
	},
	button: {
		marginBottom: 20,
		borderRadius: 40
	},
	gradient: {
		borderRadius: 40,
		justifyContent: 'center',
		alignItems: 'center',
		overflow: 'hidden',
		marginBottom: 15,
		shadowOffset: {
			width: 0,
			height: 1
		},
		shadowOpacity: 0.2,
		shadowRadius: 1.41,
		elevation: 3
	},
	text: {
		fontSize: 20,
		color: '#fff'
	},
	socialContainer: {
		flexDirection: 'column'
	},
	socialWrapper: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-evenly'
	},
	social: {
		color: '#2784D6',
		borderRadius: 60,
		height: 60,
		width: 60,
		borderWidth: 1,
		borderColor: '#2784D6',
		justifyContent: 'center',
		alignItems: 'center'
	},
	bottomTextWrapper: {
		flexDirection: 'row',
		justifyContent: 'center',
		height: '50%',
		paddingVertical: 10
	},
	signupText: {
		fontWeight: 'bold',
		color: '#000'
	}
})

export default Signin
