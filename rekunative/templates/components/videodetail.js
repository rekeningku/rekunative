import PropTypes from 'prop-types'
// import Video from 'react-native-video'

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import VideoPlayer from 'react-native-video-controls';

export default class Video extends Component {
  renderVideoPlayer(url) {
    return (
      <VideoPlayer
        source={{ uri: url }}
        navigator={this.props.navigator}
        subtitle={this.props.subtitle}
        repeat={true}
      />
    )
  }

  render () {
    const { videoUrl, height, width } = this.props
    return (
      <View style={{height: height, width: width}}>
        {this.renderVideoPlayer(videoUrl)}
      </View>
    )
  }
}

var styles = StyleSheet.create({
  backgroundVideo: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});