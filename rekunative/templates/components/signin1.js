import React, { Component } from 'react'
import { StyleSheet, Image, Dimensions, StatusBar,View,TouchableOpacity } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Formik } from 'formik'
import * as Yup from 'yup'

let screenHeight = Dimensions.get('window') -  StatusBar.currentHeight

import {
    Container,
    Input,
    Form,
    Content,
    Item,
    Button,
    Text,
    Icon,
    Grid,
    Row,
    Col,
} from 'native-base'

export default class Signin1 extends Component {
    state = {
        scrollEnabled: true
    }

    handleChangeEmail = (useHandle,validation) => (value) =>{
        validation('email')(value)
    }
    handleChangePassword = (useHandle,validation) => (value) =>{
        validation('password')(value)
    }

    handleSubmit = (useHandle,validation) => (value) =>{
        validation('email',true,true)
        return useHandle(value) 
    }
    render() {
        const {
            image,
            socmed1,
            socmed2,
            socmed3,
            field1,
            field2,
            btnSubmit,
            bottom,
        } = this.props

        const validationSchema = Yup.object().shape({
            email:Yup.string().required(field1.validation.msgRequired).email(field1.validation.message),
            password:Yup.string().required(field2.validation.msgRequired).min(field2.validation.minChr,field2.validation.message),
        })
        return (
            <Container>
                <Content 
                    style={styles.content} 
                    scrollEnabled={this.state.scrollEnabled}
                    showsVerticalScrollIndicator={false}>
                    <Grid>
                        <Row style={{flex:1}}>
                            <Col
                                style={{
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}>
                                <Image
                                    style={[
                                    styles.image, {
                                        height: image.height || 190,
                                        width: image.width || 190,
                                        marginTop: '15%'
                                    }
                                ]}
                                    source={{
                                    uri: image.url || 'https://i.ibb.co/TBzjW9h/circle-2x.png'
                                }}/>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styles.viewBtnSosmed}>
                                <Button rounded bordered style={[styles.btnSosmed,{borderColor:socmed1.borderColor || '#3741A8'}]} onPress={socmed1.onPress}>
                                    <Icon  style={{color:socmed1.color || '#3741A8'}} name={socmed1.name || 'twitter'} type='FontAwesome'/>
                                </Button>
                                <Button rounded bordered style={[styles.btnSosmed,{borderColor:socmed2.borderColor || '#3741A8'}]} onPress={socmed2.onPress}>
                                    <Icon style={{color:socmed2.color || '#3741A8'}} name={socmed2.name || 'google'} type='FontAwesome'/>
                                </Button>
                                <Button
                                    rounded
                                    bordered
                                    style={[
                                    styles.btnSosmed, {
                                        marginRight: 0,
                                        borderColor:socmed3.borderColor || '#3741A8'
                                    }
                                ]}>
                                    <Icon style={{color:socmed3.color || '#3741A8'}} name={socmed3.name || 'facebook'} type='FontAwesome' onPress={socmed3.onPress}/>
                                </Button>
                            </Col>
                        </Row>
                        <Row style={{flex:1}}>
                            <Col>
                                <Formik
                                    initialValues={{
                                        email:'',
                                        password:''
                                    }}
                                    validationSchema={validationSchema}
                                    onSubmit={(value) => btnSubmit.onPress(value)}
                                    render={(props) =>{
                                        return(
                                            <Form>
                                                <Item rounded style={styles.itemForm}>
                                                    <Input
                                                        placeholder={field1.label || 'Email'}
                                                        style={styles.input}
                                                        onChangeText={this.handleChangeEmail(field1.onChangeText,props.handleChange)}
                                                        onFocus={()=>this.setState({scrollEnabled:!this.state.scrollEnabled})}
                                                        onBlur={()=>this.setState({scrollEnabled:!this.state.scrollEnabled})}

                                                    />                                                    
                                                </Item>
                                                <Text style={styles.txtValidation}>{props.errors.email || ''}</Text>
                                                <Item rounded style={styles.itemForm}>
                                                    <Input
                                                        placeholder={field2.label || 'Password'}
                                                        style={styles.input}
                                                        secureTextEntry={true}
                                                        onChangeText={this.handleChangePassword(field2.onChangeText,props.handleChange)}
                                                        onFocus={()=>this.setState({scrollEnabled:!this.state.scrollEnabled})}
                                                        onBlur={()=>this.setState({scrollEnabled:!this.state.scrollEnabled})}
                                                    />
                                                </Item>
                                                <Text style={styles.txtValidation}>{props.errors.password || ''}</Text>
                                                <LinearGradient
                                                    start={{
                                                    x: 0,
                                                    y: 0
                                                }}
                                                    end={{
                                                    x: 1,
                                                    y: 0
                                                }}
                                                    colors={['#46b6fb', '#2B79C9']}
                                                    style={styles.linearGradient}>
                                                    <Button block style={styles.btnSubmit} onPress={props.handleSubmit}>
                                                        <Text style={styles.txtBtnSubmit}>{btnSubmit.label || 'LOGIN'}</Text>
                                                    </Button>
                                                </LinearGradient>
                                            </Form>
                                        )
                                    }}
                                     
                                />
                            </Col>
                        </Row>
                        <Row>
                            <Col style={{flexDirection:'column',justifyContent: 'space-between'}}>
                                {typeof bottom.text == 'string' ? (
                                    <View
                                        style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}>
                                        <Text>{bottom.text}</Text>
                                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Signup2')}>
                                            <Text
                                                style={{
                                                fontWeight: 'bold',
                                                color: 'black',
                                                fontWeight: 'bold'
                                            }}>
                                                {bottom.textBold}</Text>
                                        </TouchableOpacity>
                                    </View>  
                                ):bottom}
                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        height:screenHeight,
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        marginBottom: 20
    },
    content: {
        flex: 1,
        paddingRight: 30,
        paddingLeft: 30
    },
    itemForm: {
        fontSize: 14,
        marginBottom: 15,
        marginTop: 15,
        paddingLeft:15,
		paddingRight:15,
    },
    btnSubmit: {
        borderRadius: 30,
        elevation: 0,
        height: 55,
        backgroundColor: 'transparent'
    },
    linearGradient: {
        marginTop:12,
        marginBottom: 30,
        width: '100%',
        borderRadius: 50,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center'
    },
    txtBtnSubmit: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18
    },
    input: {
        padding: 9,
        fontSize: 18,
        fontWeight: 'bold',
        marginHorizontal: 10,
        marginVertical: 5,
        height: 50
    },
    itemCenter: {
        alignItems: 'center',
        backgroundColor: 'transparent',
        elevation: 0,
        overflow: 'hidden'
    },
    txtSignup: {
        fontWeight: 'bold'
    },
    txtValidation:{
        marginLeft:20,
        color:'red',
        fontSize:13
    },
    btnSosmed: {
        borderRadius: 50,
        marginBottom: 20,
        marginRight: 20,
        height: 60,
        width: 60,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: -50
    },
    viewBtnSosmed: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 50
    }
});