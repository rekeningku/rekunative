import React, { Component } from 'react'
import { List, ListItem, Text, Icon } from 'native-base'
import { FlatList, StyleSheet } from 'react-native'

export default class ListMenu extends Component {
	render() {
		const { menus } = this.props

		return (
			<List>
				<FlatList
					data={menus}
					renderItem={({ item }) => (
						<ListItem onPress={item.onPress}>
							<Icon name={item.iconName} type={item.iconType} style={styles.icon} />
							<Text>{item.label}</Text>
						</ListItem>
					)}
					keyExtractor={(index) => index.toString()}
				/>
			</List>
		)
	}
}

const styles = StyleSheet.create({
	icon: {
		fontSize: 40,
		color: '#4d94ff',
		marginRight: 10
	}
})
