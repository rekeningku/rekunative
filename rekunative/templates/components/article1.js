import React, {Component} from 'react'
import {FlatList,ImageBackground,View,StyleSheet} from 'react-native'
import {
    Container,
    Text,
    Icon,
    Content,
    Grid,
    Row,
    Col,
    H2,
} from 'native-base'

export default class Article1 extends Component {
    render() {
        const {data} = this.props
        const setting = this.props.setting || undefined
        return (
            <Container>
                <Content>
                    <Grid>
                        <Row>
                            <FlatList
                                data={data}
                                renderItem={({item})=>(
                                    <Col onPress={item.onPress}>
                                        <ImageBackground source={{uri:item.imageUrl}} style={styles.imageBackground}>
                                            <View style={{paddingHorizontal:20}}>
                                                <H2 style={[styles.header,{color:setting ?  setting.title.color : 'white'}]}>{item.title}</H2>
                                                <Text note style={{color:setting ? setting.title.color : 'white'}}>{item.time}</Text>
                                                <View style={styles.viewIcon}>
                                                    <Icon name={setting ? setting.btnIconLeft.icon : 'heart'} style={[styles.icon,{marginLeft:0,color:setting ? setting.btnIconLeft.color : 'white'}]} type={setting ? setting.btnIconLeft.type : 'FontAwesome'}/>
                                                    <Text style={{color:setting ? setting.btnIconCenter.textColor : 'white'}}>{item.like}</Text>
                                                    <Icon name={setting ? setting.btnIconCenter.icon : "comment-o"} style={[styles.icon,{color:setting ? setting.btnIconCenter.color : 'white'}]} type={setting ? setting.btnIconCenter.type : 'FontAwesome'}/>
                                                    <Text style={{color:'white'}}>{item.comment}</Text>
                                                    <Icon name={setting ? setting.btnIconRight.icon : "user-o"} style={[styles.icon,{color:setting ? setting.btnIconRight.color : 'white'}]} type={setting ? setting.btnIconRight.type : 'FontAwesome'}/>
                                                    <Text style={{color:setting ? setting.btnIconRight.textColor : 'white'}}>{item.user}</Text>
                                                </View>
                                            </View>
                                        </ImageBackground>
                                     </Col>
                                )}
                                keyExtractor={(item,index) => index.toString()}/>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    imageBackground:{
        height:'100%',
        width:'100%',
    },
    header:{
        marginTop:100,
        fontWeight:'bold'
    },
    viewIcon:{
        marginTop:20,
        marginBottom:13,
        flexDirection:'row'
    },
    icon:{
        fontSize:20,
        marginRight:12,
        marginLeft:12
    }
})