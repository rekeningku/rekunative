import React, {Component} from 'react'
import {View, StyleSheet, Dimensions} from 'react-native'
import MapView,{PROVIDER_GOOGLE, Marker} from 'react-native-maps'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import {Text, Container, Grid, Col, Row, Input, Icon, Button} from 'native-base'

let { width, height } = Dimensions.get('window')

const ASPECT_RATIO = width / height
const LATITUDE_DELTA = 0.0922
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO

export default class Mapspicker extends Component {
    state = {
        setDestination:false,
        error:null,
        region:{
            latitude:0,
            longitude:0,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
        },
        pickRegion:{
            latitude:0,
            longitude:0,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
        },
        mapsRegion:null
    }
    
    componentDidMount = () => {
        navigator.geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    region:{
                        latitude:position.coords.latitude,
                        longitude:position.coords.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    }
                })
            },
            (error) => this.setState({ error: error.message}),
            {enableHighAccuracy:false,timeout:200000,maximumAge:1000}
        ),
        this.watchID = navigator.geolocation.watchPosition(
            position => {
                this.setState({
                    region:{
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    }
                })  
            }
        )   
    }

    componentWillMount = () => {
        navigator.geolocation.clearWatch(this.watchID)
    }

    onRegionChange = (region,latitude,longitude) => {
        this.setState({
            pickRegion:{
                latitude:latitude,
                longitude:longitude,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
            },
            mapsRegion:region
        })
    }

    handleSetMaps = (lat,lon,latDelta,lonDelta) => {
        this.props.setMaps(lat,lon,latDelta,lonDelta)
    }

    render() {
        const {data} = this.props
        return (
            <Container>
                <Grid>
                    <Row style={{flex:5}}>
                        <Col>
                            <Button rounded lin style={styles.btnTopLeft} onPress={data.iconLeft.onPress}>
                                <Icon name='arrowleft' type='AntDesign' style={{color:'#000',fontSize:19}} />
                            </Button>
                            <MapView    
                                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                                style={styles.maps}
                                showsUserLocation
                                region={this.state.region}
                                initialRegion={this.state.mapsRegion}
                                onPress={(e) => {
                                    const region = {
                                        latitude:parseFloat(e.nativeEvent.coordinate.latitude),
                                        longitude:parseFloat(e.nativeEvent.coordinate.longitude),
                                        latitudeDelta:0.00922*1.5,
                                        longitudeDelta:0.00421*1.5
                                    }
                                    this.onRegionChange(region,region.latitude,region.longitude)
                                }}
                                >
                                {this.state.setDestination && ( 
                                    <MapView.Marker
                                        coordinate={{
                                            latitude: this.state.pickRegion.latitude,
                                            longitude: this.state.pickRegion.longitude,
                                        }}
                                        title="Lokasi"
                                        description="Hello"
                                    />
                                )}
                            </MapView>
                        </Col>
                    </Row>
                    <Row style={{flex:2}}>
                        <Col style={styles.container}>
                            <Text style={{marginBottom:10,fontSize:20,fontWeight:'bold'}}>{data.title}</Text>
                            <GooglePlacesAutocomplete
                                placeholder={data.placeholder}
                                minLength={2}
                                autoFocus={false}
                                fetchDetails
                                listViewDisplayed='auto'
                                query={{
                                    key:data.apiKey,
                                    language: 'en',
                                    types: 'geocode',
                                }}
                                currentLocation={false}
                                onPress={(data,details = parseFloat(0)) => {
                                    this.setState({
                                        setDestination:true
                                    })
                                    const regions = {
                                        latitude: parseFloat(details.geometry.location.lat),
                                        longitude: parseFloat(details.geometry.location.lng),
                                        latitudeDelta: 0.00922 * 1.5,
                                        longitudeDelta: 0.00421 * 1.5
                                    }
                                    this.handleSetMaps(regions.latitude,regions.longitude,regions.latitudeDelta,regions.longitudeDelta)
                                    this.onRegionChange(regions, regions.latitude, regions.longitude)
                                }}
                                debounce={200}
                                renderLeftButton={()=> <Icon name='location-pin' type='Entypo' style={{marginHorizontal:5,marginVertical:5}}/>}
                                renderRightButton={()=> <Icon name='md-search' type='Ionicons' style={{marginHorizontal:15,marginVertical:5}}/>}
                                styles={{
                                    textInputContainer: {
                                        backgroundColor: 'white',
                                        borderColor:'gray',
                                        fontSize:15,
                                        borderTopWidth: 0.95,
                                        borderBottomWidth:0.95,
                                        borderLeftWidth:0.95,
                                        borderRightWidth:0.95,
                                        borderRadius:15,

                                    },
                                   
                                }}
                            />
                        </Col>
                    </Row>
                </Grid>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        paddingHorizontal:20,
        paddingVertical:15,
    },
    maps:{
        width:'100%',
        height:'100%',
    },
    btnTopLeft:{
        position:'absolute',
        top:10,
        left:10,
        backgroundColor:'#fff'
    }
})