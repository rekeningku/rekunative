import React, { Component } from "react"
import { Container, Content, Grid, Row, Col, View, Text, Thumbnail, Item, Input } from "native-base"
import {StyleSheet, TouchableOpacity, FlatList} from 'react-native'
import LinearGradient from "react-native-linear-gradient"
import Modal from "react-native-modal"

export default class CreditCard extends Component {
    state = {
        visibleModal: null
    }

    setModalVisible(visible) {
        this.setState({visibleModal: visible})
      }

    renderButton = (text, onPress) => (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.button}>
                <Text>{text}</Text>
            </View>
        </TouchableOpacity>
    )

    renderModalContent = () => (
        <View style={styles.viewModal}>
            <Grid>
                <Row style={styles.rowModalHeader}>
                    <Col>
                        <Text style={styles.textModalHeader}>Enter security code</Text>
                    </Col>
                </Row>
                <Row style={styles.rowModalInput}>
                    <Col>
                        <Item rounded style={styles.itemModalInput}>
                            <Input/>
                        </Item>
                    </Col>
                </Row>
                <Row style={styles.rowModalAction}>
                    <Col style={styles.colModalAction}>
                    <View style={styles.viewModalAction}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible)

                                }}>
                                <View style={styles.viewModalAction}>
                                    <Text style={styles.textModalActionNo}>Cancel</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </Col>

                    <Col>
                        <View style={styles.viewModalAction}>
                            <TouchableOpacity
                                onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible)
                                }}>
                                <View style={styles.viewModalAction}>
                                    <Text style={styles.textModalActionYes}>Ok</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </Col>
                </Row>
            </Grid>
        </View>
    )

    handleOnScroll = event => {
        this.setState({
            scrollOffset: event.nativeEvent.contentOffset.y
        })
    }

    handleScrollTo = p => {
        if (this.scrollViewRef) {
            this.scrollViewRef.scrollTo(p)
        }
    }

    render() {
        const { data } = this.props
        return (
            <Container>
                <Content padder>
                    <Modal
                        isVisible={this.state.visibleModal === 2}
                        animationIn="slideInLeft"
                        animationOut="slideOutRight"
                    >
                        {this.renderModalContent()}
                    </Modal>
                    <FlatList
                        data={data}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => this.setState({ visibleModal: 2 })}>
                        <View style={styles.viewCard} >
                            <LinearGradient
                                colors={[item.backgroundColor ? item.backgroundColor[0] : '#3640A7' , item.backgroundColor ? item.backgroundColor[1] : '#3F5DCB' ]}
                                style={styles.linearGradient}
                                start={{
                                    x: 1,
                                    y: 0
                                }}
                                end={{
                                    x: 0,
                                    y: 0
                                }}
                            >
                                <Grid>
                                    <Row style={styles.rowHeader}>
                                        <Col>
                                            <Text style={styles.textName}>{item.name}</Text>
                                        </Col>
                                        <Col>
                                            <View style={styles.viewLogo}>
                                                <Thumbnail
                                                    style={styles.logo}
                                                    source={{
                                                        uri: item.logoUrl
                                                    }} />
                                            </View>
                                        </Col>
                                    </Row>
                                    <Row style={styles.rowAccountNumber}>
                                        <Col>
                                            <View style={styles.viewAccountNumber}>

                                                <Text style={styles.textAccountNumber}>{item.accountNumber}</Text>
                                            </View>
                                        </Col>
                                    </Row>
                                    <Row style={styles.rowDate}>
                                        <Col>
                                            <View style={styles.viewDate}>
                                                <Text style={styles.textDate}>{item.date}</Text>
                                            </View>
                                        </Col>
                                    </Row>
                                    <Row style={styles.rowFooter}>
                                        <Col>
                                            <Text style={styles.textCurrency}>{item.currency}</Text>
                                            <Text style={styles.textBranch}>{item.branch}</Text>
                                        </Col>
                                        <Col>
                                            <View style={styles.viewBalance}>
                                                <Text style={styles.textBalance}>{item.balance}</Text>
                                            </View>
                                        </Col>
                                    </Row>
                                </Grid>
                            </LinearGradient>
                        </View>
                    </TouchableOpacity>
                        )}
                        keyExtractor={index => index.toString()}
                    />
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    viewCard: {
        flex: 1, height: 250, width: 370, marginBottom: 20, marginLeft: 10
    },
    linearGradient: {
        flex: 1,
        borderRadius: 10
    },
    rowHeader: {
        height: '18%'
    },
    rowAccountNumber: {
        height: '27%'
    },
    rowFooter: {
        height: '30%'
    },
    rowDate: {
        height: '25%'
    },
    textName: {
        marginTop: 7, marginLeft: 10, color: '#fff', fontSize: 20, fontWeight: 'bold'
    },
    viewLogo: {
        flex: 1, flexDirection: 'row', justifyContent: 'flex-end', paddingRight: 10
    },
    logo: {
        width: 70
    },
    textAccountNumber: {
        color: '#fff', alignSelf: 'center', fontSize: 30
    },
    textCurrency: {
        marginTop: 7, marginLeft: 10, color: '#fff', fontSize: 20, fontWeight: 'bold'
    },
    textBranch: {
        marginLeft: 10, color: '#fff', fontSize: 20, fontWeight: 'bold'
    },
    textBalance: {
        color: '#fff', fontSize: 30, fontWeight: 'bold', alignSelf: 'center'
    },
    textDate: {
        color: '#fff', alignSelf: 'center'
    },
    viewBalance: {
        flex: 1, flexDirection: 'row', justifyContent: 'flex-end', paddingRight: 10
    },
    viewDate: {
        justifyContent: 'center', flex: 1, flexDirection: 'row'
    },
    viewAccountNumber: {
        justifyContent: 'center', flex: 1, flexDirection: 'row'
    },

    viewModal: {
        height: 260,
        width: 340,
        borderRadius: 10,
        backgroundColor: "white",
        alignSelf: 'center',
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    rowModalHeader: {
        height: '30%'
    },
    textModalHeader: {
        alignSelf: 'center', marginTop: 10, fontSize: 20, fontWeight: 'bold'
    },
    rowModalInput: {
        height: '50%', padding: 17
    },
    itemModalInput: {
        height: 60
    },
    rowModalAction: {
        borderTopWidth: 1, borderColor: '#d1d1e0', height: '20%'
    },
    colModalAction: {
        borderRightWidth: 1, borderColor: '#d1d1e0'
    },
    viewModalAction: {
        justifyContent: 'center', flex: 1, flexDirection: 'row'
    },
    textModalActionYes: {
        alignSelf: 'center', fontSize: 20, fontWeight: 'bold'
    },
    textModalActionNo: {
        alignSelf: 'center', fontSize: 20,fontWeight: 'bold'
    },
}) 