import React, { Component } from 'react'
import { Image, StyleSheet, Dimensions } from 'react-native'
import { Text, Grid, Row, Col, H1, Item, Input, Form, Button } from 'native-base'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import LinearGradient from 'react-native-linear-gradient'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { buildYup } from 'json-schema-to-yup'

const screenHeight = Dimensions.get('window').height - 30

const forgotPasswordSchema = Yup.object().shape({
	email: Yup.string().required('Required!').email('Email not valid!')
})

class ForgotPassword extends Component {
	// instruction label
	renderInstructionLabel = (instructionLabel = 'Enter your email below to receive your password reset and instructions') => {
		if (typeof instructionLabel === 'string') {
			return (
				<Text
					style={{
						textAlign: 'center',
						color: '#C6C6C6'
					}}
				>
					{instructionLabel}
				</Text>
			)
		}
		return instructionLabel
	}

	schema = () => {
		return (
			this.props.field1.validationSchema &&
			this.props.field1.validationConfig &&
			buildYup(
				{
					...this.props.field1.validationSchema,
					properties: {
						...this.props.field1.validationSchema.properties,
						email: {
							...this.props.field1.validationSchema.properties.email,
							pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
						}
					}
				},
				{
					...this.props.field1.validationConfig,
					properties: {
						...this.props.field1.validationConfig.errMessages,
						email: {
							...this.props.field1.validationConfig.errMessages.email,
							pattern: 'Not a valid email address'
						}
					}
				}
			)
		)
	}
	// handleChange
	handleChange = (userHandle, validation) => (value) => {
		validation('email')(value)
		return userHandle(value)
	}

	//handleSubmit
	handleSubmit = (setFieldTouched, handleSubmit) => (value) => {
		setFieldTouched('email', true, true)
		return handleSubmit(value)
	}

	render() {
		return (
			<KeyboardAwareScrollView>
				<Formik
					initialValues={{
						email: ''
					}}
					validationSchema={this.schema() || forgotPasswordSchema}
					onSubmit={(value) => this.props.btnSubmit.onPress(value)}
					validateOnBlur={true}
					render={(props) => {
						return (
							<Grid style={styles.root}>
								<Row>
									<Col style={styles.logoContainer}>
										<Image
											source={this.props.image.url}
											style={
												([ styles.logo, styles.gutterBottom ],
												{
													width: this.props.image.width,
													height: this.props.image.height
												})
											}
										/>
										<H1 style={{ marginTop: 10 }}>{this.props.title}</H1>
									</Col>
								</Row>
								<Row style={styles.formContainer}>
									<Col>
										<Form style={styles.form}>
											<Item regular last style={styles.input} bordered>
												<Input
													placeholder={this.props.field1.label || 'Email'}
													onChangeText={this.handleChange(this.props.field1.onChangeText, props.handleChange)}
													onBlur={props.handleBlur('email')}
													keyboardType={this.props.field1.keyboardType || 'email-address'}
												/>
											</Item>
											<Text style={styles.helperText}>{props.errors.email || ''}</Text>
										</Form>
										{this.renderInstructionLabel(this.props.instructionLabel)}
									</Col>
								</Row>
								<Row style={styles.buttonContainer}>
									<Col>
										<LinearGradient
											colors={[ '#2B93E8', '#1959A1' ]}
											start={{ x: 0, y: 0 }}
											end={{ x: 1, y: 0 }}
											style={styles.gradient}
										>
											<Button
												block
												full
												style={styles.button}
												onPress={this.handleSubmit(props.setFieldTouched, props.handleSubmit)}
											>
												<Text style={{ color: 'white' }}>{this.props.btnSubmit.label}</Text>
											</Button>
										</LinearGradient>
									</Col>
								</Row>
							</Grid>
						)
					}}
				/>
			</KeyboardAwareScrollView>
		)
	}
}

const styles = StyleSheet.create({
	root: {
		height: screenHeight
	},
	logoContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	logo: {},
	formContainer: {
		flex: 1,
		paddingHorizontal: 20,
		alignItems: 'center'
	},
	// form
	form: {
		marginBottom: 20
	},
	input: {
		borderRadius: 40,
		height: 60
	},
	helperText: {
		color: '#B8685F',
		fontSize: 10,
		paddingVertical: 5,
		paddingHorizontal: 25
	},
	textDescription: {
		textAlign: 'center',
		color: '#C6C6C6'
	},
	buttonContainer: {
		paddingHorizontal: 20,
		alignItems: 'center'
	},
	button: {
		elevation: 0,
		borderRadius: 40,
		backgroundColor: 'transparent',
		height: 60
	},
	gradient: {
		borderRadius: 40,
		justifyContent: 'center',
		alignItems: 'center'
	},
	text: {
		fontSize: 20,
		color: '#fff'
	},

	// util
	gutterBottom: {
		marginBottom: 20
	}
})

export default ForgotPassword
