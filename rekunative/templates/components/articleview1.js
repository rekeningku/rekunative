import React, { Component } from 'react'
import { Container, Content, Thumbnail, Text, View, Icon } from 'native-base'
import { Col, Row, Grid } from 'react-native-easy-grid'
import {StyleSheet} from 'react-native'

export default class ArticleView extends Component {
    render() {
        const {data, like, comment, person,setting} = this.props
        return (
            <Container>
                <Content>
                    <Grid>
                        <Row>
                            <Col style={{height: setting ? setting.imageArticle.height : 180}}>
                                <Thumbnail square style={styles.image} source={{ uri: data.imageUrl || 'http://fertilitynetworkuk.org/wp-content/uploads/2017/01/Facebook-no-profile-picture-icon-620x389.jpg'}} />
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styles.colTitle}>
                                <Text style={[styles.textTitle,{color: setting ? setting.title.color : '#000'}]}>{data.title || ''}</Text>
                                <Text style={[styles.textTime,{color:setting ? setting.time.color : '#000'}]}>{data.time || ''}</Text>
                            </Col>
                            <Col style={styles.colProfilePicture}>
                                <View style={styles.viewProfilePicture}>
                                    <Thumbnail circle style={[styles.profilePicture,{height: setting ? setting.imageProfile.height : 40 , width: setting ? setting.imageProfile.width : 40}]} source={{ uri: data.profilePictureUrl || 'http://fertilitynetworkuk.org/wp-content/uploads/2017/01/Facebook-no-profile-picture-icon-620x389.jpg'}} />
                                </View>
                            </Col>
                        </Row>
                        <Row>
                            <Col style={styles.colContent}>
                                <Text style={[styles.textContent,{color:setting ? setting.content.color : '#000'}]}>
                                    {data.content || ''}
                                </Text>
                            </Col>
                        </Row>
                        <Row style={styles.rowDatas}>
                            <Col onPress={like.onPress}>
                                <View style={styles.viewDatas}>
                                    <Icon name={like ? like.icon : "heart"} type={like.type || 'FontAwesome'} style={styles.iconLove} />
                                    <Text style={styles.textIcon}>{data.like || 0}</Text>
                                </View>
                            </Col>
                            <Col onPress={comment.onPress}>
                                <View style={styles.viewDatas}>
                                    <Icon name={comment ? comment.icon : "comment-o"} type={comment.type || 'FontAwesome'} style={styles.iconComment} />
                                    <Text style={styles.textIcon}>{data.comment || 0}</Text>
                                </View>
                            </Col>
                            <Col onPress={person.onPress}>
                                <View style={styles.viewDatas}>
                                    <Icon name={person ? person.icon : "user-o"} type={person.type || 'FontAwesome'} style={styles.iconPerson} />
                                    <Text style={styles.textIcon}>{data.person || 0}</Text>
                                </View>
                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    colTitle: {
        height: 80,
        padding: 10,
        paddingLeft: 15
    },
    colProfilePicture: {
        height: 70,
        width: "20%"
    },
    colContent: {
        height: "auto",
        padding: 10,
        paddingLeft: 15,
        paddingRight: 20,
        borderBottomWidth: 1,
        borderBottomColor: "#f0f0f5"
    },
    rowDatas: {
        height: 40
    },
    image: {
        width: '100%', height: '100%'
    },
    textTitle: {
        fontSize: 19, fontWeight: 'bold', marginBottom: 5
    },
    textTime: {
        fontSize: 15
    },
    viewProfilePicture: {
        justifyContent: 'center', flex: 1
    },
    profilePicture: {
        alignSelf: 'center'
    },
    textContent: {
        fontSize: 16,
    },
    viewDatas: {
        justifyContent: 'center', flex: 1, flexDirection: 'row'
    },
    iconLove: {
        color: 'red', alignSelf: 'center', marginRight: 10, fontSize: 20
    },
    iconComment: {
        alignSelf: 'center', marginRight: 10, fontSize: 20
    },
    iconPerson: {
        alignSelf: 'center', marginRight: 10, fontSize: 20
    },
    textIcon: {
        alignSelf: 'center'
    }
})