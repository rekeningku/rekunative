import React, { Component } from 'react'
import { StyleSheet, FlatList } from 'react-native'
import { Container, Content, View, Text, Thumbnail, Form, Item, Input, Row, Grid, Label, Right, Left, Icon, Button, Toast, Root } from 'native-base'
import { Switch } from 'react-native-switch'
import LinearGradient from 'react-native-linear-gradient'
import { Formik } from 'formik'
import _ from 'lodash'
import { buildYup } from 'json-schema-to-yup'

const ignoreWarning = () => (console.disableYellowBox = [ 'Warning: Cannot update during an existing state transition' ])

const Title = ({ children,color }) => {
	return (
		<View style={styles.titleContainer}>
			<Text style={[styles.title,{color: color}]}>{children}</Text>
		</View>
	)
}

class ProfileSetting extends Component {
	
	state = {
		socmed1: false,
		socmed2: false,
		socmed3: false,
		showToast: false
	}

	profileSettingSchema = buildYup(
		{
			...this.props.validationSchema,
			properties: {
				...this.props.validationSchema.properties,
				email: {
					...this.props.validationSchema.properties.email,
					pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
				}
			}
		},
		{
			...this.props.validationConfig,
			errMessages: {
				...this.props.validationConfig.errMessages,
				email: {
					...this.props.validationConfig.errMessages.email,
					pattern: 'Not a valid email address'
				}
			}
		}
	)

	handleToggleSwitch = (socmed, setFieldValue) => () => {
		setFieldValue(socmed, !this.state[socmed], true)
		this.setState({
			[socmed]: !this.state[socmed]
		})
	}

	handleSubmit = (userSubmit) => (value) => {
		this.props.socmed1.onConnect()
		this.props.socmed2.onConnect()
		this.props.socmed3.onConnect()
		return userSubmit(value)
	}

	handleChange = (input, formik) => (value) => {
		let error = input.onChangeText(value, formik)
		if (error) {
			formik.setStatus(error)
		}
		return formik.handleChange(input.name)(value)
	}

	renderFormGroup = (form, formik) => {
		return (
			<View style={styles.container}>
				<Title color={this.props.setting ? this.props.setting.subHeader.color : '#22619F'}>{form.group.toUpperCase()}</Title>
				<Form>
					<FlatList data={form.fields} renderItem={this.renderField(formik)} keyExtractor={(item, key) => item.label + key} />
				</Form>
			</View>
		)
	}

	renderField = (formik) => ({ item }) => {
		return (
			<Item style={styles.formItem}>
				<Label style={{fontSize:this.props.setting ? this.props.setting.fieldLabel.fontSize : 14,color:this.props.setting ? this.props.setting.fieldLabel.color : '#D4D4D4'}}>{item.label}</Label>
				<Input
					style={this.props.setting ? this.props.setting.inputValue : styles.formInput}
					onChangeText={this.handleChange(item, formik)}
					onBlur={formik.handleBlur(item.name)}
					secureTextEntry={item.type === 'password' || false}
					defaultValue={this.props.initialValues[item.name]}
				/>
			</Item>
		)
	}

	showToast = (props) => {
		if (props.isSubmitting) {
			ignoreWarning()
			if (!_.isEmpty(props.errors)) {
				console.warn(props.errors)
				Toast.show({
					text: props.errors[Object.keys(props.errors)[0]],
					position: 'top',
					buttonText: 'Ok',
					type: 'danger'
				})
			} else if (!_.isEmpty(props.status)) {
				console.warn(props.status)
				Toast.show({
					text: props.status[Object.keys(props.status)[0]],
					position: 'top',
					buttonText: 'Ok',
					type: 'danger'
				})
			}
		}
	}

	render() {
		const {setting} = this.props
		return (
			<Container>
				<Root>
					<Formik
						validationSchema={this.profileSettingSchema}
						initialValues={this.props.initialValues}
						validateOnChange={false}
						isInitialValid={true}
						onSubmit={this.handleSubmit(this.props.btnSubmit.onPress)}
						render={(props) => {
							this.showToast(props)
							return (
								<Content>
									<View style={styles.profileContainer}>
										<Thumbnail source={{uri:this.props.image.uri}} style={styles.profileImage} />
									</View>
									<FlatList
										data={this.props.form}
										renderItem={(data) => this.renderFormGroup(data.item, props)}
										keyExtractor={(item, key) => item.group + key}
									/>
									<View style={styles.container}>
										<Title color={setting ? setting.subHeader.color : '#22619F'}>CONNECT YOUR ACCOUNT</Title>
										<Grid>
											<Row style={styles.switchItem}>
												<Left style={styles.left}>
													<Icon style={styles.leftIcon} name={this.props.socmed1.icon || 'twitter'} type='FontAwesome' />
													<Text style={styles.switchLabel}>{this.props.socmed1.label || 'Twitter'}</Text>
												</Left>
												<Right>
													<Switch
														value={this.state.socmed1}
														onValueChange={this.handleToggleSwitch('socmed1', props.setFieldValue)}
														backgroundActive={setting ? setting.switch.socmed1.backgroundActive : '#22619F' }
														backgroundInactive={setting ? setting.switch.socmed1.backgroundInactive : '#CACACA'}
														circleActiveColor={setting ?  setting.switch.socmed1.circleActiveColor : '#FFFFFF'}
														circleInActiveColor={setting ? setting.switch.socmed1.circleInActiveColor :'#FFFFFF'}
														innerCircleStyle={setting ? setting.switch.socmed1.innerCircleStyle : { borderColor: '#CACACA' }}
													/>
												</Right>
											</Row>
											<Row style={styles.switchItem}>
												<Left style={styles.left}>
													<Icon style={styles.leftIcon} name={this.props.socmed2.icon || 'google'} type='FontAwesome' />
													<Text style={styles.switchLabel}>{this.props.socmed2.label || 'Google'}</Text>
												</Left>
												<Right>
													<Switch
														value={this.state.socmed2}
														onValueChange={this.handleToggleSwitch('socmed2', props.setFieldValue)}
														backgroundActive={setting ? setting.switch.socmed2.backgroundActive : '#22619F'}
														backgroundInactive={setting ? setting.switch.socmed2.backgroundInactive : '#CACACA'}
														circleActiveColor={setting ? setting.switch.socmed2.circleActiveColor : '#FFFFFF'}
														circleInActiveColor={setting ? setting.switch.socmed2.circleInActiveColor : '#FFFFFF'}
														innerCircleStyle={setting ? setting.switch.socmed2.innerCircleStyle : { borderColor: '#CACACA' }}
													/>
												</Right>
											</Row>
											<Row style={styles.switchItem}>
												<Left style={styles.left}>
													<Icon style={styles.leftIcon} name={this.props.socmed3.icon || 'facebook'} type='FontAwesome' />
													<Text style={styles.switchLabel}>{this.props.socmed3.icon || 'Facebook'}</Text>
												</Left>
												<Right>
													<Switch
														value={this.state.socmed3}
														onValueChange={this.handleToggleSwitch('socmed3', props.setFieldValue)}
														backgroundActive={setting ? setting.switch.socmed3.backgroundActive : '#22619F'}
														backgroundInactive={setting ? setting.switch.socmed3.backgroundInactive : '#CACACA'}
														circleActiveColor={setting ? setting.switch.socmed3.circleActiveColor : '#FFFFFF'}
														circleInActiveColor={setting ? setting.switch.socmed3.circleInActiveColor : '#FFFFFF'}
														innerCircleStyle={setting ? setting.switch.socmed3.innerCircleStyle : { borderColor: '#CACACA' }}
													/>
												</Right>
											</Row>
										</Grid>
									</View>
									<View style={styles.container}>
										<LinearGradient
											colors={setting ? setting.btnSubmit.colors : [ '#46b6fb', '#2B79C9' ]}
											start={{ x: 0, y: 0 }}
											end={{ x: 1, y: 0 }}
											style={styles.gradient}
										>
											<Button block full style={styles.button} onPress={props.handleSubmit}>
												<Text style={{ color: 'white' }}>{this.props.btnSubmit.label || 'SAVE'}</Text>
											</Button>
										</LinearGradient>
									</View>
								</Content>
							)
						}}
					/>
				</Root>
			</Container>
		)
	}
}

const styles = StyleSheet.create({
	//title
	titleContainer: {
		paddingHorizontal: 10,
		borderBottomWidth: 0.5,
		borderBottomColor: '#D9D5DC',
		paddingBottom: 10,
		paddingTop: 50
	},
	title: {
		fontWeight: '500',
		fontSize: 15
	},

	// .profile
	profileContainer: {
		width: '100%',
		height: 140,
		justifyContent: 'center',
		alignItems: 'center'
	},
	profileImage: {
		width: 80,
		height: 80,
		borderRadius: 80
	},

	// info
	container: {
		width: '100%'
	},

	//form
	formItem: {
		marginLeft: 0,
		paddingHorizontal: 10,
		borderBottomWidth: 1
	},
	formInput: {
		fontSize: 14,
		textAlign: 'right',
		fontWeight: '500',
		color:'#000'
	},

	// switch
	switchItem: {
		height: 50,
		alignItems: 'center',
		paddingHorizontal: 10,
		borderBottomWidth: 0.5,
		borderBottomColor: '#D9D5DC'
	},
	left: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	leftIcon: {
		marginRight: 5,
		color: '#CACACA',
		width: 30
	},

	// button
	button: {
		backgroundColor: 'transparent',
		borderRadius: 40,
		justifyContent: 'center',
		alignItems: 'center'
	},
	gradient: {
		marginVertical: 30,
		marginHorizontal: 10,
		borderRadius: 40,
		justifyContent: 'center',
		alignItems: 'center',
		shadowOffset: {
			width: 0,
			height: 1
		},
		shadowOpacity: 0.2,
		shadowRadius: 1.41,
		elevation: 3
	}
})

export default ProfileSetting
