import React, {Component} from 'react'
import {View,FlatList,TouchableWithoutFeedback} from 'react-native'
import {Text, Container, Grid, Row, Body,Col,Content, List, ListItem,Thumbnail, Left, Right, Icon} from 'native-base'
import { Thumbnail as ThumbnailVideo } from 'react-native-thumbnail-video'
import showPopupMenu from 'react-native-popup-menu-android'

 
export default class Video1 extends Component {

	handleMoreItemSelect = this.props.onPressMenu

	refMoreButton = el => this.moreButton = el
	showMore = () => {
		showPopupMenu(
			this.props.popUpMenu,
			this.handleMoreItemSelect,
			this.moreButton
		)
	}

    render() {
		const {data} = this.props
		console.disableYellowBox = true
        return (
			<Container>
				<Content>
					<Grid>
						<Row style={{padding:20}}>
							<Col>
								<FlatList
									data={data}
									renderItem={({item}) => (
										<List style={{marginBottom:10,borderBottomColor:'gray',borderBottomWidth:0.90}}>
											<ThumbnailVideo url={item.videoUrl} imageWidth={371} imageHeight={180}  showPlayIcon={false}/>
											<ListItem avatar noBorder>
												<Left>
													<Thumbnail source={{ uri: item.imageProfile }}/>
												</Left>
												<Body>
													<Text>{item.title}</Text>
													<Text note>{item.name}・ {item.viewers} ditoton</Text>
													<Text note>{item.date}</Text>
												</Body>
												<Right>
													<TouchableWithoutFeedback onPress={this.showMore}>
														<View ref={this.refMoreButton}>
															<Icon name='options-vertical' type='SimpleLineIcons' style={{fontSize:18}} />
														</View>
													</TouchableWithoutFeedback>
												</Right>
											</ListItem>
										</List>
									)}
                                    keyExtractor={(item, index) => index.toString()}
								/>
							</Col>
						</Row>
					</Grid>
				</Content>
			</Container>
        )
    }
}
