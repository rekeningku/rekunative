import React, { Component } from 'react'
import AppIntroSlider from 'react-native-app-intro-slider'
import { View, Text, Icon } from 'native-base'
import LinearGradient from 'react-native-linear-gradient'
import { StyleSheet } from 'react-native'

class OnBoarding1 extends Component {
	renderNextButton = () => {
		return (
			<View style={styles.buttonCircle}>
				<Icon
					// Icon
					name='md-arrow-round-forward'
					color='rgba(255, 255, 255, .9)'
					size={24}
					style={{ backgroundColor: 'transparent', color: 'white' }}
				/>
			</View>
		)
	}
	renderDoneButton = () => {
		return (
			<View style={styles.buttonCircle}>
				<Icon
					// icon
					name='md-checkmark'
					color='rgba(255, 255, 255, .9)'
					size={24}
					style={{ backgroundColor: 'transparent', color: 'white' }}
				/>
			</View>
		)
	}

	renderItem = (props) => {
		return (
			<LinearGradient
				style={[
					styles.mainContent,
					{
						paddingTop: props.topSpacer,
						paddingBottom: props.bottomSpacer,
						width: props.width,
						height: props.height
					}
				]}
				colors={props.backgroundColors ? props.backgroundColors : ['#3640A7','#3F5DCB']}
				start={{ x: 0, y: 0.1 }}
				end={{ x: 0.1, y: 1 }}
			>
				<Icon style={{ backgroundColor: 'transparent', fontSize: 150, color: 'white' }} name={props.icon} type='FontAwesome5' />
				<View>
					<Text style={styles.title}>{props.title}</Text>
					<Text style={styles.text}>{props.text}</Text>
				</View>
			</LinearGradient>
		)
	}

	render() {
		return (
			<AppIntroSlider
				// config
				slides={this.props.data}
				renderItem={this.props.renderItem || this.renderItem}
				renderDoneButton={this.props.renderDoneButton || this.renderDoneButton}
				renderNextButton={this.props.renderNextButton || this.renderNextButton}
			/>
		)
	}
}

const styles = StyleSheet.create({
	mainContent: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'space-around'
	},
	image: {
		width: 320,
		height: 320
	},
	text: {
		color: 'rgba(255, 255, 255, 0.8)',
		backgroundColor: 'transparent',
		textAlign: 'center',
		paddingHorizontal: 16
	},
	title: {
		fontSize: 22,
		color: 'white',
		backgroundColor: 'transparent',
		textAlign: 'center',
		marginBottom: 16
	}
})
export default OnBoarding1
