import React, {Component} from 'react'
import {View,StyleSheet,Dimensions,TouchableOpacity} from 'react-native'
import Carousel, { ParallaxImage } from 'react-native-snap-carousel'
import { Text,Icon } from 'native-base';

const horizontalMargin = 20
const slideWidth = 280;

const sliderWidth = Dimensions.get('window').width
const itemWidth = slideWidth + horizontalMargin * 2

export default class Carousel1 extends Component {

    render() {
        const {carousel,data} = this.props
        return (
            <View>
                <TouchableOpacity onPress={carousel.header.onPress}>
                    <View style={{justifyContent:'space-between',flexDirection:'row'}}>
                        <Text style={{fontWeight:carousel.header.fontWeight || 'bold',color:carousel.header.color || 'white' ,fontSize:carousel.header.fontSize || 20}}>{carousel.header.title}</Text>
                        <Icon onPress={carousel.header.onPress} name={carousel.header.iconRight || 'chevron-right'} style={{fontSize:carousel.header.iconSize || 20,color:carousel.header.color || 'white'}} type={carousel.header.iconType || 'Entypo'}/>
                    </View>
                </TouchableOpacity>
                <Carousel
                    data={data}

                    renderItem={({item,index},parallaxProps) => {
                        return(
                            <TouchableOpacity onPress={item.onPress}>
                                <View style={{height:carousel.setting ? carousel.setting.height : 200 ,width:carousel.setting ? carousel.setting.width : 640,paddingHorizontal:horizontalMargin}}>
                                        <ParallaxImage
                                            source={{uri:item.url}}
                                            containerStyle={styles.imageContainer}
                                            parallaxFactor={0.35} 
                                            showSpinner={carousel.header.showSpinner || true}
                                            style={{resizeMode:carousel.header.resizeMode || 'cover'}}
                                            spinnerColor={carousel.header.spinnerColor || '#2B79C9'}
                                            {...parallaxProps}  
                                        />
                                </View>
                            </TouchableOpacity>
                        )
                    }}

                    hasParallaxImages={true}
                    sliderWidth={sliderWidth}
                    itemWidth={itemWidth}
                    containerCustomStyle={styles.slider}
                    contentContainerCustomStyle={styles.sliderContentContainer}
                    activeAnimationType={carousel.setting ? carousel.setting.activeAnimationType : 'spring'}
                    inactiveSlideScale={carousel.setting ? carousel.setting.inactiveSlideScale : 1}
                    enableMomentum={carousel.setting ? carousel.setting.enableMomentum : true}
                    activeSlideAlignment={carousel.setting ? carousel.setting.activeSlideAlignment : 'start'}
                    inactiveSlideOpacity={carousel.setting ? carousel.setting.inactiveSlideOpacity : 1}  
                    activeAnimationOptions={carousel.setting ? carousel.setting.activeAnimationOptions : {
                        friction: 4,
                        tension: 40
                    }}
                    loop={carousel.setting ? carousel.setting.loop : true}
                />
            </View>
           
        )
    }
}

const styles = StyleSheet.create({
    imageContainer:{
        borderRadius:10,
        width: '50%',
        height:'70%',
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,
    },
    sliderContentContainer:{
        paddingVertical:10
    },
    slider: {
        marginTop: 15,
        overflow: 'visible' // for custom animations
    },
})