import React, { Component } from 'react'
import { View, Text, Container, Content, Button, Icon, Header, Left, Body, Title } from 'native-base'
import { FlatList, StyleSheet, Modal } from 'react-native'
import { withNavigation } from 'react-navigation'
import ListMenu from './listmenuComponent'

const ModalGridMenu = (props) => (
	<Modal animationType='slide' transparent={false} visible={props.visible} style={{ marginTop: 200 }} onRequestClose={() => {}}>
		<View>
			<Header style={{ backgroundColor: 'white' }}>
				<Left>
					<Button transparent onPress={props.closeModal}>
						<Icon style={{ color: 'black' }} name='arrow-back' />
					</Button>
				</Left>
				<Body>
					<Title style={{ color: 'black' }}>{props.label}</Title>
				</Body>
			</Header>
			<ListMenu menus={props.submenu.menus} gridExtend />
		</View>
	</Modal>
)

class GridMenuItem extends Component {
	state = {
		visible: false
	}

	handlePress = (submenu) => () => {
		this.props.onPress()

		if (submenu) {
			return this.openModal()
		} else if (typeof this.props.to === 'string') {
			return this.props.navigation.navigate(this.props.to)
		}
	}

	openModal = () => {
		this.setState({
			visible: true
		})
	}

	closeModal = () => {
		this.setState({
			visible: false
		})
	}

	componentWillUnmount() {
		this.closeModal
	}

	render() {
		return (
			<View style={[ styles.buttonWrapper, this.props.odd ? styles.oddButton : styles.evenButton ]}>
				{this.state.visible && <ModalGridMenu {...this.props} visible={this.state.visible} closeModal={this.closeModal} />}
				<Button block style={styles.button} onPress={this.handlePress(this.props.submenu && this.props.submenu.menus)}>
					<Icon type='FontAwesome5' name={this.props.icon} style={{ color: '#257FD0dd' }} />
					<Text
						uppercase={false}
						style={{
							marginTop: 10,
							color: '#1E1E1Edd'
						}}
					>
						{this.props.label.toLowerCase()}
					</Text>
				</Button>
			</View>
		)
	}
}

class GridMenu1 extends Component {
	renderItem = (props) => {
		return <GridMenuItem {...props.item} navigation={this.props.navigation} odd={props.index % 2 !== 0 ? true : false} />
	}

	render() {
		return (
			<Container>
				<Content style={styles.container}>
					<FlatList
						data={this.props.data}
						renderItem={this.renderItem}
						numColumns={2}
						style={styles.flatList}
						keyExtractor={(item, key) => item.label + key}
						contentContainerStyle={{
							justifyContent: 'space-between'
						}}
					/>
				</Content>
			</Container>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		width: '100%',
		padding: 10,
		paddingBottom: 0,
		backgroundColor: '#F2F2F2'
	},
	flatList: {
		width: '100%',
		marginBottom: 10
	},
	button: {
		backgroundColor: 'white',
		height: 100,
		flex: 1,
		width: '100%',
		alignSelf: 'center',
		flexDirection: 'column'
	},
	buttonWrapper: {
		width: '50%',
		alignItems: 'center',
		marginBottom: 5
	},
	oddButton: {
		paddingLeft: 2.5
	},
	evenButton: {
		paddingRight: 2.5
	}
})

export default withNavigation(GridMenu1)
