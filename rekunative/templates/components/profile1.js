import React, {Component} from 'react'
import {StyleSheet, ImageBackground, View, FlatList, Modal, Image} from 'react-native'

import {
    Container,
    Content,
    Text,
    Grid,
    Row,
    Col,
    H3,
    Thumbnail,
    Button,
    Icon
} from 'native-base'

export default class Profile1 extends Component {

    render() {
        const {data, btnSubHeader, imagePosts,btnOptionModal} = this.props
        return (
            <Container>
                <Content>
                    <Grid>
                        <Row>
                            <Col style={styles.colHeader}>
                                <Thumbnail
                                    large
                                    style={{
                                    width: 115,
                                    height: 115,
                                    borderRadius: 115 / 2
                                }}
                                    source={{
                                    uri: data.imageUrl || 'https://i.ibb.co/F0mV063/cewe-hacke.jpg'
                                }}/>
                                <Text style={styles.txtName}>{data.name}</Text>
                            </Col>
                        </Row>
                        <Row style={styles.rowAccount}>
                            <Col>
                                <View style={styles.viewCenter}>
                                    <Text style={styles.txtNumber} onPress={data.text1.onPress}>{data.text1.value}</Text>
                                    <Text style={styles.txtInfoAccount} onPress={data.text1.onPress}>{data.text1.label}</Text>
                                </View>
                            </Col>
                            <Col>
                                <View style={styles.viewCenter}>
                                    <Text style={styles.txtNumber} onPress={data.text2.onPress}>{data.text2.value}</Text>
                                    <Text style={styles.txtInfoAccount} onPress={data.text2.onPress}>{data.text2.label}</Text>
                                </View>
                            </Col>
                            <Col>
                                <View style={styles.viewCenter}>
                                    <Text style={styles.txtNumber} onPress={data.text3.onPress}>{data.text3.value}</Text>
                                    <Text style={styles.txtInfoAccount} onPress={data.text3.onPress}>{data.text3.label}</Text>
                                </View>
                            </Col>
                        </Row>
                        <Row
                            style={{
                            paddingVertical: 20
                        }}>
                            <Col
                                style={[
                                styles.colCenter, {
                                    borderRightWidth: 0.92,
                                    borderRightColor: '#D4D6D7'
                                }
                            ]}>
                            <Button block style={styles.btnSubHeader} onPress={btnSubHeader.left.onPress}>
                                <Text style={styles.txtFolMsg}>{btnSubHeader.left.name}</Text>
                            </Button>
                            </Col>
                            <Col style={styles.colCenter}>
                                <Button block style={styles.btnSubHeader} onPress={btnSubHeader.right.onPress}>
                                    <Text style={styles.txtFolMsg} onPress={btnSubHeader.right.onPress}>{btnSubHeader.right.name}</Text>
                                </Button>
                            </Col>
                        </Row>
                        <Row
                            style={{
                            flexWrap: 'wrap',
                            flexDirection: 'row'
                        }}>
                            <FlatList
                                numColumns={3}
                                data={imagePosts}
                                renderItem={({item}) => <Images item={item} btnOptionModal={btnOptionModal}/>  }
                                keyExtractor={item => item}/>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
}

class Images extends Component{
    constructor(props){
        super(props)
        this.state = {
            modalVisible : false,  
        }
    }

    setModalVisible = (visible) => {
        this.setState({
            modalVisible:visible
        })
    }

    render(){
        return(
        <Col onPress={()=>this.setModalVisible(true)}>
            <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => {
                this.setModalVisible(!this.state.modalVisible)
            }}>
                <View style={{paddingHorizontal:10,paddingVertical:5,width:'100%', paddingTop:10,flexDirection:'row',justifyContent:'space-between'}}>
                    <Text style={{fontSize:20,fontWeight:'200'}} onPress={()=>this.setModalVisible(!this.state.modalVisible)}>Close</Text>
                    <Icon onPress={this.props.btnOptionModal.onPress} name='ellipsis-h' type='FontAwesome'/>
                </View>
                <View>
                    <Image style={styles.photo} source={{uri:this.props.item.url || 'http://vollrath.com/ClientCss/images/VollrathImages/No_Image_Available.jpg' }} />
                </View>
                <View style={styles.viewIconBottom}>
                    <Row size={3}>
                        <Col style={styles.colIcon}>
                            <Icon onPress={this.props.item.onPressLike} name="heart-o" style={styles.txtIcon} type='FontAwesome'/>
                            <Text style={{fontSize:12}}>{this.props.item.like}</Text>
                        </Col>
                        <Col style={styles.colIcon}>
                            <Icon onPress={this.props.item.onPressComment} name="comment-o" style={styles.txtIcon}  type='FontAwesome'/>
                            <Text style={{fontSize:12}}>{this.props.item.comment}</Text>
                        </Col>
                        <Col style={styles.colIcon}>
                            <Icon onPress={this.props.item.onPressUser} name="user-o" style={styles.txtIcon} type='FontAwesome'/>
                            <Text style={{fontSize:12}}>{this.props.item.comment}</Text>
                        </Col>
                    </Row>
                </View>
                            
            </Modal>
            <View style={styles.colPhoto}>
                <ImageBackground 
                        source={{
                        uri: this.props.item.url || 'http://vollrath.com/ClientCss/images/VollrathImages/No_Image_Available.jpg'
                    }}
                        resizeMode='cover'
                        style={styles.photo}
                    />
            </View>
        </Col>     
        )
    }
}

const styles = StyleSheet.create({
    colHeader: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30
    },
    txtInfoAccount:{    
        fontWeight:'100'
    },
    txtName: {
        marginTop: 12,
        fontWeight: 'bold',
        fontSize: 23
    },
    btnSubHeader:{
        backgroundColor:'transparent',
        elevation:0
    },
    viewCenter: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 10
    },
    txtNumber: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    rowAccount: {
        marginTop: 30,
        borderTopWidth: 0.92,
        borderTopColor: '#D4D6D7',
        borderBottomColor: '#D4D6D7',
        borderBottomWidth: 0.92,
        paddingVertical: 10
    },
    colCenter: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    txtFolMsg: {
        fontSize: 17,
        color: 'salmon',
        fontWeight: 'bold'
    },
    txtIcon:{
        fontSize:20,
        marginRight:10,
    },
    colPhoto: {
        paddingRight: 1.3,
        paddingLeft: 1.3,
        paddingTop: 1.3,
        paddingBottom: 1.3,
        alignItems: 'center',
        justifyContent: 'center',
        height: 130,
    },
    photo: {
        height: '100%',
        width: '100%',
    },
    colIcon:{
        alignItems:'center',
        justifyContent:'center',
        flexDirection:'row'
    },
    viewIconBottom:{
        width:'100%',
        position:'absolute',
        backgroundColor:'white',
        bottom:0,
        height:'7%'
    }
})