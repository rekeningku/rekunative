import React, { Component } from 'react'
import { Container, Content, List, ListItem, Text, Icon } from 'native-base'
import { FlatList, StyleSheet } from 'react-native'

export default class ListMenu extends Component {
	render() {
		const { data } = this.props

		return (
			<Container>
				<Content>
					<List>
						<FlatList
							data={data}
							renderItem={({ item }) => (
								<ListItem onPress={item.onPress}>
									<Icon name={item.icon} type={item.type} style={styles.icon} />
									<Text>{item.label}</Text>
								</ListItem>
							)}
							keyExtractor={(index) => index.toString()}
						/>
					</List>
				</Content>
			</Container>
		)
	}
}

const styles = StyleSheet.create({
	icon: {
		fontSize: 40,
		color: '#4d94ff',
		marginRight: 10
	}
})
