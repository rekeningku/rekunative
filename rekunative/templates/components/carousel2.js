import React, {Component} from 'react'
import {View,ImageBackground,ScrollView,TouchableOpacity,FlatList} from 'react-native'
import { Text, Icon } from 'native-base';

export default class Carousel2 extends Component {
    render() {
        const {carousel,data} = this.props
        return (
            <View>
                <TouchableOpacity onPress={carousel.header.onPress}>
                    <View style={{justifyContent:'space-between',flexDirection:'row'}}>
                        <Text style={{fontSize:carousel.header.fontSize || 20,color:carousel.header.color || '#fff',fontWeight:carousel.header.fontWeight || 'bold' }}>{carousel.header.title}</Text>
                        <Icon name={carousel.header.iconRight || 'chevron-right'} style={{color:carousel.header.color || '#fff',fontSize:carousel.header.iconSize || 17}} type={carousel.header.typeIcon || 'Entypo'}/>
                    </View>
                </TouchableOpacity>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{flexDirection: 'row'}}>
                    <FlatList
                        data={data}
                        horizontal
                        renderItem={({item}) =>(
                            <View style={{height:carousel.setting ? carousel.setting.height : 180 ,width: carousel.setting ? carousel.setting.width : 330,paddingHorizontal:carousel.setting ? carousel.setting.paddingHorizontal : 8,paddingVertical: carousel.setting ? carousel.setting.paddingVertical : 10}}>
                                <TouchableOpacity style={{borderRadius:carousel.setting ? carousel.setting.imageBorderRadius : 5}} onPress={item.onPress}>
                                    <ImageBackground imageStyle={{ borderRadius:carousel.setting ? carousel.setting.imageBorderRadius : 5}} source={{uri:item.url || 'http://vollrath.com/ClientCss/images/VollrathImages/No_Image_Available.jpg' }} resizeMode='cover' style={{height:'100%',width:'100%',alignItems:'flex-end'}}>
                                        <Text style={{fontSize:item.fontSize || 20 ,color:item.color || '#000' ,fontWeight:'bold',position:'absolute',bottom:13,right:20}}>{item.title || ''}</Text>
                                    </ImageBackground>
                                </TouchableOpacity>
                            </View>
                        )}
                        keyExtractor={(item,index) => index.toString()} 
                    />
                </ScrollView>
            </View>
        )
    }
}
