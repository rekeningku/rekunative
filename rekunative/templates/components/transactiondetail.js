import React, { Component } from 'react'
import { ImageBackground, View, StyleSheet, Image, FlatList } from 'react-native'
import { Container, Content, Text, Grid, Row, Col, H3, Icon, Button } from 'native-base'

export default class TransactionDetail extends Component {
  render() {
      const {transaction, btnChatSeller, btnDetailOrder, btnCancelOrder, header} = this.props
    return (
        <Container>
            <Content>
                <Grid>
                    <Row>
                        <Col style={{height:140}}>
                            <ImageBackground resizeMode='cover' source={{uri:header.imageBackgroundUrl || 'https://i.pinimg.com/564x/9b/ea/67/9bea67b08810012f4f30878a11a2f6ef.jpg' }} style={styles.imgBackground}>
                                <View style={styles.viewImgBackground}>
                                    <H3 style={{color:'#fff',fontWeight:'bold'}}>{transaction.waitingForPayment === true ? 'Menunggu Pembayaran' : 'Telah Dibayar'}</H3>
                                    <Text style={{color:'#fff'}}>{header.subtitle} {header.text}</Text>
                                </View>
                            </ImageBackground>
                        </Col>
                    </Row>
                    <View style={styles.viewRow}>
                        <Row>
                            <Col>
                                <Text style={{color:'gray',marginBottom:20}}>Pengiriman dan Penagihan ke</Text>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <Text style={{marginBottom:5}}>{transaction.account.fullname}</Text>
                                <Text style={{marginBottom:5}}>{transaction.account.phone}</Text>
                                <Text style={{color:'gray',fontSize:15}}>{transaction.account.address}</Text>
                            </Col>    
                        </Row>
                    </View>
                    <FlatList
                        data={transaction.orders}
                        renderItem={({item}) =>(
                            <View style={[styles.viewRow,{paddingTop:11}]}>
                                <Row size={2} style={{justifyContent:'space-between'}}>
                                    <Text style={{fontSize:13}}>Paket 1</Text>
                                    <Text style={{fontSize:13,fontStyle:'italic',color:'gray'}}>Menunggu Pembayaran</Text>
                                </Row>
                                <Row>
                                    <Col>
                                        <Text style={{fontSize:12,color:'gray'}}>Dijual oleh {item.store} <Icon style={{fontSize:10,color:'gray'}} name='chevron-right' type='FontAwesome' /></Text>
                                        <Text style={{fontSize:12,color:'gray'}}>Dapatkan pada {item.estimatedTime} , {item.delivery}</Text>
                                    </Col>
                                </Row>
                                <Row style={{marginVertical:25}}>
                                    <View style={{height:80,width:80,marginRight:14}}>
                                        <Image source={{uri:item.imageUrl || 'https://pngimage.net/wp-content/uploads/2018/06/no-image-png-2.png'}} style={{height:'100%',width:'100%'}} />
                                    </View>
                                    <View style={{width:'70%'}}>
                                        <Text onPress={btnDetailOrder.onPress} style={{fontSize:14}} numberOfLines={2}>{item.name}</Text>
                                        <Text style={{color:'gray',fontSize:13}}>{item.warranty == true ? 'Bergaransi' : 'Tidak ada Garansi'}</Text>
                                        <Text style={{fontSize:14}}>Rp.{item.priceItem}</Text>
                                        <Text style={{color:'gray',fontSize:14}}>x {item.qty}</Text>
                                    </View>
                                </Row>
                                <Row style={{justifyContent: 'flex-end',marginTop:-20}}>
                                    <Button bordered iconLeft dark onPress={btnCancelOrder.onPress}>
                                        <Icon name='info-circle' type='FontAwesome' />
                                        <Text>{btnCancelOrder.label || 'BATAL'}</Text>
                                    </Button>
                                </Row>
                                <Row style={styles.rowChatSeller}>
                                    <Button style={styles.btnChatSeller} onPress={btnChatSeller.onPress}>
                                    <Icon style={{color:'gray',marginRight:0}} name='comment' type='FontAwesome' />
                                        <Text style={{color:'gray'}}>{btnChatSeller.label || 'Chat Seller'}</Text>
                                    </Button>
                                </Row>
                            </View>
                        )}
                        keyExtractor={(item,index) => index.toString() }
                    />

                    <FlatList
                        data={transaction.orders}
                        renderItem={({item}) => (
                            <View style={styles.viewRow}>
                                <Row>
                                    <Text onPress={btnDetailOrder.onPress} style={{color:'#3DAE97',fontSize:16}}>Pesanan #{item.orderCode}</Text>
                                </Row>
                                <Row size={2}>
                                    <Col>
                                        <Text style={{fontSize:13,color:'gray'}}>Dipesan pada</Text>
                                        <Text style={{fontSize:13,color:'gray'}}>{item.orderTime}</Text>
                                    </Col>
                                    <Col style={{alignItems:'space-between'}}>
                                        <Text onPress={btnDetailOrder.onPress} style={{fontSize:13,color:'salmon'}}>Lihat Petunjuk</Text>
                                    </Col>
                                </Row>
                            </View>
                        )}
                        keyExtractor={(item,index) => index.toString()}
                    />

                 
                    <View style={styles.viewRow}>
                        <Row style={styles.rowBiaya}>
                            <Text style={{color:'gray'}}>Sub Total</Text>
                            <Text>Rp. {transaction.subTotal}</Text>
                        </Row>
                        <Row style={styles.rowBiaya}>
                            <Text style={{color:'gray'}}>Biaya Pengiriman</Text>
                            <Text>Rp. {transaction.shippingCosts}</Text>
                        </Row>
                        <Row style={styles.rowJmlBiaya}>
                            <Text style={{color:'gray'}}>Jumlah Potongan</Text>
                            <Text>-Rp. {transaction.piece}</Text>
                        </Row>
                        <Row style={styles.rowTotal}>
                            <Text style={{fontSize:14}}>{transaction.orders.length} Barang {transaction.orders.length} Paket</Text>
                        </Row>
                        <Row style={styles.rowTotal}>
                            <Text>Total:</Text>
                            <Text style={{color:'salmon'}}> Rp.{transaction.total} </Text>
                        </Row>
                        <Row style={{justifyContent:'flex-end'}}>
                            <Text style={{color:'gray',fontSize:13}}>Metode Pembayaran</Text>
                            <Text style={{fontSize:13}}> Melalui {transaction.paymentMethod}</Text>
                        </Row>
                    </View>
                </Grid>
            </Content>
        </Container>
    )
  }
}

const styles = StyleSheet.create({
    imgBackground:{
        height:'100%',
        width:'100%'
    },
    viewImgBackground:{
        paddingHorizontal:30,
        paddingVertical:30,
        width:260
    },
    viewRow:{
        paddingHorizontal:20,
        paddingVertical:20 ,
        borderBottomColor:'#F1F1F1',
        borderBottomWidth:10
    },
    rowBiaya:{
        justifyContent:'space-between',
        marginBottom:12
    },
    rowJmlBiaya:{
        justifyContent:'space-between',
        borderBottomColor:'#F1F1F1',
        borderBottomWidth:1,
        paddingBottom:12
    },
    rowTotal:{
        marginBottom:10,
        justifyContent:'flex-end'
    },
    rowChatSeller:{
        marginVertical:20,
        borderTopColor:'#F1F1F1',
        borderTopWidth:0.80
    },
    btnChatSeller:{
        width:'100%',
        justifyContent:'center',
        elevation:0,
        backgroundColor:'transparent'
    },
})