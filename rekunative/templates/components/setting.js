import React, { Component } from 'react'
import {StyleSheet,View} from 'react-native'
import { Switch } from 'react-native-switch'

import {
  Container,
  Content,
  Button,
  Text,
  Icon,
  Grid,
  Row,
  Col,
  Left,
  Right,
} from 'native-base'

const Title = ({children,color}) =>(
	<View style={styles.titleContainer}>
		<Text style={[styles.title,{color}]}>{children}</Text>
	</View>
)
	
export default class Setting extends Component {
	state={
		pushNotifications:false,
		refreshAuto:false,
		socmed1:false,
		socmed2:false,
		socmed3:false,
	}

	handleChangeSwitch = (field,value,props) => {
		this.setState({
			[field]:value
		})
		return props(value)
	}

	handleChangeSocmed = (field,value) =>{
		this.setState({
			[field]:value
		})
	}
	render() {
		const {data1,data2,data3,dataSwitch,setting} = this.props
		return (
			<Container>
				<Content>
					<Grid>
						<Row>
							<Col style={{width:'100%',marginTop:10}}>
								<Title color={setting ? setting.title1.color : '#22619F'}>{data1.title.label}</Title>
								<Button block style={styles.btnSetting}>
									<Text style={{color:'#000'}}>{data1.field1.label}</Text>
								</Button>
								<Button block style={styles.btnSetting}>
									<Text style={{color:'#000'}}>{data1.field2.label}</Text>
								</Button>
							</Col>
						</Row>
						<Row style={styles.rowSwitch}>
							<Left>
								<Text style={styles.labelSwitch}>{data1.field3.label}</Text>
							</Left>
							<Right>
								<Switch
								value={this.state.pushNotifications}
								onValueChange={() => this.handleChangeSwitch('pushNotifications',!this.state.pushNotifications,data1.field3.onChange)}
								backgroundActive={setting ? setting.switch1.backgroundActive : '#22619F'}
								backgroundInactive={setting ? setting.switch1.backgroundInactive : '#CACACA'}
								circleActiveColor={setting ? setting.switch1.circleActiveColor : '#FFFFFF'}
								circleInActiveColor={setting ? setting.switch1.circleInActiveColor : '#FFFFFF'}
								innerCircleStyle={setting ? setting.switch1.innerCircleStyle : { borderColor: '#CACACA' }}
								/>
							</Right>
						</Row>
						<Row style={styles.rowSwitch}>
							<Left>
								<Text style={styles.labelSwitch}>{data1.field4.label}</Text>
							</Left>
							<Right>
								<Switch
								value={this.state.refreshAuto}
								onValueChange={() => this.handleChangeSwitch('refreshAuto',!this.state.refreshAuto,data1.field4.onChange)}
								backgroundActive={setting ? setting.switch2.backgroundActive : '#22619F'}
								backgroundInactive={setting ? setting.switch2.backgroundInactive : '#CACACA'}
								circleActiveColor={setting ? setting.switch2.circleActiveColor : '#FFFFFF'}
								circleInActiveColor={setting ? setting.switch2.circleInActiveColor : '#FFFFFF'}
								innerCircleStyle={setting ? setting.switch2.innerCircleStyle : { borderColor: '#CACACA'}}
								/>
							</Right>
						</Row>

						<Row>
							<Col>
								<Title color={setting ? setting.title2.color : '#22619F'}>{data2.title.name}</Title>
							</Col>
						</Row>
						<Row style={styles.rowSocmed} onPress={() => this.handleChangeSocmed('socmed1',!this.state.socmed1)}>
							<Left style={{flexDirection:'row',alignItems:'center'}}>
								<Icon style={[styles.iconSocmed,{color: this.state.socmed1 ? setting ? setting.socmed1.activeColor :'#47ABDC' : '#CACACA'}]}name={data2.field1.iconLeft} type='FontAwesome'/>
								<Text style={[styles.labelSocmed,{color: this.state.socmed1 ? setting ? setting.socmed1.activeColor :'#47ABDC' : '#CACACA'}]}>{data2.field1.label}</Text>
							</Left>
							<Right>
								<Icon style={{fontSize:19,color:this.state.socmed1 ? setting ? setting.socmed1.activeColor :'#47ABDC' : '#CACACA',marginRight:25}} name={data2.field1.iconRight} type='FontAwesome'/>
							</Right>
						</Row>
						<Row style={styles.rowSocmed} onPress={() => this.handleChangeSocmed('socmed2',!this.state.socmed2)}>
							<Left style={{flexDirection:'row',alignItems:'center'}}>
								<Icon style={[styles.iconSocmed,{color:this.state.socmed2 ? setting ? setting.socmed2.activeColor : '#E44339' : '#CACACA'}]} name={data2.field2.iconLeft} type='FontAwesome'/>
								<Text style={[styles.labelSocmed,{color:this.state.socmed2 ? setting ? setting.socmed2.activeColor : '#E44339' : '#CACACA'}]}>Find Firends With Google</Text>
							</Left>
							<Right>
								<Icon style={{fontSize:19,color:this.state.socmed2 ? setting ? setting.socmed2.activeColor : '#E44339' : '#CACACA',marginRight:25}} name={data2.field2.iconRight} type='FontAwesome'/>
							</Right>
						</Row>
						<Row style={styles.rowSocmed} onPress={() => this.handleChangeSocmed('socmed3',!this.state.socmed3)}>
							<Left style={{flexDirection:'row',alignItems:'center'}}>
								<Icon style={[styles.iconSocmed,{color:this.state.socmed3 ? setting ? setting.socmed3.activeColor : '#34578A' : '#CACACA'}]} name={data2.field3.iconLeft} type='FontAwesome'/>
								<Text style={[styles.labelSocmed,{color:this.state.socmed3 ? setting ? setting.socmed3.activeColor : '#34578A' : '#CACACA'}]}>{data2.field3.label}</Text>
							</Left>
							<Right>
								<Icon style={{fontSize:19,color:this.state.socmed3 ? setting ? setting.socmed3.activeColor : '#34578A' : '#CACACA',marginRight:25}} name={data2.field3.iconRight} type='FontAwesome'/>
							</Right>
						</Row>

						<Row>
							<Col style={{width:'100%',marginTop:10}}>
								<Title color={setting ? setting.title3.color : '#22619F'}>{data3.title.name}</Title>

								<Button block style={styles.btnSetting}>
									<Text style={{color:'#000'}}>{data3.field1.label}</Text>
								</Button>
								<Button block style={styles.btnSetting}>
									<Text style={{color:'#000'}}>{data3.field2.label}</Text>
								</Button>
								<Button block style={styles.btnSetting}>
									<Text style={{color:'#000'}}>{data3.field3.label}</Text>
								</Button>
								<Button block style={styles.btnSetting}>
									<Text style={{color:'#000'}}>{data3.field4.label}</Text>
								</Button>
							</Col>
						</Row>
					</Grid>
				</Content>
			</Container>
		)
	}
}

const styles = StyleSheet.create({
	//title
	titleContainer: {
		paddingHorizontal:16,
		borderBottomWidth: 0.5,
		borderBottomColor: '#D9D5DC',
		paddingBottom: 10,
		paddingTop:15
	},
	title: {
		fontWeight: '500',
		fontSize: 15,
	},

	//label switch
	rowSwitch:{
		paddingTop:10,
		paddingBottom: 10,
		marginRight:12
	},

	labelSwitch:{
		marginLeft:16
	},

	//button setting
	btnSetting:{
		elevation:0,
		backgroundColor:'transparent',
		justifyContent:'flex-start',
		borderBottomWidth: 0.5,
		borderBottomColor: '#D9D5DC',
		height:70
	},

	//socmed
	rowSocmed:{
		paddingTop:10,
		paddingBottom: 10
	},
	iconSocmed:{
		width:30,
		marginLeft:12
	},
	labelSocmed:{
		marginLeft:10,
		width:'100%'
	},

})
