import React, { Component } from 'react'
import { Container, Header, Item, Input, Icon, Text, Content, List, ListItem, Thumbnail, View } from 'native-base'
import { StyleSheet, TouchableOpacity, TouchableWithoutFeedback } from "react-native"
import { createFilter } from 'react-native-search-filter'
import showPopupMenu from 'react-native-popup-menu-android'


export default class Contact extends Component {
  constructor(props) {
    super(props)
    this.state = {
      searchTerm: '',
      click: false
    }
  }
  searchUpdated(term) {
    this.setState({ searchTerm: term })
  }

  handleSearch = (val) => {
    this.setState({
      click: val
    })
  }

  render() {
    const { data, filterKey } = this.props
    const filteredContacts = data.filter(createFilter(this.state.searchTerm, filterKey || 'name'))
    return (
      <Container>
        {!this.state.click ? (
          <Header style={styles.header}>
            <View style={styles.viewHeaderLeft}>
              <Icon name="arrowleft" type='AntDesign' style={styles.iconHeaderLeft} />
            </View>
            <View style={styles.viewHeader}>
              <Text style={styles.textHeader}>Select contact</Text>
              <Text style={styles.textTotalContacts}>3 contacts</Text>
            </View>
            <View style={styles.viewIcon}>
              <TouchableWithoutFeedback onPress={() => this.handleSearch(!this.state.click)} underlayColor='#00000011'>
                <Icon name="ios-search" style={styles.icon} />
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={this.showMore}>
                <View ref={this.refMoreButton}>
                  <Icon name="options-vertical" type='SimpleLineIcons' style={styles.iconHeaderOption} />
                </View>
              </TouchableWithoutFeedback>
            </View>
          </Header>
        ) : (
            <Header searchBar rounded>
              <Item style={styles.searchBar}>
                <TouchableOpacity onPress={() => this.handleSearch(!this.state.click)} >
                  <Icon name="arrowleft" type='AntDesign' />
                </TouchableOpacity>
                <Input placeholder="Search" onChangeText={term => { this.searchUpdated(term) }} />
              </Item>
            </Header>
          )}
        <Content>
          <List>
            {filteredContacts.map(data => {
              return (
                <ListItem avatar style={styles.listItem} onPress={data.onPress} key={data.id}>
                  <Thumbnail source={{ uri: data.image }} style={styles.thumbnail} />
                  <View>
                    <Text style={styles.contactName}>{data.name}</Text>
                    {typeof data.status === 'string' ? (
                      <Text style={{fontSize: 18, marginLeft: 20}}>
                            {data.status}
                      </Text>
                    ):data.status}
                  </View>
                </ListItem>
              )
            })}
          </List>
        </Content>
      </Container>
    )
  }
  refMoreButton = el => this.moreButton = el
  showMore = () => {
    showPopupMenu(
      this.props.popUpMenu,
      this.handleMoreItemSelect,
      this.moreButton
    )
  }
  handleMoreItemSelect = this.props.onPressMenu
}


const styles = StyleSheet.create({
  header: {
    backgroundColor: '#0099ff'
  },
  viewHeader: {
    flex: 1,
    justifyContent: "center"
  },
  viewHeaderLeft: {
    justifyContent: "center"
  },
  iconHeaderLeft: {
    color: "#fff"
  },
  iconHeaderOption: {
    color: "#fff",
    alignContent: "flex-end",
    fontSize: 20,
    marginTop: 3
  },
  textHeader: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
    marginLeft: 15
  },
  textTotalContacts: {
    color: "white",
    fontSize: 12,
    fontWeight: "bold",
    marginLeft: 15
  },
  viewIcon: {
    justifyContent: "center",
    flexDirection: "row",
    marginTop: 12
  },
  icon: {
    color: "#fff",
    alignContent: "flex-end",
    marginRight: 20,
    fontSize: 25
  },
  searchBar: {
    borderRadius: 0
  },
  listItem: {
    borderBottomWidth: 0.5,
    margin: 5
  },
  thumbnail: {
    height: 40, 
    width: 40
  },
  contactName: {
    fontSize: 18,
    marginLeft: 20,
    fontWeight: "bold"
  },
  status: {
    fontSize: 18,
    marginLeft: 20
  }
})