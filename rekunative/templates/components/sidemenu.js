import React, { Component } from 'react'
import { Image, TouchableOpacity, View, StyleSheet, FlatList } from 'react-native'
import {
    Container,
    Content,
    Button,
    Text,
    Icon,
    Row,
    Col,
    Grid,
    H3,
    Drawer,
    Left,
    Right,
    H1
  } from 'native-base'

class SideBar extends Component {
  render() {
    const {data} = this.props
    return (
        <Container>
            <Content>
                <Grid>
                    <Row style={[styles.itemCenter,{marginTop:20}]}>
                        <Col style={{height:120,width:120}}>
                            <Image source={{uri:'https://imgdb.net/images/4877.png'}} style={{height:'100%',width:'100%'}} />
                        </Col>
                    </Row>
                    <Row>
                        <Col style={[styles.itemCenter,{marginTop:12}]}>
                            <H3>Side Menu</H3>
                        </Col>
                    </Row>
                    <Row size={3} style={{marginTop:30}}>
                        <Col>
                            <FlatList
                                data={data}
                                renderItem={({item}) => (
                                    <Button block style={styles.btnMenu} onPress={item.onPress}>
                                        <Left style={styles.leftMenu}>
                                            <Icon style={[styles.iconMenu,{color:item.color || '#22619F'}]} name={item.iconLeft} type='FontAwesome' />
                                            <Text style={styles.txtMenu}>{item.name}</Text>
                                        </Left>
                                        <Right>
                                            <Icon style={[styles.iconMenu,{color:item.color || '#22619F',fontSize:15}]} name={item.iconRight || 'chevron-right'} type='FontAwesome' />                               
                                        </Right>
                                    </Button>
                                )}
                                keyExtractor={(item,index) => index.toString()}  
                             />
                        </Col>
                    </Row>
                </Grid>
            </Content>
        </Container>
        
    )
  }
}

export default class SideMenu extends Component{
    
    closeDrawer = () => {
        this.drawer._root.close()
    }   
      openDrawer = () => {
        this.drawer._root.open()
    }

    render(){
        return(
            <Drawer
                ref={(ref) => { this.drawer = ref }}
                content={<SideBar data={this.props.data}  navigator={this.navigator} />}
                onClose={() => this.closeDrawer()} >
                <Container>
                    <Content>
                        <View style={styles.viewClickMe}>
                            <TouchableOpacity style={{marginVertical:'50%'}} onPress={()=>this.openDrawer()}>
                                <H1>Click Me</H1>
                            </TouchableOpacity>
                        </View>
                    </Content>
                </Container>
            </Drawer>
        )
    }
} 

const styles = StyleSheet.create({
    itemCenter:{
        justifyContent:'center',
        alignItems:'center'
    },
    btnMenu:{
        flexDirection:'row',
        borderTopWidth:0.98,
        height:50,
        borderColor:'#D9D5DC',
        backgroundColor:'transparent',
        elevation:0
    },
    iconMenu:{
        fontSize:25,
        width:30,
    },
    leftMenu:{
        paddingHorizontal:10,
        flexDirection:'row'
    },
    txtMenu:{
        fontWeight:'bold',
        marginLeft:15
    },
    viewClickMe:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})