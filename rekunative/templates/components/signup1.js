import React, { Component } from "react"
import {Text, Button, Content, Form, Item, Input, Thumbnail, View} from "native-base"
import { StyleSheet, Dimensions, PixelRatio, TouchableOpacity } from "react-native"
import LinearGradient from "react-native-linear-gradient"
import { Formik } from "formik"
import * as Yup from "yup"
import { buildYup } from 'json-schema-to-yup'

let screenWidth = Dimensions.get("window").width

let screenHeight = Dimensions.get("window").height

const widthPercentageToDP = widthPercent => {
  const elemWidth = parseFloat(widthPercent)

  return PixelRatio.roundToNearestPixel((screenWidth * elemWidth) / 100)
}

const heightPercentageToDP = heightPercent => {
  const elemHeight = parseFloat(heightPercent)

  return PixelRatio.roundToNearestPixel((screenHeight * elemHeight) / 100)
}

const listenOrientationChange = that => {
  Dimensions.addEventListener("change", newDimensions => {
    screenWidth = newDimensions.window.width
    screenHeight = newDimensions.window.height

    that.setState({ orientation: screenWidth < screenHeight ? "portrait" : "landscape" })
  })
}

const removeOrientationListener = () => {
  Dimensions.removeEventListener("change", () => {})
}

export {
  widthPercentageToDP,
  heightPercentageToDP,
  listenOrientationChange,
  removeOrientationListener
}

//Validation Rules
const SignUp1Schema = Yup.object().shape({
  email: Yup.string().required("Required!").email("Email not valid!"),
  name: Yup.string().required("Required!"),
  password: Yup.string().required("Required!").min(6, "Min 6 Character"),
  password2: Yup.string().required("Required!").oneOf([Yup.ref("password")],"Your password and confirmation password do not match.") //Confirm Passsword
})

export default class Signup1 extends Component {
  static navigationOptions = {
    header: null
  }

  state = {
    scrollEnabled: true
  }

  schema = () => {
    return(
    this.props.config && 
    this.props.schema && 
    buildYup(
      {
        ...this.props.schema,
        properties:{
          ...this.props.schema.properties,
          name:{
            ...this.props.schema.properties.name,
          },
          email:{
            ...this.props.schema.properties.email,
						pattern: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          },
          password:{
            ...this.props.schema.properties.password,
          },
          passwordConfirmation:{
            ...this.props.schema.properties.passwordConfirmation,
          }
        }
      },
      {
        ...this.props.config,
        properties:{
          ...this.props.config.errMessages,
          name:{
            ...this.props.config.errMessages.name,
          },
          email:{
            ...this.props.config.errMessages.email,
							pattern: 'Not a valid email address'
          },
          password:{
              ...this.props.config.errMessages.password,
          },
          passwordConfirmation:{
            ...this.props.config.errMessages.passwordConfirmation,
          }
        }
      }
    ))
  }

  // handleChange
  handleChangeName = (userHandle, validation) => value => {
    validation("name")(value)
    return userHandle(value)
  }
  handleChangeEmail = (userHandle, validation) => value => {
    validation("email")(value)
    return userHandle(value)
  }
  handleChangePassword = (userHandle, validation) => value => {
    validation("password")(value)
    return userHandle(value)
  }
  //Confirmation Password
  handleChangePassword2 = (userHandle, validation) => value => {
    validation("password2")(value)
    return userHandle(value)
  }

  //handleSubmit
  handleSubmit = (setFieldTouched, handleSubmit) => value => {
    setFieldTouched("name", true, true)
    setFieldTouched("email", true, true)
    setFieldTouched("password", true, true)
    setFieldTouched("password2", true, true)
    return handleSubmit(value)
  }

  render() {
    const { image, field1, field2, field3, field4, customInfo, btnSignUp, txtTitle } = this.props
    return (
      <Formik
        initialValues={{
          name: "",
          email: "",
          password: "",
          password2: ""
        }}
        validationSchema={this.schema() || SignUp1Schema}
        onSubmit={this.props.btnSignUp.onPress}
        validateOnChange={false}
        validateOnBlur={false}
        render={props => {
        let item
        if(props.errors.password2){
            item =  <Item rounded style={styles.inputErrorPassword2}>
                        <Input
                            placeholder={field3.label}
                            style={styles.fontInput}
                            onChangeText={this.handleChangePassword(this.props.field3.onChangeText, props.handleChange)}
                            onFocus={() => this.setState({scrollEnabled: !this.state.scrollEnabled})}
                            onBlur={() =>this.setState({scrollEnabled: !this.state.scrollEnabled})}
                        />
                    </Item>

        }
        else{
            item = <Item rounded style={styles.input}>
                        <Input
                            placeholder={field3.label}
                            style={styles.fontInput}
                            onChangeText={this.handleChangePassword(this.props.field3.onChangeText, props.handleChange)}
                            onFocus={() => this.setState({scrollEnabled: !this.state.scrollEnabled})}
                            onBlur={() =>this.setState({scrollEnabled: !this.state.scrollEnabled})}
                        />
                    </Item>
        }
          return (
            <Content style={styles.container} scrollEnabled={this.state.scrollEnabled} showsVerticalScrollIndicator={false}>
              <View style={styles.viewFront}>
                <View>
                  <Thumbnail
                    source={{ uri: image.url }}
                    style={styles.thumbnail}
                  />
                  <Text style={styles.title}> {txtTitle.label} </Text>
                </View>
                <Form style={styles.form}>
                  <View style={styles.viewForm}>
                    <Item rounded style={styles.input}>
                      <Input
                        placeholder={field1.label}
                        style={styles.fontInput}
                        onChangeText={this.handleChangeName(this.props.field1.onChangeText, props.handleChange)}
                        onFocus={() => this.setState({scrollEnabled: !this.state.scrollEnabled})}
                        onBlur={() => this.setState({scrollEnabled: !this.state.scrollEnabled})}
                      />
                    </Item>

                    {props.errors.name && (<Item style={styles.borderHelperText}><Text style={styles.helperText}>{props.errors.name || ""}</Text></Item>)}

                    <Item rounded style={styles.input}>
                      <Input
                        placeholder={field2.label}
                        style={styles.fontInput}
                        onChangeText={this.handleChangeEmail(this.props.field2.onChangeText, props.handleChange)}
                        onFocus={() => this.setState({scrollEnabled: !this.state.scrollEnabled})}
                        onBlur={() => this.setState({scrollEnabled: !this.state.scrollEnabled})}
                      />
                    </Item>

                    {props.errors.email && (<Item style={styles.borderHelperText}><Text style={styles.helperText}>{props.errors.email || ""}</Text></Item>)}
                    
                    {item}

                    {props.errors.password && (<Item style={styles.borderHelperText}><Text style={styles.helperText}>{props.errors.password || ""}</Text></Item>)}

                    <Item rounded style={styles.input}>
                      <Input
                        placeholder={field4.label}
                        style={styles.fontInput}
                        onChangeText={this.handleChangePassword2(this.props.field4.onChangeText, props.handleChange)}
                        onFocus={() => this.setState({scrollEnabled: !this.state.scrollEnabled})}
                        onBlur={() => this.setState({scrollEnabled: !this.state.scrollEnabled})}
                      />
                    </Item>

                    {props.errors.password2 && (<Item style={styles.borderHelperText}><Text style={styles.helperText}>{props.errors.password2 || ""}</Text></Item>)}

                  </View>
                  <LinearGradient
                    colors={[btnSignUp.color1 || '#46b6fb', btnSignUp.color2 || '#2B79C9']}
                    style={styles.linearGradient}
                    start={{
                      x: 1,
                      y: 0
                    }}
                    end={{
                      x: 0,
                      y: 1
                    }}
                    onFocus={() => this.setState({scrollEnabled: !this.state.scrollEnabled})}
                    onBlur={() => this.setState({scrollEnabled: !this.state.scrollEnabled})}
                  >
                    <Button full rounded style={styles.button} onPress={this.handleSubmit(props.setFieldTouched,props.handleSubmit)}>
                      <Text style={styles.fontButton}>{btnSignUp.label}</Text>
                    </Button>
                  </LinearGradient>
                  {typeof customInfo.text === 'string' ? (
                    <View style={styles.customView}>
                      <Text>{customInfo.text}</Text>
                      <TouchableOpacity  onPress={()=>alert('Login')}>
                          <Text style={styles.customText}> {customInfo.textBold}</Text>
                      </TouchableOpacity>
                    </View>
                  ):customInfo}
                </Form>
              </View>
            </Content>
          )
        }}
      />
    )
  }
}

const styles = StyleSheet.create({
  viewFront: {
    flex: 1,
    width: widthPercentageToDP("94%"),
    height: heightPercentageToDP("90%")
  },
  viewForm: {
    flex: 1, 
    justifyContent: "space-around",
  },
  form: {
    flex: 1, 
    alignItems: "stretch"
  },
  thumbnail: {
    width: 100,
    height: 100,
    marginTop: 30,
    marginBottom: 5,
    alignSelf: 'center'
  },
  title: {
    fontWeight: "normal",
    alignSelf: "center",
    fontSize: heightPercentageToDP("3.5%"),
    marginBottom: 80
  },
  linearGradient: {
    paddingLeft: 0,
    paddingRight: 0,
    borderRadius: 20,
    marginTop: 10
  },
  button: {
    width: "100%",
    alignSelf: "center",
    padding: 5,
    elevation: 0,
    backgroundColor: "transparent",
    height: 60
  },
  fontInput: {
    fontSize: 20,
    flex: 1,
    height: "auto",
    alignSelf: "stretch"
  },
  fontButton: {
    fontSize: 20
  },
  input: {
    marginBottom: 10,
    marginTop: 5,
    alignSelf: "stretch",
    height: "auto"
  },
  inputErrorPassword2: {
    marginBottom: 30,
    marginTop: 5,
    alignSelf: "stretch",
    height: "auto"
  },
  container: {
    padding: 10
  },
  helperText: {
    color: "#ff0000",
    fontSize: heightPercentageToDP("2.5%"),
    paddingHorizontal: 15
  },
  borderHelperText: {
    marginTop:10, 
    marginBottom:20,
    borderBottomWidth: 0
  },
  customView: {
    flexDirection: 'row',
    alignItems:'center',
    justifyContent:'center',
    marginTop:15
},
  customText: {
      fontWeight: 'bold',
      color: 'black',
      fontWeight:'bold'
}
})
