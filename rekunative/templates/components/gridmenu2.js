import React, {Component} from 'react'
import {FlatList,View,StyleSheet} from 'react-native'
import {Container, Content, Text, Icon, Button, Grid, Row ,Col} from 'native-base'

const Menu = ({data}) => (
    <Col>
        <View style={styles.buttonWrapper}>
            <Button block style={styles.button} onPress={data.onPress}>
                <Icon style={{color:'#257FD0dd',paddingBottom:10,fontSize:25}}name={data.icon} type='FontAwesome5'/>
                <Text style={{color:'#000',fontSize:12}}>{data.label}</Text>    
            </Button>
        </View>    
    </Col>    
)

export default class GridMenu2 extends Component {
    
    render() {
        const {data} = this.props
        return (
            <Container>
                <Content>
                    <Grid>
                        <Row>
                            <FlatList
                                data={data}
                                numColumns={3}
                                renderItem={ ({item}) => <Menu data={item} />}
                                keyExtractor={(item,index) => index.toString()}
                            />
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    buttonWrapper:{
        borderColor:'gray',
        borderBottomWidth: 0.92,
        borderLeftWidth: 0.92,
        borderRightWidth: 0.92,
        borderTopWidth: 0.92,
        
    },
    button:{
        backgroundColor:'transparent',
        height:120,
        width:'100%',
		alignSelf: 'center',
        flexDirection: 'column',
        paddingHorizontal:2.5,
        elevation:0,
    }
})
