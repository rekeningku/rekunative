const theme = {
    lightBlue: {
        backgroundColor: '#000',
        buttonLinear: {
            color1: '#46b6fb',
            color2: '#2B79C9'
        },
        fontColor: '#000'
    },
    darkBlue: {
        backgroundColor: '#0A1042',
        inputBorderColor: '#060E3F',
        buttonLinear: {
            color1: '#242B7C',
            color2: '#242B7C'
        },
        fontColor: '#fff'
    }
}

export default theme