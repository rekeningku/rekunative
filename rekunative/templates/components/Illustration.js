import React, { Component } from 'react'
import { Image,View, StyleSheet } from 'react-native'
import {
    Container,
    Text,
} from 'native-base'

const Illustration = ({children,text,image,style}) => {
    if(children === undefined){
        return(
            <View style={style}>
                {typeof image === 'string' ? (
                    <Image source={{uri:image}} style={{height:280,width:280,alignSelf:'stretch',marginBottom:5}} />
                ):image}
                <Text style={{color:style[1].color || '#46b6fb' ,fontSize:style[1].fontSize || 19,fontWeight:'bold'}}>{text}</Text>
            </View>
        )
    }else{
        return children
    }
}

export default class IllustrationComponent extends Component {
  render() {
    const {data,children} = this.props
    return (
        <Container>
            <View style={styles.viewIllustrasion}>
                <Illustration 
                style={[styles.illustration,{color:data.color,fontSize:data.fontSize}]} 
                image={data.image}
                text={data.text} 
                >
                    {children}
                </Illustration>
            </View>
        </Container>
    )
  }
}

const styles = StyleSheet.create({
    viewIllustrasion:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    illustration:{
        justifyContent:'center'
        ,alignItems:'center'
    }
})