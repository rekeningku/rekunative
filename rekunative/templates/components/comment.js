import React, { Component } from "react"
import { Container,Content,List,ListItem,Left,Body,Right,Thumbnail,Text, View, Input, Item, Button, Footer, Icon } from "native-base"
import { FlatList, StyleSheet } from "react-native"

export default class Comment extends Component {
  showReplies(id)  {
    return this.props.replies.map(function (item, i) {
        if (id == item.commentId) {
            return (
                <View style={{flexDirection: 'row', width: '95%', marginBottom :25}} key={i}>
                    <Thumbnail source={{ uri: item.image }} style={{ height: 30, width: 30, marginRight: 10 }} />
                    <View style={{backgroundColor: '#e0e0eb', borderRadius: 15, width: '90%', height: 65, paddingLeft: 10}}>
                        <Text style={{fontWeight: 'bold', fontSize: 14}}>{item.name}</Text>
                        <Text style={{fontSize: 14}}>{item.comment}</Text>
                        <Text note style={{ marginTop: 5}} >{item.time}</Text>
                    </View>          
                </View>
            )
        }
    })
  }

  render() {
    const { comments, showReply, replyId, replyFocus, buttonReply, buttonLike, buttonCommentListener, buttonReplyListener, handleChangeComment, handleChangeReply, showReplies, customStyles, isCustom } = this.props
    return (
      <Container>
        <Content keyboardDismissMode='none' keyboardShouldPersistTaps='always'>
          <List>
            <FlatList
              keyboardDismissMode='none'
              keyboardShouldPersistTaps='always'
              extraData={[]}
              data={comments}
              renderItem={({ item }) => (
                <ListItem avatar noBorder>
                  <Left>
                    <Thumbnail source={{ uri: item.image }} style={isCustom ? customStyles.thumbnail : styles.thumbnail}/>
                  </Left>
                  <Body style={styles.body}>
                  <View style={isCustom ? customStyles.view : styles.view}>
                    <Text style={styles.textName}>{item.name}</Text>
                    <Text style={styles.textComment}>{item.comment}</Text>
                  </View>
                    
                    <View style={styles.viewLike}>
                    <Text note style={styles.textTime} >{item.time}</Text>
                      <Text style={styles.textLike} onPress={() => buttonLike(item.id)}>Like  {item.like > 0 && (
                        <Text><Icon name='thumbs-o-up' type='FontAwesome' style={isCustom ? customStyles.iconLike : styles.iconLike} />{item.like}</Text>
                      )}
                      </Text>                      
                      <Text style={styles.textReply} onPress={ () => buttonReply(item.id)}>Reply</Text>
                    </View>

                    {isCustom ? showReplies(item.id) : this.showReplies(item.id)}

                    {showReply && replyId == item.id && (
                      <View style={styles.viewReply}>
                        <Thumbnail source={{ uri: item.image }} style={isCustom ? customStyles.thumbnailReply : styles.thumbnailReply} />
                        <Item rounded style={styles.itemReply}>
                          <Input autoFocus={replyFocus} placeholder='Write a reply...'
                            onChangeText={handleChangeReply}
                          />
                        </Item>
                        <Button full onPress={() => buttonReplyListener(item.id)} style={isCustom ? customStyles.buttonReply : styles.buttonReply}>
                          <Text style={isCustom ? customStyles.textButtonReply : styles.textButtonReply}>Reply</Text>
                        </Button>
                      </View>
                    )}
                  </Body>
                </ListItem>                
              )}      
              keyExtractor={index => index.toString()}
            />
          </List>
          <Footer style={isCustom ? customStyles.footer : styles.footer}>
            <View style={styles.viewComment}>
              <Thumbnail source={{ uri: comments[0].image }} style={ isCustom ? customStyles.thumbnailComment : styles.thumbnailComment } />
              <Item rounded style={styles.itemComment}>
                <Input placeholder='Write a comment...' onChangeText={handleChangeComment} />
              </Item>
              <Button full onPress={buttonCommentListener} style={isCustom ? customStyles.buttonComment : styles.buttonComment}>
                <Text style={styles.textPost}>Post</Text>
              </Button>
            </View>
          </Footer>
        </Content>      
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  body: {
    marginBottom: -20
  },
  textName: {
    fontWeight: "bold",
    fontSize: 14
  },
  textComment: {
    fontSize: 14
  },
  textTime: {
    marginRight: 10,
    marginTop: 0
  },
  textReply: {
    fontSize: 14
  },
  thumbnail: {
    height: 40,
    width: 40
  },
  view: {
    backgroundColor: "#e0e0eb",
    borderRadius: 15,
    paddingLeft: 10,
    paddingTop: 5,
    paddingBottom: 5,
    height: "auto",
    width: "97%"
  },
  viewLike: {
    flex: 1,
    flexDirection: "row",
    marginBottom: 10
  },
  textLike: {
    marginRight: 10,
    fontSize: 14
  },
  iconLike: {
    fontSize: 15,
    color: "blue"
  },
  viewReply: {
    flexDirection: "row",
    marginTop: 5,
    marginBottom: 10,
    width: "120%"
  },
  itemReply: {
    width: "55%",
    height: 40
  },
  thumbnailReply: {
    height: 30,
    width: 30
  },
  buttonReply: {
    elevation: 0,
    height: 40,
    width: 65,
    marginLeft: 5,
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#d1d1e0"
  },
  textButtonReply: {
    fontSize: 10,
    color: "#b3b3cc"
  },
  right: {
    position: "absolute",
    right: 10
  },
  viewComment: {
    flex: 1,
    flexDirection: "row",
    marginTop: 10,
    width: "120%"
  },
  thumbnailComment: {
    height: 40,
    width: 40,
    marginLeft: 5
  },
  itemComment: {
    flex: 1,
    alignSelf: "stretch",
    height: 40
  },
  buttonComment: {
    elevation: 0,
    height: 40,
    width: 60,
    marginLeft: 5,
    marginRight: 5,
    backgroundColor: "#fff",
    borderWidth: 1,
    borderColor: "#d1d1e0"
  },
  textPost: {
    fontSize: 10,
    color: "#b3b3cc"
  },
  footer: {
    backgroundColor: "#fff"
  },

  viewReplies: {
      flexDirection: 'row', width: '95%', marginBottom :25
  },
  thumbnailReplies: {
      height: 30, width: 30, marginRight: 10
  },
  viewInnerReplies: {
      backgroundColor: '#e0e0eb', borderRadius: 15, width: '90%', height: 65, paddingLeft: 10
  },
  textRepliesName: {
      fontWeight: 'bold', fontSize: 14
  },
  textRepliesComment: {
      fontSize: 14
  },
  textRepliesTime: {
      marginTop: 5
  },
})
