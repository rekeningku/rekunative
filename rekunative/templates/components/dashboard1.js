import React, { Component } from 'react'
import { View, Text, Content, Container, Grid, Row, Button, Icon, Body, H3, Card, CardItem } from 'native-base'
import { StyleSheet, processColor } from 'react-native'
import _ from 'lodash'

import { PieChart, LineChart } from 'react-native-charts-wrapper'
const defaultPetrel = '#3B9199'
class Dashboard extends Component {
	state = {
		socmedContainerWidth: null
	}

	handleSelect(event) {
		let entry = event.nativeEvent
		if (entry == null) {
			this.setState({ ...this.state, selectedEntry: null })
		} else {
			this.setState({ ...this.state, selectedEntry: JSON.stringify(entry) })
		}
	}

	renderSocmed = () => {
		const width = this.state.socmedContainerWidth / 3 - 8
		return [ 1, 2, 3 ].map((e) => {
			const { color = 'white', backgroundColor = '#2AB5FA' } = this.props[`socmed${e}`]
			return (
				<Button style={[ styles.buttonSocmed, { backgroundColor, width } ]} key={e} onPress={this.props[`socmed${e}`].onPress}>
					<View style={styles.buttonInnerWrapper}>
						<View style={styles.textWrapper}>
							<Text style={[ styles.text, { color } ]}>{this.props[`socmed${e}`].number}</Text>
							<Text style={[ styles.text, styles.textSmall, { color } ]}>{this.props[`socmed${e}`].label}</Text>
						</View>
						<View style={styles.IconWrapper}>
							<Icon name={this.props[`socmed${e}`].icon} type='FontAwesome' style={{ color: 'white' }} />
						</View>
					</View>
				</Button>
			)
		})
	}

	getSocmedContainerWidth = (event) => {
		this.setState({
			socmedContainerWidth: event.nativeEvent.layout.width
		})
	}

	renderPieChart = () => {
		return (
			!_.isEmpty(this.props.pieChart) && (
				<Card style={styles.pieChart}>
					<CardItem header>
						<Text>{this.props.pieChart.label}</Text>
					</CardItem>
					<CardItem style={{ flex: 1 }}>
						<View style={{ flex: 1 }}>
							<PieChart
								style={styles.chart}
								data={this.props.pieChart.data.dataSets.config ? this.props.pieChart.data : {
									dataSets:[{
										values:this.props.pieChart.data.dataSets[0].values,
										label:'',
										config:{
													colors: [
														processColor('#C0FF8C'),
														processColor('#FFF78C'),
														processColor('#FFD08C'),
														processColor('#8CEAFF'),
														processColor('#FF8C9D')
													],
													valueTextSize: 20,
													valueTextColor: processColor('white'),
													sliceSpace: 5,
													valueFormatter: "#.#'%'",
													valueLineColor: processColor('green')
												},
											}]
								}}
								// logEnabled={true}
								chartDescription={this.props.pieChart.description ? {
									text:this.props.pieChart.description.text,
									textSize : 10,
									textColor : processColor('darkgray')
								} : {
									text : 'This is Pie chart description',
									textSize : 10,
									textColor : processColor('darkgray')
								}}
								legend={this.props.pieChart.legend ?  this.props.pieChart.legend : {
									enabled: true,
									textSize: 10,
									form: 'CIRCLE',
									horizontalAlignment: 'LEFT',
									verticalAlignment: 'BOTTOM',
									orientation: 'HORIZONTAL',
									wordWrapEnabled: true
								}}
								highlights={this.props.pieChart.highlights ? this.props.pieChart.highlights : [ { x: 2 } ]}
								entryLabelColor={processColor('green')}
								entryLabelTextSize={10}
								drawEntryLabels={false}
								rotationEnabled={true}
								usePercentValues={true}
								// centerTextRadiusPercent={10}
								holeColor={processColor('white')}
								transparentCircleRadius={10}
								holeRadius={30}
								transparentCircleColor={processColor('#f0f0f088')}
								maxAngle={360}
								onSelect={this.handleSelect.bind(this)}
								onChange={(event) => console.log(event.nativeEvent)}
							/>
						</View>
					</CardItem>
				</Card>
			)
		)
	}

	renderLineChart = () => {
		return (
			!_.isEmpty(this.props.lineChart) && (
				<Card style={styles.pieChart}>
					<CardItem header>
						<Text>{this.props.lineChart.label}</Text>
					</CardItem>
					<CardItem style={{ flex: 1 }}>
						<View style={{ flex: 1 }}>
							<LineChart
								style={styles.chart}
								data={this.props.lineChart.data.config ? this.props.lineChart.data : {
									dataSets:[{
										values: this.props.lineChart.data.dataSets[0].values,
										label:'Line',
										config: {
											// mode: 'CUBIC_BEZIER',
											drawValues: false,
											lineWidth: 2,
											drawCircles: true,
											circleColor: processColor(defaultPetrel),
											drawCircleHole: false,
											circleRadius: 5,
											highlightColor: processColor('transparent'),
											color: processColor(defaultPetrel),
											drawFilled: true,
											fillGradient: {
												colors: [ processColor('#EBF6FF'), processColor('#EBF6FF') ],
												positions: [ 0, 0.5 ],
												angle: 90,
												orientation: 'TOP_BOTTOM'
											},
											fillAlpha: 1000,
											valueTextSize: 15
										}
									}]	
								}}
								borderColor={processColor('teal')}
								marker={{
									enabled: true,
									digits: 2,
									backgroundTint: processColor('teal'),
									markerColor: processColor('#F0C0FF8C'),
									textColor: processColor('white')
								}}
								xAxis={{
									enabled: true,
									granularity: 1,
									drawLabels: false,
									position: 'BOTTOM',
									drawAxisLine: false,
									drawGridLines: false,
									fontFamily: 'HelveticaNeue-Medium',
									fontWeight: 'bold',
									textSize: 12,
									textColor: processColor('gray')
								}}
								yAxis={{
									left: {
										enabled: true
									},
									right: {
										enabled: false
									}
								}}
								drawGridBackground={false}
								borderWidth={1}
								drawBorders={true}
								autoScaleMinMaxEnabled={false}
								touchEnabled={true}
								dragEnabled={true}
								scaleEnabled={true}
								scaleXEnabled={true}
								scaleYEnabled={true}
								pinchZoom={true}
								doubleTapToZoomEnabled={true}
								highlightPerTapEnabled={true}
								dragDecelerationEnabled={true}
								dragDecelerationFrictionCoef={0.99}
								highlightPerDragEnabled={false}
							/>
						</View>
					</CardItem>
				</Card>
			)
		)
	}

	renderProgressChart = () => {
		return (
			!_.isEmpty(this.props.progressChart) && (
				<Card style={styles.pieChart}>
					<CardItem header>{typeof this.props.progressChart.label === "object" ? (
						<View>
							<H3 style={{ fontWeight: 'bold' }}>{this.props.progressChart.label.title}</H3>
							<Text>{this.props.progressChart.label.number}</Text>
							<Text style={{ fontSize: 10 }}>{this.props.progressChart.label.small}</Text>
						</View>
						): this.props.progressChart.label
						}</CardItem>
					<CardItem style={{ flex: 1 }}>
						<View style={{ flex: 1 }}>
							<PieChart
								style={styles.chart}
								data={this.props.progressChart.data.config ? this.props.progressChart.data : {
									dataSets:[{
										values: this.props.progressChart.data.dataSets[0].values,
										label:'',
										config: {
											colors: [
												processColor('#C0FF8C'),
												processColor('#FFFFFF'),
												processColor('#FFD08C'),
												processColor('#8CEAFF'),
												processColor('#FF8C9D')
											],
											valueTextSize: 20,
											valueTextColor: processColor('white'),
											// xValuePosition: "OUTSIDE_SLICE",
											// yValuePosition: 'OUTSIDE_SLICE',
											valueFormatter: "#.#'%'",
											valueLineColor: processColor('green')
										}
									}]
								}}
								// logEnabled={true}
								chartDescription={this.props.progressChart.description ? {
									text: this.props.progressChart.description.text,
									textSize: 15,
									textColor: processColor('darkgray')
								} : {
									text: '' , 
									textSize: 15,
									textColor: processColor('darkgray')
								}}
								legend={this.props.progressChart.legend ? this.props.progressChart.legend : {
									enabled: true,
									textSize: 15,
									form: 'CIRCLE',

									horizontalAlignment: 'RIGHT',
									verticalAlignment: 'TOP',
									orientation: 'VERTICAL',
									wordWrapEnabled: true
								}} 
								highlights={this.props.progressChart.highlights ? this.props.progressChart.highlights : [ { x: 2 } ] }
								entryLabelColor={processColor('green')}
								entryLabelTextSize={10}
								drawEntryLabels={false}
								rotationEnabled={true}
								usePercentValues={true}
								centerTextRadiusPercent={10}
								holeColor={processColor('white')}
								transparentCircleRadius={10}
								holeRadius={30}
								transparentCircleColor={processColor('#f0f0f088')}
								maxAngle={360}
								onSelect={this.handleSelect.bind(this)}
								onChange={(event) => console.log(event.nativeEvent)}
							/>
						</View>
					</CardItem>
				</Card>
			)
		)
	}

	renderFirstChart = () => {
		if (this.props.pieChart && this.props.pieChart.index === 0) return this.renderPieChart()
		if (this.props.lineChart && this.props.lineChart.index === 0) return this.renderLineChart()
		if (this.props.progressChart && this.props.progressChart.index === 0) return this.renderProgressChart()
		return null
	}

	renderSecondChart = () => {
		if (this.props.pieChart && this.props.pieChart.index === 1) return this.renderPieChart()
		if (this.props.lineChart && this.props.lineChart.index === 1) return this.renderLineChart()
		if (this.props.progressChart && this.props.progressChart.index === 1) return this.renderProgressChart()
		return null
	}

	renderThirdChart = () => {
		if (this.props.pieChart && this.props.pieChart.index === 2) return this.renderPieChart()
		if (this.props.lineChart && this.props.lineChart.index === 2) return this.renderLineChart()
		if (this.props.progressChart && this.props.progressChart.index === 2) return this.renderProgressChart()
		return null
	}

	render() {
		return (
			<Container>
				<Content style={styles.container}>
					<Card transparent style={{ marginTop: 0, marginBottom: 0 }}>
						<Grid onLayout={this.getSocmedContainerWidth}>
							<Row style={styles.socmed}>{this.renderSocmed()}</Row>
						</Grid>
					</Card>
					{this.renderFirstChart()}

					{this.renderSecondChart()}

					{this.renderThirdChart()}
				</Content>
			</Container>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#F2F2F2',
		padding: 10
	},

	// socmed,
	socmed: {
		justifyContent: 'space-between'
	},
	buttonSocmed: {
		// width: '33.33%',
		padding: 10,
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonInnerWrapper: {
		flex: 1,
		flexDirection: 'row'
	},
	textWrapper: {
		flex: 1
	},
	text: {
		color: 'white'
	},
	textSmall: {
		fontSize: 10
	},
	IconWrapper: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},

	// pieChart
	pieChart: {
		marginBottom: 0,
		marginTop: 10,
		height: 500
	},

	chart: {
		flex: 1
	}
})

export default Dashboard