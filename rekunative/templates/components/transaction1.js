import React, {Component} from 'react'
import { StyleSheet, FlatList, Image} from 'react-native';
import {Container,Tab,Tabs,Grid,Row,Col,Text,ScrollableTab, ListItem, List, Left, Thumbnail, Body, TabHeading} from 'native-base'

export default class Transaction1 extends Component {
    render() {
        const {transactions,proses,sedangDikirim,sampaiTujuan} = this.props
        return (
            <Container>
                    <Tabs transparent renderTabBar={()=> <ScrollableTab style={{backgroundColor:'#2B79C9'}} />} >
                        <Tab heading={
                            <TabHeading style={{backgroundColor:'#2B79C9'}}>
                                <Text>Proses</Text>
                            </TabHeading>
                        }>

                        {proses ? <Proses/> : <NotFound text='Proses'/> }
                        </Tab>
                        <Tab heading={
                            <TabHeading style={{backgroundColor:'#2B79C9'}}>
                                <Text>Sedang Dikirim</Text>
                            </TabHeading>
                        }>
                        
                        {sedangDikirim ? <SedangDikirim/> : <NotFound text='Sedang Dikirim'/> }
                        </Tab>
                        <Tab heading={
                            <TabHeading style={{backgroundColor:'#2B79C9'}}>
                                <Text>Sampai Tujuan</Text>
                            </TabHeading>
                        }>

                        {sampaiTujuan ? <SampaiTujuan/> : <NotFound text='Sampai Tujuan'/> }
                        </Tab>
                        <Tab heading={
                            <TabHeading style={{backgroundColor:'#2B79C9'}}>
                                <Text>Transaction</Text>
                            </TabHeading>
                        }>

                        {transactions ? <Transaction data={transactions}/> : <NotFound text='transactions'/> }
                        </Tab>
                    </Tabs>
            </Container>
        )
    }
}
const NotFound = ({text = ''}) => {
    return(
        <Grid>
            <Row>
                <Col style={{justifyContent:'center',alignItems:'center',alignSelf:'stretch'}}>
                    <Image source={{uri:'https://imgdb.net/images/4888.png'}} style={{height:220,width:220}} />
                    <Text>Tidak Ada {text}</Text>
                </Col>
            </Row>
        </Grid>
    )
}
const Proses = ({data}) => {
    return(
        <Grid>
            <Row style={{justifyContent:'center',alignItems:'center'}}>
                <Text>Proses</Text>
            </Row>
        </Grid>
    )
}
const SedangDikirim = ({data}) => {
    return(
       <Grid>
            <Row style={{justifyContent:'center',alignItems:'center'}}>
                <Text>Sedang Dikirim</Text>
            </Row>
        </Grid>
    )
}
const SampaiTujuan = ({data}) => {
    return(
        <Grid>
            <Row style={{justifyContent:'center',alignItems:'center'}}>
                <Text>SampaiTujuan</Text>
            </Row>
        </Grid>
    )
}
const Transaction = ({data}) => {
    return(
       <Grid>
            <Row>
                <Col>
                    <FlatList
                        data={data}
                        renderItem={ ({item})=> (
                            <List>
                                <ListItem button avatar thumbnail>
                                    <Left>
                                        <Thumbnail
                                            source={{uri:item.imageStore}}
                                        />
                                    </Left>
                                    <Body>
                                        <Text>{item.code}</Text>
                                        <Text note>{item.date}</Text>
                                        <Text>Beli dari : {item.store}</Text>
                                        <Text note>{item.transactionStatus ? 'transaction Selesai ...' : 'transaction Dibatalkan'}</Text>
                                            {!item.noResi == '' && (
                                                <Text>
                                                    <Text note>Nomor Resi </Text> 
                                                    <Text style={styles.txtNoResi}>{item.noResi}</Text>
                                                </Text>
                                            )}
                                    </Body>
                                </ListItem>
                            </List>
                           
                        )}
                        keyExtractor={(item,index) => index.toString()}
                    />
                </Col>
            </Row>
        </Grid>
    )
}

const styles = StyleSheet.create({
    txtNoResi:{
        fontSize:13,
        fontWeight:'bold',
        color:'#000'
    }
})