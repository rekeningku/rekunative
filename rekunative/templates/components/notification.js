import React, {Component} from 'react'
import {StyleSheet, FlatList} from 'react-native'
import {
    Container,
    Text,
    Body,
    Content,
    Grid,
    Row,
    Col,
    List,
    ListItem,
    Thumbnail,
    Left,
    Right
} from 'native-base';

export default class Notification extends Component {
    render() {
        const {data,setting} = this.props
        const activeColor = setting ? setting.isRead.activeColor  : '#ECF2FE'
        const unactiveColor = setting ? setting.isRead.unactiveColor : '#fff'
        return (
            <Container>
                <Content>
                    <Grid>
                        <Row>
                            <Col>
                                <FlatList
                                    data={data}
                                    renderItem={({item}) =>( 
                                        <List style={{backgroundColor:!item.isRead ? activeColor : unactiveColor}}>
                                        <ListItem button avatar thumbnail>
                                            <Left>
                                                <Thumbnail
                                                    source={{
                                                    uri: item.imgProfileUrl
                                                }}/>
                                            </Left>
                                            <Body>
                                                <Text>
                                                    <Text
                                                        style={{
                                                        fontWeight: 'bold'
                                                    }}>{item.name} </Text>
                                                    <Text>{item.desc}</Text>
                                                </Text>
                                                <Text note style={{marginTop:5}}>{item.time}</Text>
                                            </Body>
                                            <Right>
                                                <Thumbnail
                                                    square
                                                    source={{
                                                    uri: item.imageUrl || '__'
                                                }}/>
                                            </Right>
                                        </ListItem>
                                        </List>
                                    )}
                                    keyExtractor={(item, index) => index.toString()}/>
                            </Col>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
}